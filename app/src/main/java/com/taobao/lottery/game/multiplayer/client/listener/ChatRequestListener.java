package com.taobao.lottery.game.multiplayer.client.listener;

public abstract interface ChatRequestListener
{
  public abstract void onSendChatDone(byte paramByte, String paramString);
  
  public abstract void onSendPrivateChatDone(byte paramByte, String paramString);
}


