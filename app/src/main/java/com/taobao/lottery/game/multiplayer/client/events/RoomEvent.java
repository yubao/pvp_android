 package com.taobao.lottery.game.multiplayer.client.events;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;

import org.json.JSONException;
import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class RoomEvent
 {
   private RoomData info;
   private byte result;
   
   public RoomEvent(RoomData info, byte result)
   {
     this.info = info;
     this.result = result;
   }
   
 
 
 
 
   public RoomData getData()
   {
     return this.info;
   }
   
 
 
 
   public byte getResult()
   {
     return this.result;
   }
   
 
 
 
 
 
   public static RoomEvent buildRoomEvent(MMOGameResponseMessage msg)
     throws JSONException
   {
     RoomData roomInfo = null;
     if (msg.getResultCode() == 0)
     {
       JSONObject jsonRoom = new JSONObject(new String(msg.getPayLoad()));
       roomInfo = new RoomData(jsonRoom.getString("id"), jsonRoom.getString("owner"), jsonRoom.getString("name"), jsonRoom.getInt("maxUsers"));
     }
     
 
     RoomEvent roomEvent = new RoomEvent(roomInfo, msg.getResultCode());
     return roomEvent;
   }
 }


