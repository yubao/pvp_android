 package com.taobao.lottery.game.multiplayer.client;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameNotifyMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;
import com.taobao.lottery.game.multiplayer.client.util.Util;
 
 
 
 
 
 
 
 
 
 public class MessageDispatchThread
   extends Thread
 {
   boolean shouldStop = false;
   MMOGameClient theClient = null;
   
   MessageDispatchThread(MMOGameClient client) {
     super("MessageDispatchThread");
     this.theClient = client;
   }
   
 
 
   public void run()
   {
     while (!this.shouldStop)
     {
 
       MMOGameMessage msg = this.theClient.getMessageFromQueue();
       if (msg == null) {
         synchronized (this.theClient.dispatcher) {
           try {
             this.theClient.dispatcher.wait();
 
 
           }
           catch (InterruptedException e) {}
         }
         
       }
       else if (msg.getType() == 1)
       {
         this.theClient.onResponse((MMOGameResponseMessage)msg);
       }
       else
       {
         this.theClient.onNotify((MMOGameNotifyMessage)msg);
       }
     }
     
     Util.trace("MessageDispatchThread exiting");
   }
   
   public void terminate() {
     this.shouldStop = true;
   }
 }


