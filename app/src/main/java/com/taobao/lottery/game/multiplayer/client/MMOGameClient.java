 package com.taobao.lottery.game.multiplayer.client;
 
 import android.util.Log;

 import com.taobao.lottery.game.multiplayer.client.events.LiveRoomInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveUserInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LobbyEvent;
import com.taobao.lottery.game.multiplayer.client.events.MatchedRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomEvent;
import com.taobao.lottery.game.multiplayer.client.listener.ChatRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.ConnectionRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.LobbyRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.NotifyListener;
import com.taobao.lottery.game.multiplayer.client.listener.RoomRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.TurnBasedRoomListener;
import com.taobao.lottery.game.multiplayer.client.listener.UpdateRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.ZoneRequestListener;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameRequestMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;
import com.taobao.lottery.game.multiplayer.client.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
 
 public class MMOGameClient
 {
   private ClientChannel clientChannel;
   private String apiKey;
   private final Set<ConnectionRequestListener> ConnectionRequestListeners = Collections.synchronizedSet(new java.util.HashSet());
   private final Set<ZoneRequestListener> zoneRequestListeners = Collections.synchronizedSet(new java.util.HashSet());
   private final Set<RoomRequestListener> roomRequestListeners = Collections.synchronizedSet(new java.util.HashSet());
   private final Set<LobbyRequestListener> lobbyRequestListeners = Collections.synchronizedSet(new java.util.HashSet());
   private final Set<ChatRequestListener> chatRequestListeners = Collections.synchronizedSet(new java.util.HashSet());
   private final List<UpdateRequestListener> updateRequestListeners = Collections.synchronizedList(new ArrayList());
   private final List<NotifyListener> notifyListeners = Collections.synchronizedList(new ArrayList());
   private final Set<TurnBasedRoomListener> turnBasedRoomListeners = Collections.synchronizedSet(new java.util.HashSet());
   
   int sessionId = 0;
   private String authData = "";
   private Thread udpListenerThread = null;
   private UDPListener udpListener = null;
   
   private ConnectionWatchTask connectTimeoutTask;
   
   private java.util.Timer connectionTimer;
   protected MessageDispatchThread dispatcher = null;
   private final ArrayList<com.taobao.lottery.game.multiplayer.client.message.MMOGameMessage> messageQueue = new ArrayList();
   
 
   public static void enableTrace(boolean enable)
   {
     Util.TRACE_ENABLED = enable;
   }
   
 
 
 
   public static void setRecoveryAllowance(int maxRecoveryTime)
   {
     Util.RECOVERY_ALLOWANCE_TIME = maxRecoveryTime;
   }
   
 
 
 
 
 
   public static MMOGameClient getInstance()
     throws Exception
   {
     if (_instance == null) {
       throw new Exception("MMOGameClient not initialized!");
     }
     return _instance; }
   
   private static MMOGameClient _instance = null;
   private int connectionState = 2;
   private final int MAX_UPDATE_SIZE = 1000;
   private final int MAX_CHAT_SIZE = 500;
   
 
 
 
 
 
 
 
 
 
 
 
 
   public static byte initialize(String apiKey, String server)
   {
     if ((_instance != null) || (apiKey == null) || (server == null)) {
       return 4;
     }
     _instance = new MMOGameClient();
     _instance.apiKey = apiKey;
     Util.MMOGameServerHost = server;
     _instance.dispatcher = new MessageDispatchThread(_instance);
     _instance.dispatcher.start();
     return 0;
   }
   
   public void setAppKey(String appKey) {
     _instance.apiKey = appKey;
   }
   
   public void setHost(String hostAddress) {
     Util.MMOGameServerHost = hostAddress;
   }
   
   public void setPort(int port) {
     Util.MMOGameServerPort = port;
   }
   
   synchronized void addMessageToQueue(com.taobao.lottery.game.multiplayer.client.message.MMOGameMessage msg) {
     this.messageQueue.add(msg);
   }
   
   synchronized com.taobao.lottery.game.multiplayer.client.message.MMOGameMessage getMessageFromQueue() {
     if (this.messageQueue.size() > 0) {
       return (com.taobao.lottery.game.multiplayer.client.message.MMOGameMessage)this.messageQueue.remove(0);
     }
     
     return null;
   }
   
   String getAPIKey()
   {
     return this.apiKey;
   }
   
 
 
 
 
 
 
 
   public void addConnectionRequestListener(ConnectionRequestListener listener)
   {
     this.ConnectionRequestListeners.add(listener);
   }
   
 
 
 
 
   public void removeConnectionRequestListener(ConnectionRequestListener listener)
   {
     this.ConnectionRequestListeners.remove(listener);
   }
   
 
 
 
 
 
 
   public void addZoneRequestListener(ZoneRequestListener listener)
   {
     this.zoneRequestListeners.add(listener);
   }
   
 
 
 
 
   public void removeZoneRequestListener(ZoneRequestListener listener)
   {
     this.zoneRequestListeners.remove(listener);
   }
   
 
 
 
 
 
 
   public void addLobbyRequestListener(LobbyRequestListener listener)
   {
     this.lobbyRequestListeners.add(listener);
   }
   
 
 
 
 
   public void removeLobbyRequestListener(LobbyRequestListener listener)
   {
     this.lobbyRequestListeners.remove(listener);
   }
   
 
 
 
 
 
 
   public void addRoomRequestListener(RoomRequestListener listener)
   {
     this.roomRequestListeners.add(listener);
   }
   
 
 
 
 
 
 
   public void addTurnBasedRoomListener(TurnBasedRoomListener listener)
   {
     this.turnBasedRoomListeners.add(listener);
   }
   
 
 
 
 
   public void removeTurnBasedRoomListener(TurnBasedRoomListener listener)
   {
     this.turnBasedRoomListeners.remove(listener);
   }
   
 
 
 
 
   public void removeRoomRequestListener(RoomRequestListener listener)
   {
     this.roomRequestListeners.remove(listener);
   }
   
 
 
 
 
 
   public void addChatRequestListener(ChatRequestListener listener)
   {
     this.chatRequestListeners.add(listener);
   }
   
 
 
 
 
   public void removeChatRequestListener(ChatRequestListener listener)
   {
     this.chatRequestListeners.remove(listener);
   }
   
 
 
 
 
 
   public void addUpdateRequestListener(UpdateRequestListener listener)
   {
     this.updateRequestListeners.add(listener);
   }
   
 
 
 
 
   public void removeUpdateRequestListener(UpdateRequestListener listener)
   {
     this.updateRequestListeners.remove(listener);
   }
   
 
 
 
 
 
 
   public void addNotificationListener(NotifyListener listener)
   {
     if (!this.notifyListeners.contains(listener)) {
       this.notifyListeners.add(listener);
     }
   }
   
 
 
 
 
   public void removeNotificationListener(NotifyListener listener)
   {
     this.notifyListeners.remove(listener);
   }
   
 
 
 
 
 
 
 
   public void connectWithUserName(String userName, String authData)
   {
     if ((isNull(userName)) || (!isUserNameValid(userName))) {
       fireConnectionEvent((byte)4, null);
       return; }
     if ((this.connectionState != 2) && (this.connectionState != 3)) {
       fireConnectionEvent((byte)4, null);
       return;
     }
     try {
       this.connectionState = 1;
       Util.userName = userName;
       this.authData = authData;
       this.sessionId = 0;

         onLookUpServer((byte)0);

     } catch (Exception ex) {
       fireConnectionEvent((byte)5, null);
     }
   }
   
   private boolean isUserNameValid(String userName) {
     if ((userName.length() > 25) || (userName.indexOf(';') != -1) || (userName.indexOf(',') != -1) || (userName.indexOf('/') != -1) || (userName.indexOf('\\') != -1)) {
       return false;
     }
     return true;
   }
   
 
 
 
   public void disconnect()
   {
     if ((this.connectionState == 2) || (this.connectionState == 3)) {
       fireDisconnectEvent((byte)4);
       return;
     }
     Util.userName = "";
     this.sessionId = 0;
     this.connectionState = 3;
     MMOGameRequestMessage signoutMsg = new MMOGameRequestMessage((byte)14, this.sessionId, 0, (byte)0, (byte)0, (byte)1, 0, null);
     
 
 
     this.clientChannel.SendRequest(signoutMsg);
   }
   
 
 
 
 
 
   public int getConnectionState()
   {
     return this.connectionState;
   }
   
 
 
 
   public void RecoverConnection()
   {
     if ((this.sessionId == 0) || (Util.userName.length() <= 0) || (this.connectionState != 2)) {
       fireConnectionEvent((byte)4, null);
       return;
     }
     try {
       onLookUpServer((byte)0);
       this.connectionState = 4;
     } catch (Exception ex) {
       fireConnectionEvent((byte)5, null); } }
   
   void onResponse(MMOGameResponseMessage msg) { String descCreate;
     RoomEvent roomEvent;
     String descDelete;
     String descJoin;
     String descUpdate;
     LiveRoomInfoEvent liveRoomInfoEvent;
     MatchedRoomsEvent matchedRoomsEvent;
     LobbyEvent lobbyEvent;
     String descChat; String descPrivatechat; com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent allRoomsEvent; com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent allUsersEvent; LiveUserInfoEvent customUserEvent; LiveUserInfoEvent userInfoEvent; String descMove; String descStart; String descStop; com.taobao.lottery.game.multiplayer.client.events.MoveEvent[] moves; 

               switch (msg.getRequestType())
     {
     case 1: 
       handleAuthenticateResponse(msg);
       break;
     case 64: 
       if (msg.getResultCode() == 0) {
         MMOGameRequestMessage updateMsg = new MMOGameRequestMessage((byte)65, this.sessionId, 0, (byte)0, (byte)2, (byte)1, 0, null);
         
 
 
 
         this.udpListener.SendRequest(updateMsg);
       }
       else {
         cleanupUdpListener();
       }
       fireUDPEvent(msg.getResultCode());
       break;
     case 6: 
       descCreate = null;
       try {
         roomEvent = RoomEvent.buildRoomEvent(msg);
         descCreate = new JSONObject(new String(msg.getPayLoad())).optString("desc");
       } catch (JSONException ex) {
         roomEvent = new RoomEvent(null, (byte)6);
       }
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onCreateRoomDone(roomEvent, descCreate);
       }
       break;
     case 11: 
       descDelete = null;
       try {
         roomEvent = RoomEvent.buildRoomEvent(msg);
         descDelete = new JSONObject(new String(msg.getPayLoad())).optString("desc");
       } catch (JSONException ex) {
         roomEvent = new RoomEvent(null, (byte)6);
       }
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onDeleteRoomDone(roomEvent, descDelete);
       }
       break;
     case 7: 
     case 24: 
     case 27: 
     case 37: 
       descJoin = null;
       try {
         roomEvent = RoomEvent.buildRoomEvent(msg);
         descJoin = new JSONObject(new String(msg.getPayLoad())).optString("desc");
       } catch (JSONException ex) {
         roomEvent = new RoomEvent(null, (byte)6);
       }
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onJoinRoomDone(roomEvent, descJoin);
       }
       break;
     case 25: 
       descUpdate = null;
       try {
         liveRoomInfoEvent = LiveRoomInfoEvent.buildLiveRoomEvent(msg);
         descUpdate = new JSONObject(new String(msg.getPayLoad())).optString("desc");
       } catch (JSONException ex) {
         liveRoomInfoEvent = new LiveRoomInfoEvent(null, (byte)6, null, null);
       }
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onUpdatePropertyDone(liveRoomInfoEvent, descUpdate);
       }
       break;
     case 35: 
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onLockPropertiesDone(msg.getResultCode());
       }
       break;
     case 36: 
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onUnlockPropertiesDone(msg.getResultCode());
       }
       break;
     case 10: 
       try {
         roomEvent = RoomEvent.buildRoomEvent(msg);
       } catch (JSONException ex) {
         roomEvent = new RoomEvent(null, (byte)6);
       }
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onLeaveRoomDone(roomEvent);
       }
       break;
     case 8: 
       try {
         roomEvent = RoomEvent.buildRoomEvent(msg);
       } catch (JSONException ex) {
         roomEvent = new RoomEvent(null, (byte)6);
       }
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onSubscribeRoomDone(roomEvent);
       }
       break;
     case 9: 
       try
       {
         roomEvent = RoomEvent.buildRoomEvent(msg);
       } catch (JSONException ex) {
         roomEvent = new RoomEvent(null, (byte)6);
       }
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onUnSubscribeRoomDone(roomEvent);
       }
       break;
     case 20: 
       try {
         liveRoomInfoEvent = LiveRoomInfoEvent.buildLiveRoomEvent(msg);
       } catch (JSONException ex) {
         liveRoomInfoEvent = new LiveRoomInfoEvent(null, (byte)6, null, null);
       }
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onGetLiveRoomInfoDone(liveRoomInfoEvent);
       }
       break;
     case 28: 
     case 29: 
     case 38: 
       try {
         matchedRoomsEvent = MatchedRoomsEvent.buildMatchedRoomsEvent(msg);
       } catch (JSONException ex) {
         matchedRoomsEvent = new MatchedRoomsEvent((byte)6, null);
       }
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onGetMatchedRoomsDone(matchedRoomsEvent);
       }
       break;
     case 23: 
       try {
         liveRoomInfoEvent = LiveRoomInfoEvent.buildLiveRoomEvent(msg);
       } catch (JSONException ex) {
         liveRoomInfoEvent = new LiveRoomInfoEvent(null, (byte)6, null, null);
       }
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onGetLiveLobbyInfoDone(liveRoomInfoEvent);
       }
       break;
     case 21: 
       try {
         liveRoomInfoEvent = LiveRoomInfoEvent.buildLiveRoomEvent(msg);
       } catch (JSONException ex) {
         liveRoomInfoEvent = new LiveRoomInfoEvent(null, (byte)6, null, null);
       }
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onSetCustomRoomDataDone(liveRoomInfoEvent);
       }
       break;
     case 3: 
       try {
         lobbyEvent = LobbyEvent.buildLobbyEvent(msg);
       } catch (JSONException ex) {
         lobbyEvent = new LobbyEvent(null, (byte)6);
       }
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onSubscribeLobbyDone(lobbyEvent);
       }
       break;
     case 4: 
       try {
         lobbyEvent = LobbyEvent.buildLobbyEvent(msg);
       } catch (JSONException ex) {
         lobbyEvent = new LobbyEvent(null, (byte)6);
       }
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onUnSubscribeLobbyDone(lobbyEvent);
       }
       break;
     case 2: 
       try
       {
         lobbyEvent = LobbyEvent.buildLobbyEvent(msg);
       } catch (JSONException ex) {
         lobbyEvent = new LobbyEvent(null, (byte)6);
       }
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onJoinLobbyDone(lobbyEvent);
       }
       break;
     case 5: 
       try {
         lobbyEvent = LobbyEvent.buildLobbyEvent(msg);
       } catch (JSONException ex) {
         lobbyEvent = new LobbyEvent(null, (byte)6);
       }
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onLeaveLobbyDone(lobbyEvent);
       }
       break;
     case 12: 
       try
       {
         descChat = new JSONObject(new String(msg.getPayLoad())).optString("desc");
       } catch (JSONException je) {
         descChat = null;
       }
       for (ChatRequestListener listener : this.chatRequestListeners) {
         listener.onSendChatDone(msg.getResultCode(), descChat);
       }
       break;
     case 30: 
       try
       {
         descPrivatechat = new JSONObject(new String(msg.getPayLoad())).optString("desc");
       } catch (JSONException je) {
         descPrivatechat = null;
       }
       for (ChatRequestListener listener : this.chatRequestListeners) {
         listener.onSendPrivateChatDone(msg.getResultCode(), descPrivatechat);
       }
       break;
     case 13: 
       for (int i = 0; i < this.updateRequestListeners.size(); i++) {
         ((UpdateRequestListener)this.updateRequestListeners.get(i)).onSendUpdateDone(msg.getResultCode());
       }
       break;
     case 17: 
       try
       {
         allRoomsEvent = com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent.buildAllRoomsEvent(msg);
       } catch (JSONException ex) {
         allRoomsEvent = new com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent((byte)6, null);
       }
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onGetAllRoomsDone(allRoomsEvent);
       }
       break;
     case 18: 
       try
       {
         allUsersEvent = com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent.buildAllUsersEvent(msg);
       } catch (JSONException ex) {
         allUsersEvent = new com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent((byte)6, null);
       }
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onGetOnlineUsersDone(allUsersEvent);
       }
       break;
     case 22: 
       try
       {
         customUserEvent = LiveUserInfoEvent.buildLiveUserInfoEvent(msg);
       } catch (JSONException ex) {
         customUserEvent = new LiveUserInfoEvent((byte)6, null, null, null, false, false);
       }
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onSetCustomUserDataDone(customUserEvent);
       }
       break;
     case 19: 
       try
       {
         userInfoEvent = LiveUserInfoEvent.buildLiveUserInfoEvent(msg);
       } catch (JSONException ex) {
         userInfoEvent = new LiveUserInfoEvent((byte)6, null, null, null, false, false);
       }
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onGetLiveUserInfoDone(userInfoEvent);
       }
       break;
     case 31: 
       try
       {
         descMove = new JSONObject(new String(msg.getPayLoad())).optString("desc");
       } catch (JSONException je) {
         descMove = null;
       }
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onSendMoveDone(msg.getResultCode(), descMove);
       }
       break;
     case 66: 
       try
       {
         descStart = new JSONObject(new String(msg.getPayLoad())).optString("desc");
       } catch (JSONException je) {
         descStart = null;
       }
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onStartGameDone(msg.getResultCode(), descStart);
       }
       break;
     case 67: 
       try
       {
         descStop = new JSONObject(new String(msg.getPayLoad())).optString("desc");
       } catch (JSONException je) {
         descStop = null;
       }
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onStopGameDone(msg.getResultCode(), descStop);
       }
       break;
     case 68: 
       moves = com.taobao.lottery.game.multiplayer.client.events.MoveEvent.buildMoveHistoryArray(msg);
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onGetMoveHistoryDone(msg.getResultCode(), moves);
       }
       break;
     case 69: 
       if (msg.getResultCode() == 0) {
         try {
           JSONObject payload;	
           payload = new JSONObject(new String(msg.getPayLoad()));
           for (ZoneRequestListener listener : this.zoneRequestListeners)
             listener.onRPCDone(msg.getResultCode(), payload.getString("function"), payload.get("return"));
         } catch (JSONException e) {
           for (ZoneRequestListener listener : this.zoneRequestListeners) {
             listener.onRPCDone((byte)6, null, null);
           }
         }
       } else {
         for (ZoneRequestListener listener : this.zoneRequestListeners) {
           listener.onRPCDone(msg.getResultCode(), null, null);
         }
       }
       break;
     case 70: 
       if (msg.getResultCode() == 0) {
         try {
           JSONObject payload;	
           payload = new JSONObject(new String(msg.getPayLoad()));
           for (RoomRequestListener listener : this.roomRequestListeners)
             listener.onRPCDone(msg.getResultCode(), payload.getString("function"), payload.get("return"));
         } catch (JSONException e) {
           for (RoomRequestListener listener : this.roomRequestListeners) {
             listener.onRPCDone((byte)6, null, null);
           }
         }
       } else {
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onRPCDone(msg.getResultCode(), null, null);
         }
       }
       break;
     }
   }
   
   private void handleAuthenticateResponse(MMOGameResponseMessage msg)
   {
     try
     {
       JSONObject jsonAuth = new JSONObject(new String(msg.getPayLoad()));
       String desc = jsonAuth.getString("desc");
       if (msg.getResultCode() == 0) {
         this.sessionId = jsonAuth.getInt("sessionid");
         this.clientChannel.startKeepAlives();
         if (this.connectionState == 4) {
           this.connectionState = 0;
           fireConnectionEvent((byte)8, desc);
         }
         else {
           this.connectionState = 0;
           fireConnectionEvent((byte)0, desc);
         }
       }
       else {
         this.connectionState = 2;
         this.sessionId = 0;
         fireConnectionEvent((byte)1, desc);
       }
     } catch (JSONException ex) {
       this.connectionState = 2;
       Util.trace("exception " + ex + " in handleAuthenticationResponse " + ex.getMessage());
       fireConnectionEvent((byte)6, null);
       this.clientChannel.disconnect();
     }
   }
   
   void onNotify(com.taobao.lottery.game.multiplayer.client.message.MMOGameNotifyMessage msg) {
     try {
       JSONObject notifyData = null;
       com.taobao.lottery.game.multiplayer.client.events.RoomData roomData = null;
       com.taobao.lottery.game.multiplayer.client.events.LobbyData lobbyData = null;
       if (msg.getPayLoadType() == 2) {
         notifyData = new JSONObject(new String(msg.getPayLoad()));
       }
       switch (msg.getUpdateType())
       {
       case 9: 
         com.taobao.lottery.game.multiplayer.client.events.ChatEvent evt = com.taobao.lottery.game.multiplayer.client.events.ChatEvent.buildEvent(notifyData);
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onChatReceived(evt);
         }
         break;
       case 12: 
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onPrivateChatReceived(notifyData.optString("sender"), notifyData.optString("chat"));
         }
         break;
       case 1: 
         roomData = buildRoomData(notifyData);
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onRoomCreated(roomData);
         }
         break;
       case 2: 
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onRoomDestroyed(roomData);
         }
         break;
       case 10: 
         byte reserved = msg.getReserved();
         com.taobao.lottery.game.multiplayer.client.events.UpdateEvent event = new com.taobao.lottery.game.multiplayer.client.events.UpdateEvent(msg.getPayLoad(), (reserved & 0x2) > 0);
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onUpdatePeersReceived(event);
         }
         break;
       case 3: 
         lobbyData = buildLobbyData(notifyData);
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onUserJoinedLobby(lobbyData, notifyData.getString("user"));
         }
         break;
       case 5: 
         roomData = buildRoomData(notifyData);
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onUserJoinedRoom(roomData, notifyData.getString("user"));
         }
         break;
       case 4: 
         lobbyData = buildLobbyData(notifyData);
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onUserLeftLobby(lobbyData, notifyData.getString("user"));
         }
         break;
       case 6: 
         roomData = buildRoomData(notifyData);
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onUserLeftRoom(roomData, notifyData.getString("user"));
         }
         break;
       case 14: 
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onUserPaused(notifyData.getString("id"), notifyData.optBoolean("isLobby"), notifyData.getString("user"));
         }
         break;
       case 15: 
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onUserResumed(notifyData.getString("id"), notifyData.optBoolean("isLobby"), notifyData.getString("user"));
         }
         break;
       case 11: 
         roomData = buildRoomData(notifyData);
         HashMap<String, Object> properties = Util.getHashMapFromProperties(notifyData.getString("properties"));
         HashMap<String, String> lockProperties = Util.getHashMapFromProperties(notifyData.getString("lockProperties"));
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onUserChangeRoomProperty(roomData, notifyData.getString("sender"), properties, lockProperties);
         }
         break;
       case 13: 
         com.taobao.lottery.game.multiplayer.client.events.MoveEvent moveEvent = com.taobao.lottery.game.multiplayer.client.events.MoveEvent.buildEvent(notifyData);
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onMoveCompleted(moveEvent);
         }
         break;
       case 16:
         Log.e("onNotify",notifyData.toString());
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onGameStarted(notifyData.optString("sender"), notifyData.optString("id"), notifyData.optString("nextTurn"));
         }
         break;
       case 17: 
         for (int i = 0; i < this.notifyListeners.size(); i++) {
           ((NotifyListener)this.notifyListeners.get(i)).onGameStopped(notifyData.optString("sender"), notifyData.optString("id"));
         }
       }
       
     }
     catch (JSONException e)
     {
       Util.trace(e.getMessage());
     }
   }
   
   void onConnect(boolean success) {
     if (this.connectionTimer != null) {
       this.connectionTimer.cancel();
       this.connectionTimer = null;
       this.connectTimeoutTask = null;
     }
     if (!success) {
       if (this.udpListener != null) {
         this.udpListener.Stop();
       }
       this.udpListener = null;
       this.udpListenerThread = null;
     }
     if (success) {
       if (this.connectionState == 4) {
         sendAuthRequest(Util.userName, this.sessionId);
       }
       else {
         sendAuthRequest(Util.userName, 0);
       }
     }
     else if (this.connectionState == 3) {
       this.connectionState = 2;
       fireDisconnectEvent((byte)0);
     }
     else if (this.connectionState != 2) {
       this.connectionState = 2;
       if ((this.sessionId != 0) && (Util.RECOVERY_ALLOWANCE_TIME > 0)) {
         fireConnectionEvent((byte)9, null);
       }
       else {
         fireConnectionEvent((byte)5, null);
       }
     }
   }
   
 
 
 
 
 
 
   private void sendAuthRequest(String user, int sid)
   {
     try
     {
       JSONObject authObj = new JSONObject();
       String timeStamp = String.valueOf(System.currentTimeMillis());
       authObj.put("version", "Java_1.5");
       authObj.put("timeStamp", timeStamp);
       authObj.put("user", user);
       authObj.put("apiKey", this.apiKey);
       authObj.put("keepalive", 6);
       authObj.put("recoverytime", Util.RECOVERY_ALLOWANCE_TIME);
       authObj.put("authData", this.authData);
       MMOGameRequestMessage authMsg = new MMOGameRequestMessage((byte)1, sid, 0, (byte)0, (byte)0, (byte)2, authObj.toString().getBytes().length, authObj.toString().getBytes());
       
 
 
 
       this.clientChannel.SendRequest(authMsg);
     } catch (JSONException ex) {
       fireConnectionEvent((byte)4, null);
     }
   }
   
 
 
 
 
 
 
 
   public void setCustomRoomData(String roomid, String data)
   {
     if (isNotConnected()) {
       LiveRoomInfoEvent evt = new LiveRoomInfoEvent(null, (byte)5, null, null);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onSetCustomRoomDataDone(evt);
       }
       return; }
     if ((isNull(roomid)) || (isNull(data))) {
       LiveRoomInfoEvent evt = new LiveRoomInfoEvent(null, (byte)4, null, null);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onSetCustomRoomDataDone(evt);
       }
       return;
     }
     sendCustomDataRequest((byte)21, roomid, data);
   }
   
 
 
 
 
 
 
 
 
 
   public void setCustomUserData(String username, String data)
   {
     if (isNotConnected()) {
       LiveUserInfoEvent evt = new LiveUserInfoEvent((byte)5, null, null, null, false, false);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onSetCustomUserDataDone(evt);
       }
       return; }
     if ((isNull(username)) || (isNull(data))) {
       LiveUserInfoEvent evt = new LiveUserInfoEvent((byte)4, null, null, null, false, false);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onSetCustomUserDataDone(evt);
       }
       return;
     }
     sendCustomDataRequest((byte)22, username, data);
   }
   
 
 
 
 
 
   public void getOnlineUsers()
   {
     if (isNotConnected()) {
       com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent allUsersEvent = new com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent((byte)5, null);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onGetOnlineUsersDone(allUsersEvent);
       }
       return;
     }
     
     MMOGameRequestMessage msg = new MMOGameRequestMessage((byte)18, this.sessionId, 0, (byte)0, (byte)0, (byte)0, "".getBytes().length, "".getBytes());
     
 
 
 
     this.clientChannel.SendRequest(msg);
   }
   
 
 
 
 
 
   public void getAllRooms()
   {
     if (isNotConnected()) {
       com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent allRoomsEvent = new com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent((byte)5, null);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onGetAllRoomsDone(allRoomsEvent);
       }
       return;
     }
     MMOGameRequestMessage msg = new MMOGameRequestMessage((byte)17, this.sessionId, 0, (byte)0, (byte)0, (byte)0, "".getBytes().length, "".getBytes());
     
 
 
 
     this.clientChannel.SendRequest(msg);
   }
   
 
 
 
 
 
 
 
   public void getLiveUserInfo(String username)
   {
     if (isNotConnected()) {
       LiveUserInfoEvent evt = new LiveUserInfoEvent((byte)5, null, null, null, false, false);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onGetLiveUserInfoDone(evt);
       }
       return;
     }
     if (isNull(username)) {
       LiveUserInfoEvent evt = new LiveUserInfoEvent((byte)4, null, null, null, false, false);
       for (ZoneRequestListener listener : this.zoneRequestListeners)
         listener.onGetLiveUserInfoDone(evt);
       return;
     }
     LiveUserInfoEvent evt;
     try {
       JSONObject roomObj = new JSONObject();
       roomObj.put("name", username);
       
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)19, this.sessionId, 0, (byte)0, (byte)0, (byte)2, roomObj.toString().getBytes().length, roomObj.toString().getBytes());
       
 
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) {
       evt = new LiveUserInfoEvent((byte)4, null, null, null, false, false);
       
 
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onGetLiveUserInfoDone(evt);
       }
     }
   }
   
 
 
 
 
 
 
   public void getLiveRoomInfo(String roomid)
   {
     if (isNotConnected()) {
       LiveRoomInfoEvent event = new LiveRoomInfoEvent(null, (byte)5, null, null);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onGetLiveRoomInfoDone(event);
       }
       return;
     }
     if (isNull(roomid)) {
       LiveRoomInfoEvent event = new LiveRoomInfoEvent(null, (byte)4, null, null);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onGetLiveRoomInfoDone(event);
       }
       return;
     }
     sendRoomRequest((byte)20, roomid);
   }
   
 
 
 
 
   public void getLiveLobbyInfo()
   {
     if (isNotConnected()) {
       LiveRoomInfoEvent liveRoomInfoEvent = new LiveRoomInfoEvent(null, (byte)5, null, null);
       
 
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onGetLiveLobbyInfoDone(liveRoomInfoEvent);
       }
       return;
     }
     sendLobbyRequestMessage((byte)23);
   }
   
 
 
 
 
 
   public void joinLobby()
   {
     if (isNotConnected()) {
       LobbyEvent errEvent = new LobbyEvent(null, (byte)5);
       
 
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onJoinLobbyDone(errEvent);
       }
       return;
     }
     
     sendLobbyRequestMessage((byte)2);
   }
   
 
 
 
 
 
 
   public void leaveLobby()
   {
     if (isNotConnected()) {
       LobbyEvent errEvent = new LobbyEvent(null, (byte)5);
       
 
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onLeaveLobbyDone(errEvent);
       }
       return;
     }
     
     sendLobbyRequestMessage((byte)5);
   }
   
 
 
 
 
   public void subscribeLobby()
   {
     if (isNotConnected()) {
       LobbyEvent errEvent = new LobbyEvent(null, (byte)5);
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onSubscribeLobbyDone(errEvent);
       }
       return;
     }
     sendLobbyRequestMessage((byte)3);
   }
   
 
 
 
 
   public void unsubscribeLobby()
   {
     if (isNotConnected()) {
       LobbyEvent errEvent = new LobbyEvent(null, (byte)5);
       for (LobbyRequestListener listener : this.lobbyRequestListeners) {
         listener.onUnSubscribeLobbyDone(errEvent);
       }
       return;
     }
     sendLobbyRequestMessage((byte)4);
   }
   
 
 
 
 
 
 
 
 
 
   public void createRoom(String name, String owner, int maxUsers, HashMap<String, Object> tableProperties)
   {
     JSONObject properties = new JSONObject();
     if ((tableProperties != null) && (tableProperties.size() > 0)) {
       properties = Util.getJsonObjectFromHashtable(tableProperties);
     }
     if (properties.toString().getBytes().length > 2048) {
       RoomEvent evt = new RoomEvent(null, (byte)7);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onCreateRoomDone(evt, null);
       }
       return;
     }
     if (isNotConnected()) {
       RoomEvent evt = new RoomEvent(null, (byte)5);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onCreateRoomDone(evt, null);
       }
       return; }
     if ((isNull(name)) || (isNull(owner)) || (isNegativeOrZero(maxUsers))) {
       RoomEvent evt = new RoomEvent(null, (byte)4);
       for (ZoneRequestListener listener : this.zoneRequestListeners)
         listener.onCreateRoomDone(evt, null);
       return;
     }
     RoomEvent evt;
     try {
       JSONObject roomObj = new JSONObject();
       roomObj.put("maxUsers", maxUsers);
       roomObj.put("owner", owner);
       roomObj.put("name", name);
       roomObj.put("properties", properties);
       
       MMOGameRequestMessage roomCreateMsg = new MMOGameRequestMessage((byte)6, this.sessionId, 0, (byte)0, (byte)0, (byte)2, roomObj.toString().getBytes().length, roomObj.toString().getBytes());
       
 
 
 
 
       this.clientChannel.SendRequest(roomCreateMsg);
     } catch (JSONException ex) {
       evt = new RoomEvent(null, (byte)4);
       
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onCreateRoomDone(evt, null);
       }
     }
   }
   
 
 
 
 
 
 
 
 
 
 
   public void createTurnRoom(String name, String owner, int maxUsers, HashMap<String, Object> tableProperties, int time)
   {
     JSONObject properties = new JSONObject();
     if ((tableProperties != null) && (tableProperties.size() > 0)) {
       properties = Util.getJsonObjectFromHashtable(tableProperties);
     }
     if (properties.toString().getBytes().length > 2048) {
       RoomEvent evt = new RoomEvent(null, (byte)7);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onCreateRoomDone(evt, null);
       }
       return;
     }
     if (isNotConnected()) {
       RoomEvent evt = new RoomEvent(null, (byte)5);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onCreateRoomDone(evt, null);
       }
       return; }
     if ((isNull(name)) || (isNull(owner)) || (isNegativeOrZero(maxUsers))) {
       RoomEvent evt = new RoomEvent(null, (byte)4);
       for (ZoneRequestListener listener : this.zoneRequestListeners)
         listener.onCreateRoomDone(evt, null);
       return;
     }
     RoomEvent evt;
     try {
       JSONObject roomObj = new JSONObject();
       roomObj.put("maxUsers", maxUsers);
       roomObj.put("owner", owner);
       roomObj.put("name", name);
       roomObj.put("turnTime", time);
       roomObj.put("inox", true);
       roomObj.put("properties", properties);
       
       MMOGameRequestMessage roomCreateMsg = new MMOGameRequestMessage((byte)6, this.sessionId, 0, (byte)0, (byte)0, (byte)2, roomObj.toString().getBytes().length, roomObj.toString().getBytes());
       
 
 
 
 
       this.clientChannel.SendRequest(roomCreateMsg);
     } catch (JSONException ex) {
       evt = new RoomEvent(null, (byte)4);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onCreateRoomDone(evt, null);
       }
     }
   }
   
 
 
 
 
   public void sendMove(String moveData)
   {
     if (isNotConnected()) {
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onSendMoveDone((byte)5, null);
       }
       return; }
     if ((moveData == null) || (moveData.length() > 500)) {
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onSendMoveDone((byte)4, null);
       }
       return;
     }
     try {
       JSONObject chatObj = new JSONObject();
       chatObj.put("moveData", moveData);
       
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)31, this.sessionId, 0, (byte)0, (byte)0, (byte)2, chatObj.toString().getBytes().length, chatObj.toString().getBytes());
       
 
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) {
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onSendMoveDone((byte)6, null);
       }
     }
   }
   
 
 
 
 
   public void startGame()
   {
     if (isNotConnected()) {
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onStartGameDone((byte)5, null);
       }
       return;
     }
     try {
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)66, this.sessionId, 0, (byte)0, (byte)0, (byte)0, "".getBytes().length, "".getBytes());
       
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (Exception ex) {
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onStartGameDone((byte)6, null);
       }
     }
   }
   
 
 
 
 
 
   public void stopGame()
   {
     if (isNotConnected()) {
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onStopGameDone((byte)5, null);
       }
       return;
     }
     try {
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)67, this.sessionId, 0, (byte)0, (byte)0, (byte)0, "".getBytes().length, "".getBytes());
       
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (Exception ex) {
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onStopGameDone((byte)6, null);
       }
     }
   }
   
 
 
 
   public void getMoveHistory()
   {
     if (isNotConnected()) {
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onGetMoveHistoryDone((byte)5, null);
       }
       return;
     }
     try {
       JSONObject chatObj = new JSONObject();
       chatObj.put("count", 5);
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)68, this.sessionId, 0, (byte)0, (byte)0, (byte)2, chatObj.toString().getBytes().length, chatObj.toString().getBytes());
       
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) {
       for (TurnBasedRoomListener listener : this.turnBasedRoomListeners) {
         listener.onGetMoveHistoryDone((byte)6, null);
       }
     }
   }
   
 
 
 
 
 
   public void deleteRoom(String roomId)
   {
     if (isNotConnected()) {
       RoomEvent evt = new RoomEvent(null, (byte)5);
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onDeleteRoomDone(evt, null);
       }
       return;
     }
     sendRoomRequest((byte)11, roomId);
   }
   
 
 
 
 
 
   public void joinRoom(String roomId)
   {
     if (isNotConnected()) {
       RoomEvent errEvent = new RoomEvent(null, (byte)5);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onJoinRoomDone(errEvent, null);
       }
       return;
     }
     if (isNull(roomId)) {
       RoomEvent errEvent = new RoomEvent(null, (byte)4);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onJoinRoomDone(errEvent, null);
       }
       return;
     }
     
     sendRoomRequest((byte)7, roomId);
   }
   
 
 
 
 
 
 
 
 
 
 
   public void joinRoomInRange(int minUser, int maxUser, boolean maxPreferred)
   {
     byte errorCode = 0;
     RoomEvent event;
     try { if (isNotConnected()) {
         errorCode = 5;
       }
       if ((minUser < 0) || (maxUser < 0) || (minUser > maxUser)) {
         errorCode = 4;
       }
       if (errorCode == 0)
         sendRangeMatchMakingRequest((byte)37, minUser, maxUser, maxPreferred);
     } catch (JSONException ex) { 
       errorCode = 6; } finally { 
       if (errorCode != 0) {
         event = new RoomEvent(null, errorCode);
         for (RoomRequestListener listener : this.roomRequestListeners)
           listener.onJoinRoomDone(event, null);
       }
     }
   }
   
   public void updateRoomProperties(String roomID, HashMap<String, Object> tableProperties, String[] removeArray) {
     LiveRoomInfoEvent liveRoomInfoEvent;
     if (isNotConnected()) {
       liveRoomInfoEvent = new LiveRoomInfoEvent(null, (byte)5, null, null);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onUpdatePropertyDone(liveRoomInfoEvent, null);
       }
     } else {
       JSONObject properties = null;
       String removeProperties = "";
       if ((tableProperties != null) && (tableProperties.size() > 0)) {
         properties = Util.getJsonObjectFromHashtable(tableProperties);
       }
       if (removeArray != null) {
         removeProperties = "";
         for (int i = 0; i < removeArray.length; i++) {
           if (i < removeArray.length - 1) {
             removeProperties = removeProperties.concat(removeArray[i] + ";");
           } else {
             removeProperties = removeProperties.concat(removeArray[i]);
           }
         }
       }
       updateRoomPropertiesRequest((byte)25, roomID, properties, removeProperties);
     }
   }
   
   public void lockProperties(HashMap<String, Object> tableProperties) {
     if (isNotConnected()) {
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onLockPropertiesDone((byte)5);
       }
     } else {
       JSONObject properties = null;
       if ((tableProperties != null) && (tableProperties.size() > 0)) {
         properties = Util.getJsonObjectFromHashtable(tableProperties);
       }
       sendLockPropertiesRequest((byte)35, properties);
     }
   }
   
   public void unlockProperties(String[] unlockProperties) {
     if (isNotConnected()) {
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onUnlockPropertiesDone((byte)5);
       }
     } else {
       String unlock = "";
       if (unlockProperties != null) {
         unlock = "";
         for (int i = 0; i < unlockProperties.length; i++) {
           if (i < unlockProperties.length - 1) {
             unlock = unlock.concat(unlockProperties[i] + ";");
           } else {
             unlock = unlock.concat(unlockProperties[i]);
           }
         }
       }
       sendUnlockPropertiesRequest((byte)36, unlock);
     }
   }
   
   public void joinRoomWithProperties(HashMap<String, Object> tableProperties) {
     byte errorCode = 0;
     RoomEvent event;
     try { if (isNotConnected()) {
         errorCode = 5;
       }
       if (errorCode == 0)
         sendPropertyMatchMakingRequest((byte)27, tableProperties);
     } catch (JSONException ex) { 
       errorCode = 6; } finally { 
       if (errorCode != 0) {
         event = new RoomEvent(null, errorCode);
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onJoinRoomDone(event, null);
         }
       }
     }
   }
   
 
 
 
 
 
 
 
   public void getRoomInRange(int minUser, int maxUsers)
   {
     byte errorCode = 0;
     MatchedRoomsEvent event;
     try { if (isNotConnected()) {
         errorCode = 5;
       }
       if ((minUser < 0) || (maxUsers < 0) || (minUser > maxUsers)) {
         errorCode = 4;
       }
       if (errorCode == 0)
         sendRangeMatchMakingRequest((byte)38, minUser, maxUsers, false);
     } catch (JSONException ex) { 
       errorCode = 6; } finally { 
       if (errorCode != 0) {
         event = new MatchedRoomsEvent(errorCode, null);
         for (ZoneRequestListener listener : this.zoneRequestListeners) {
           listener.onGetMatchedRoomsDone(event);
         }
       }
     }
   }
   
 
 
 
 
 
 
   public void getRoomWithProperties(HashMap<String, Object> properties)
   {
     byte errorCode = 0;
     MatchedRoomsEvent event;
     try { if (isNotConnected()) {
         errorCode = 5;
       }
       if (errorCode == 0)
         sendPropertyMatchMakingRequest((byte)29, properties);
     } catch (JSONException ex) { 
       errorCode = 6; } finally { 
       if (errorCode != 0) {
         event = new MatchedRoomsEvent(errorCode, null);
         for (ZoneRequestListener listener : this.zoneRequestListeners) {
           listener.onGetMatchedRoomsDone(event);
         }
       }
     }
   }
   
 
 
 
 
 
 
   public void leaveRoom(String roomId)
   {
     if (isNotConnected()) {
       RoomEvent errEvent = new RoomEvent(null, (byte)5);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onLeaveRoomDone(errEvent);
       }
       return; }
     if (isNull(roomId)) {
       RoomEvent errEvent = new RoomEvent(null, (byte)4);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onLeaveRoomDone(errEvent);
       }
       return;
     }
     sendRoomRequest((byte)10, roomId);
   }
   
 
 
 
 
 
   public void subscribeRoom(String roomId)
   {
     if (isNotConnected()) {
       RoomEvent errEvent = new RoomEvent(null, (byte)5);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onSubscribeRoomDone(errEvent);
       }
       return;
     }
     
     sendRoomRequest((byte)8, roomId);
   }
   
 
 
 
 
 
   public void unsubscribeRoom(String roomId)
   {
     if (isNotConnected()) {
       RoomEvent errEvent = new RoomEvent(null, (byte)5);
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onUnSubscribeRoomDone(errEvent);
       }
       return;
     }
     sendRoomRequest((byte)9, roomId);
   }
   
 
 
 
 
 
 
 
   public void sendChat(String message)
   {
     if (isNotConnected()) {
       for (ChatRequestListener listener : this.chatRequestListeners) {
         listener.onSendChatDone((byte)5, null);
       }
       return; }
     if ((message == null) || (message.length() > 500)) {
       for (ChatRequestListener listener : this.chatRequestListeners) {
         listener.onSendChatDone((byte)4, null);
       }
       return;
     }
     try {
       JSONObject chatObj = new JSONObject();
       chatObj.put("chat", message);
       
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)12, this.sessionId, 0, (byte)0, (byte)0, (byte)2, chatObj.toString().getBytes().length, chatObj.toString().getBytes());
       
 
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) {
       for (ChatRequestListener listener : this.chatRequestListeners) {
         listener.onSendChatDone((byte)4, null);
       }
     }
   }
   
 
 
 
 
 
 
 
   public void invokeZoneRPC(String functionName, Object... args)
   {
     if (isNotConnected()) {
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onRPCDone((byte)5, null, null);
       }
       return;
     }
     try {
       org.json.JSONArray argsArray = new org.json.JSONArray();
       for (Object obj : args) {
         argsArray.put(obj);
       }
       JSONObject chatObj = new JSONObject();
       chatObj.put("function", functionName);
       chatObj.put("args", argsArray);
       
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)69, this.sessionId, 0, (byte)0, (byte)0, (byte)2, chatObj.toString().getBytes().length, chatObj.toString().getBytes());
       
 
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) {
       for (ZoneRequestListener listener : this.zoneRequestListeners) {
         listener.onRPCDone((byte)4, null, null);
       }
     }
   }
   
 
 
 
 
 
 
 
 
   public void invokeRoomRPC(String roomId, String functionName, Object... args)
   {
     if (isNotConnected()) {
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onRPCDone((byte)5, null, null);
       }
       return;
     }
     try {
       org.json.JSONArray argsArray = new org.json.JSONArray();
       for (Object obj : args) {
         argsArray.put(obj);
       }
       JSONObject chatObj = new JSONObject();
       chatObj.put("roomId", roomId);
       chatObj.put("function", functionName);
       chatObj.put("args", argsArray);
       
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)70, this.sessionId, 0, (byte)0, (byte)0, (byte)2, chatObj.toString().getBytes().length, chatObj.toString().getBytes());
       
 
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) {
       for (RoomRequestListener listener : this.roomRequestListeners) {
         listener.onRPCDone((byte)4, null, null);
       }
     }
   }
   
 
 
 
 
 
 
 
 
   public void sendPrivateChat(String userName, String message)
   {
     if (isNotConnected()) {
       for (ChatRequestListener listener : this.chatRequestListeners) {
         listener.onSendPrivateChatDone((byte)5, null);
       }
       return; }
     if ((message == null) || (message.length() > 500)) {
       for (ChatRequestListener listener : this.chatRequestListeners) {
         listener.onSendPrivateChatDone((byte)4, null);
       }
       return;
     }
     try {
       JSONObject chatObj = new JSONObject();
       chatObj.put("to", userName);
       chatObj.put("chat", message);
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)30, this.sessionId, 0, (byte)0, (byte)0, (byte)2, chatObj.toString().getBytes().length, chatObj.toString().getBytes());
       
 
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) {
       for (ChatRequestListener listener : this.chatRequestListeners) {
         listener.onSendPrivateChatDone((byte)4, null);
       }
     }
   }
   
 
 
 
 
 
 
   public void sendUDPUpdatePeers(byte[] update)
   {
     if ((isNotConnected()) || (this.udpListener == null) || (update.length > 1000)) {
       return;
     }
     try {
       MMOGameRequestMessage updateMsg = new MMOGameRequestMessage((byte)13, this.sessionId, 0, (byte)0, (byte)2, (byte)1, update.length, update);
       
 
 
 
       this.udpListener.SendRequest(updateMsg);
     }
     catch (Exception ex) {
       Util.trace(ex.getMessage());
     }
   }
   
   public void initUDP() {
     if (isNotConnected()) {
       Util.trace("Can't initUDP till connected successfully");
       fireUDPEvent((byte)5);
       return;
     }
     if (this.udpListener != null) {
       Util.trace("udpListener already exists");
       fireUDPEvent((byte)4);
       return;
     }
     this.udpListener = new UDPListener();
     this.udpListenerThread = new Thread(this.udpListener);
     try {
       MMOGameRequestMessage updateMsg = new MMOGameRequestMessage((byte)64, this.sessionId, 0, (byte)0, (byte)2, (byte)1, 0, null);
       
 
 
 
       this.udpListener.SendRequest(updateMsg);
       this.udpListenerThread.start();
     } catch (Exception ex) {
       Util.trace(ex.getMessage());
     }
   }
   
 
 
 
 
 
 
   public void sendUpdatePeers(byte[] update)
   {
     if (isNotConnected()) {
       for (UpdateRequestListener listener : this.updateRequestListeners) {
         listener.onSendUpdateDone((byte)5);
       }
       return; }
     if (update.length > 1000) {
       for (UpdateRequestListener listener : this.updateRequestListeners) {
         listener.onSendUpdateDone((byte)4);
       }
       return;
     }
     try {
       MMOGameRequestMessage updateMsg = new MMOGameRequestMessage((byte)13, this.sessionId, 0, (byte)0, (byte)0, (byte)1, update.length, update);
       
 
 
 
       this.clientChannel.SendRequest(updateMsg);
     } catch (Exception ex) {
       for (UpdateRequestListener listener : this.updateRequestListeners) {
         listener.onSendUpdateDone((byte)6);
       }
     }
   }
   
   private void sendLobbyRequestMessage(byte code) {
     LobbyEvent errEvent;
     try {
       JSONObject lobbyObj = new JSONObject();
       lobbyObj.put("isPrimary", true);
       
       MMOGameRequestMessage roomCreateMsg = new MMOGameRequestMessage(code, this.sessionId, 0, (byte)0, (byte)0, (byte)2, lobbyObj.toString().getBytes().length, lobbyObj.toString().getBytes());
       
 
 
 
       this.clientChannel.SendRequest(roomCreateMsg);
     }
     catch (JSONException ex) {
       errEvent = new LobbyEvent(null, (byte)4);
       
       if (code == 2) {
         for (LobbyRequestListener listener : this.lobbyRequestListeners) {
           listener.onJoinLobbyDone(errEvent);
         }
       }
       if (code == 5) {
         for (LobbyRequestListener listener : this.lobbyRequestListeners) {
           listener.onLeaveLobbyDone(errEvent);
         }
       }
       if (code == 3) {
         for (LobbyRequestListener listener : this.lobbyRequestListeners) {
           listener.onSubscribeLobbyDone(errEvent);
         }
       }
       if (code == 4) {
         for (LobbyRequestListener listener : this.lobbyRequestListeners)
           listener.onUnSubscribeLobbyDone(errEvent);
       }
     }
   }
   
   private void sendCustomDataRequest(byte code, String key, String data) {
     LiveUserInfoEvent evt;
     try {
       JSONObject reqObj = new JSONObject();
       if (code == 21) {
         reqObj.put("id", key);
       } else {
         reqObj.put("name", key);
       }
       reqObj.put("data", data);
       
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage(code, this.sessionId, 0, (byte)0, (byte)0, (byte)2, reqObj.toString().getBytes().length, reqObj.toString().getBytes());
       
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) { 
       if (code == 21) {
         LiveRoomInfoEvent evtRoom = new LiveRoomInfoEvent(null, (byte)4, null, null);
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onSetCustomRoomDataDone(evtRoom);
         }
       } else if (code == 22) {
         evt = new LiveUserInfoEvent((byte)4, null, null, null, false, false);
         
 
         for (ZoneRequestListener listener : this.zoneRequestListeners)
           listener.onSetCustomUserDataDone(evt);
       }
     }
   }
   
   private void sendRoomRequest(byte code, String id) {
     LiveRoomInfoEvent event;
     try {
       JSONObject roomObj = new JSONObject();
       roomObj.put("id", id);
       
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage(code, this.sessionId, 0, (byte)0, (byte)0, (byte)2, roomObj.toString().getBytes().length, roomObj.toString().getBytes());
       
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     }
     catch (JSONException ex) {
       RoomEvent errEvent = new RoomEvent(null, (byte)4);
       if (code == 7) {
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onJoinRoomDone(errEvent, null);
         }
       }
       if (code == 10) {
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onLeaveRoomDone(errEvent);
         }
       }
       if (code == 8) {
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onSubscribeRoomDone(errEvent);
         }
       }
       if (code == 9) {
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onUnSubscribeRoomDone(errEvent);
         }
       }
       if (code == 20) {
         event = new LiveRoomInfoEvent(null, (byte)4, null, null);
         
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onGetLiveRoomInfoDone(event);
         }
       }
     }
   }
   
   private void sendRangeMatchMakingRequest(byte code, int minUser, int maxUser, boolean maxPreferred)
     throws JSONException
   {
     JSONObject roomObj = new JSONObject();
     roomObj.put("minUsers", minUser);
     roomObj.put("maxUsers", maxUser);
     roomObj.put("maxPreferred", maxPreferred);
     MMOGameRequestMessage roomMsg = new MMOGameRequestMessage(code, this.sessionId, 0, (byte)0, (byte)0, (byte)2, roomObj.toString().getBytes().length, roomObj.toString().getBytes());
     
 
 
 
 
 
 
 
 
     this.clientChannel.SendRequest(roomMsg);
   }
   
   private void sendPropertyMatchMakingRequest(byte code, HashMap<String, Object> tableProperties) throws JSONException
   {
     JSONObject properties = null;
     if ((tableProperties != null) && (tableProperties.size() > 0)) {
       properties = Util.getJsonObjectFromHashtable(tableProperties);
     }
     JSONObject roomObj = new JSONObject();
     roomObj.put("properties", properties);
     MMOGameRequestMessage roomMsg = new MMOGameRequestMessage(code, this.sessionId, 0, (byte)0, (byte)0, (byte)2, roomObj.toString().getBytes().length, roomObj.toString().getBytes());
     
 
 
 
 
 
 
 
 
     this.clientChannel.SendRequest(roomMsg);
   }
   
   private void updateRoomPropertiesRequest(byte code, String id, JSONObject property, String removeProperties) {
     LiveRoomInfoEvent liveRoomInfoEvent;
     try { JSONObject roomObj = new JSONObject();
       roomObj.put("id", id);
       roomObj.put("addOrUpdate", property);
       roomObj.put("remove", removeProperties);
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage(code, this.sessionId, 0, (byte)0, (byte)0, (byte)2, roomObj.toString().getBytes().length, roomObj.toString().getBytes());
       
 
 
 
 
 
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (Exception ex) {
       if (code == 25) {
         liveRoomInfoEvent = new LiveRoomInfoEvent(null, (byte)4, null, null);
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onUpdatePropertyDone(liveRoomInfoEvent, null);
         }
       }
     }
   }
   
   private void sendLockPropertiesRequest(byte code, JSONObject property) {
     try {
       JSONObject roomObj = new JSONObject();
       roomObj.put("lockProperties", property);
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage(code, this.sessionId, 0, (byte)0, (byte)0, (byte)2, roomObj.toString().getBytes().length, roomObj.toString().getBytes());
       
 
 
 
 
 
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) {
       if (code == 35) {
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onLockPropertiesDone((byte)4);
         }
       }
     }
   }
   
   private void sendUnlockPropertiesRequest(byte code, String unlockProperties) {
     try {
       JSONObject roomObj = new JSONObject();
       roomObj.put("unlockProperties", unlockProperties);
       MMOGameRequestMessage roomMsg = new MMOGameRequestMessage(code, this.sessionId, 0, (byte)0, (byte)0, (byte)2, roomObj.toString().getBytes().length, roomObj.toString().getBytes());
       
 
 
 
 
 
 
 
 
       this.clientChannel.SendRequest(roomMsg);
     } catch (JSONException ex) {
       if (code == 36) {
         for (RoomRequestListener listener : this.roomRequestListeners) {
           listener.onUnlockPropertiesDone((byte)4);
         }
       }
     }
   }
   
   private com.taobao.lottery.game.multiplayer.client.events.RoomData buildRoomData(JSONObject objData) throws JSONException {
     String id = objData.getString("id");
     String name = objData.getString("name");
     String owner = objData.getString("owner");
     int max = objData.getInt("maxUsers");
     return new com.taobao.lottery.game.multiplayer.client.events.RoomData(id, owner, name, max);
   }
   
   private com.taobao.lottery.game.multiplayer.client.events.LobbyData buildLobbyData(JSONObject objData) throws JSONException {
     String id = objData.getString("id");
     String name = objData.getString("name");
     String owner = objData.getString("owner");
     int max = objData.getInt("maxUsers");
     return new com.taobao.lottery.game.multiplayer.client.events.LobbyData(id, owner, name, max, true);
   }
   
   private void fireConnectionEvent(byte resultCode, String desc)
   {
     com.taobao.lottery.game.multiplayer.client.events.ConnectEvent event = new com.taobao.lottery.game.multiplayer.client.events.ConnectEvent(resultCode);
     Object[] listenersCopy = this.ConnectionRequestListeners.toArray();
     for (Object listenerCopy : listenersCopy) {
       ConnectionRequestListener listener = (ConnectionRequestListener)listenerCopy;
       if (this.ConnectionRequestListeners.contains(listener)) {
         listener.onConnectDone(event, desc);
       }
     }
   }
   
   void cleanupUdpListener() {
     if (this.udpListener != null) {
       this.udpListener.Stop();
     }
     this.udpListener = null;
     this.udpListenerThread = null;
   }
   
   protected void fireUDPEvent(byte resultCode) {
     Object[] listenersCopy = this.ConnectionRequestListeners.toArray();
     for (Object listenerCopy : listenersCopy) {
       ConnectionRequestListener listener = (ConnectionRequestListener)listenerCopy;
       if (this.ConnectionRequestListeners.contains(listener)) {
         listener.onInitUDPDone(resultCode);
       }
     }
   }
   
   private void fireDisconnectEvent(byte resultCode) {
     com.taobao.lottery.game.multiplayer.client.events.ConnectEvent event = new com.taobao.lottery.game.multiplayer.client.events.ConnectEvent(resultCode);
     Object[] listenersCopy = this.ConnectionRequestListeners.toArray();
     for (Object listenerCopy : listenersCopy) {
       ConnectionRequestListener listener = (ConnectionRequestListener)listenerCopy;
       if (this.ConnectionRequestListeners.contains(listener)) {
         listener.onDisconnectDone(event);
       }
     }
   }
   
   private boolean isNull(String check) {
     return check == null;
   }
   
   private boolean isNotConnected() {
     return getConnectionState() != 0;
   }
   
   private boolean isNegativeOrZero(int value) {
     return value <= 0;
   }
   
   private void startChannelConnectTimer() {
     this.connectTimeoutTask = new ConnectionWatchTask(this);
     this.connectionTimer = new java.util.Timer();
     this.connectionTimer.schedule(this.connectTimeoutTask, 6000L);
   }
   
   public void onLookUpServer(byte lookUpaStatus) {
     if (lookUpaStatus == 0) {
       startChannelConnectTimer();
       this.clientChannel = new ClientChannel(Util.MMOGameServerHost, Util.MMOGameServerPort);
       Thread t = new Thread(this.clientChannel);
       t.setDaemon(false);
       t.start();
     } else {
       this.connectionState = 2;
       fireConnectionEvent(lookUpaStatus, null);
     }
   }
   
   private class ConnectionWatchTask extends java.util.TimerTask
   {
     MMOGameClient owner;
     
     ConnectionWatchTask(MMOGameClient client) {
       this.owner = client;
     }
     
     public void run()
     {
       if (this.owner.connectionState != 0) {
         byte result = 5;
         if (MMOGameClient.this.connectionState == 4) {
           result = 9;
         }
         this.owner.connectionState = 2;
         this.owner.fireConnectionEvent(result, null);
         this.owner.clientChannel.disconnect();
       }
     }
   }
 }


