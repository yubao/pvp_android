package com.taobao.lottery.game.multiplayer.client.listener;

import com.taobao.lottery.game.multiplayer.client.events.MoveEvent;

public abstract interface TurnBasedRoomListener
{
  public abstract void onSendMoveDone(byte paramByte, String paramString);
  
  public abstract void onStartGameDone(byte paramByte, String paramString);
  
  public abstract void onStopGameDone(byte paramByte, String paramString);
  
  public abstract void onGetMoveHistoryDone(byte paramByte, MoveEvent[] paramArrayOfMoveEvent);
}


