 package com.taobao.lottery.game.multiplayer.client.events;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;
import com.taobao.lottery.game.multiplayer.client.util.Util;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class LiveRoomInfoEvent
   extends RoomEvent
 {
   private String[] joinedUsers;
   private String customData;
   private HashMap<String, Object> properties;
   private HashMap<String, String> lockPropeties;
   
   public LiveRoomInfoEvent(RoomData data, byte result, String[] users, String customData)
   {
     super(data, result);
     this.joinedUsers = users;
     this.customData = customData;
   }
   
 
 
 
 
 
 
   public LiveRoomInfoEvent(RoomData data, byte result, String[] users, String customData, String properties, String lockProperties)
   {
     super(data, result);
     this.joinedUsers = users;
     this.customData = customData;
     if (properties != null) {
       buildProperties(properties);
     }
     if (lockProperties != null) {
       buildLockProperties(lockProperties);
     }
   }
   
   private void buildProperties(String input) {
     if (this.properties == null) {
       this.properties = new HashMap();
     }
     this.properties.clear();
     try {
       JSONObject jSONObject = new JSONObject(input);
       Iterator it = jSONObject.keys();
       while (it.hasNext()) {
         String key = it.next().toString();
         this.properties.put(key, jSONObject.get(key));
       }
     } catch (Exception e) {
       Util.trace("buildProperties: " + e);
     }
   }
   
   private void buildLockProperties(String input) {
     if (this.lockPropeties == null) {
       this.lockPropeties = new HashMap();
     }
     this.lockPropeties.clear();
     try {
       JSONObject jSONObject = new JSONObject(input);
       Iterator it = jSONObject.keys();
       while (it.hasNext()) {
         String key = it.next().toString();
         this.lockPropeties.put(key, jSONObject.get(key).toString());
       }
     } catch (Exception e) {
       Util.trace("buildLockProperties: " + e);
     }
   }
   
 
 
 
   public String[] getJoinedUsers()
   {
     return this.joinedUsers;
   }
   
 
 
 
   public String getCustomData()
   {
     return this.customData;
   }
   
 
 
 
   public HashMap<String, Object> getProperties()
   {
     return this.properties;
   }
   
 
 
 
   public HashMap<String, String> getLockProperties()
   {
     return this.lockPropeties;
   }
   
 
 
 
 
 
   public static LiveRoomInfoEvent buildLiveRoomEvent(MMOGameResponseMessage msg)
     throws JSONException
   {
     RoomData roomInfo = null;
     String customData = "";
     String properties = null;
     String lockProperties = null;
     String[] usernames = null;
     if (msg.getResultCode() == 0)
     {
       JSONObject jsonRoom = new JSONObject(new String(msg.getPayLoad()));
       roomInfo = new RoomData(jsonRoom.getString("id"), jsonRoom.getString("owner"), jsonRoom.getString("name"), jsonRoom.getInt("maxUsers"));
       
       String jsonUsernames = jsonRoom.getString("usernames");
       if (jsonUsernames.length() > 0) {
         usernames = jsonUsernames.split(";");
       }
       customData = jsonRoom.getString("data");
       properties = jsonRoom.getString("properties");
       lockProperties = jsonRoom.getString("lockProperties");
     }
     return new LiveRoomInfoEvent(roomInfo, msg.getResultCode(), usernames, customData, properties, lockProperties);
   }
 }


