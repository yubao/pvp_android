 package com.taobao.lottery.game.multiplayer.client.events;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class MatchedRoomsEvent
 {
   private byte result;
   private RoomData[] roomData;
   
   public MatchedRoomsEvent(byte result, RoomData[] roomData)
   {
     this.roomData = roomData;
     this.result = result;
   }
   
 
 
 
   public byte getResult()
   {
     return this.result;
   }
   
 
 
 
   public RoomData[] getRoomsData()
   {
     return this.roomData;
   }
   
 
 
 
 
   public static MatchedRoomsEvent buildMatchedRoomsEvent(MMOGameResponseMessage msg)
     throws JSONException
   {
     ArrayList<RoomData> roomsDataList = new ArrayList();
     if (msg.getResultCode() == 0)
     {
       JSONObject roomData = new JSONObject(new String(msg.getPayLoad()));
       Iterator it = roomData.keys();
       while (it.hasNext()) {
         String key = it.next().toString();
         JSONObject jsonRoom = (JSONObject)roomData.get(key);
         RoomData roomInfo = new RoomData(key, jsonRoom.getString("owner"), jsonRoom.getString("name"), jsonRoom.getInt("maxUsers"));
         
         roomsDataList.add(roomInfo);
       }
     }
     RoomData[] roomData = new RoomData[roomsDataList.size()];
     for (int i = 0; i < roomsDataList.size(); i++) {
       roomData[i] = ((RoomData)roomsDataList.get(i));
     }
     return new MatchedRoomsEvent(msg.getResultCode(), roomData);
   }
 }


