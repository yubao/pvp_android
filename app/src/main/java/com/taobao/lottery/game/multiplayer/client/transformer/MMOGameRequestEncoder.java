 package com.taobao.lottery.game.multiplayer.client.transformer;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameRequestMessage;

import java.nio.ByteBuffer;
 
 
 
 
 
 
 
 
 
 
 public class MMOGameRequestEncoder
 {
   public static ByteBuffer encode(MMOGameRequestMessage msg)
   {
     ByteBuffer buf = ByteBuffer.allocate(16 + msg.getPayLoadSize());
     buf.put(msg.getType());
     buf.put(msg.getRequestType());
     buf.putInt(msg.getSessionId());
     buf.putInt(msg.getRequestId());
     buf.put(msg.getReserved());
     buf.put(msg.getPayLoadType());
     buf.putInt(msg.getPayLoadSize());
     if (msg.getPayLoadSize() > 0)
     {
       buf.put(msg.getPayLoad());
     }
     return buf;
   }
 }


