 package com.taobao.lottery.game.multiplayer.client.events;
 
 import org.json.JSONException;
 import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class ChatEvent
 {
   private String chat;
   private String sender;
   private String locId;
   private boolean isLocationLobby;
   
   public ChatEvent(String chat, String sender, String id, boolean fromLobby)
   {
     this.chat = chat;
     this.sender = sender;
     this.locId = id;
     this.isLocationLobby = fromLobby;
   }
   
 
 
 
   public String getLocationId()
   {
     return this.locId;
   }
   
 
 
 
   public boolean isLocationLobby()
   {
     return this.isLocationLobby;
   }
   
 
 
 
   public String getSender()
   {
     return this.sender;
   }
   
 
 
 
   public String getMessage()
   {
     return this.chat;
   }
   
 
 
 
 
   public static ChatEvent buildEvent(JSONObject notifyData)
     throws JSONException
   {
     String chat = notifyData.getString("chat");
     String sender = notifyData.getString("sender");
     String locId = notifyData.getString("id");
     boolean isLobby = notifyData.has("isLobby");
     return new ChatEvent(chat, sender, locId, isLobby);
   }
 }


