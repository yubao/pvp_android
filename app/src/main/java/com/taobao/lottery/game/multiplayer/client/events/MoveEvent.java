 package com.taobao.lottery.game.multiplayer.client.events;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;
import com.taobao.lottery.game.multiplayer.client.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 public class MoveEvent
 {
   private String sender;
   private String moveData;
   private String nextTurn;
   private String roomId;
   
   public MoveEvent() {}
   
   public MoveEvent(String sender, String moveData, String nextTurn, String roomId)
   {
     this.sender = sender;
     this.moveData = moveData;
     this.nextTurn = nextTurn;
     this.roomId = roomId;
   }
   
   public String getSender() {
     return this.sender;
   }
   
   public String getMoveData() {
     return this.moveData;
   }
   
   public String getNextTurn() {
     return this.nextTurn;
   }
   
   public String getRoomId() {
     return this.roomId;
   }
   
   public static MoveEvent buildEvent(JSONObject notifyData) {
     MoveEvent event = new MoveEvent();
     event.roomId = notifyData.optString("id");
     event.moveData = notifyData.optString("moveData");
     event.nextTurn = notifyData.optString("nextTurn");
     event.sender = notifyData.optString("sender");
     return event;
   }
   
   public static MoveEvent[] buildMoveHistoryArray(MMOGameResponseMessage msg) {
     MoveEvent[] eventArray = null;
     try {
       String message = new String(msg.getPayLoad());
       JSONObject responseJson = new JSONObject(message);
       JSONArray moveArray = responseJson.getJSONArray("history");
       if (moveArray != null) {
         eventArray = new MoveEvent[moveArray.length()];
         for (int i = 0; i < moveArray.length(); i++) {
           eventArray[i] = buildEvent(moveArray.getJSONObject(i));
         }
       }
     } catch (Exception e) {
       Util.trace("buildGetMoveHistoryData: " + e.toString());
     }
     return eventArray;
   }
 }


