package com.taobao.lottery.game.multiplayer.client.listener;

import com.taobao.lottery.game.multiplayer.client.events.ConnectEvent;

public abstract interface ConnectionRequestListener
{
  public abstract void onConnectDone(ConnectEvent paramConnectEvent, String paramString);
  
  public abstract void onDisconnectDone(ConnectEvent paramConnectEvent);
  
  public abstract void onInitUDPDone(byte paramByte);
}


