package com.taobao.lottery.game.multiplayer.client.listener;

public abstract interface UpdateRequestListener
{
  public abstract void onSendUpdateDone(byte paramByte);
}


