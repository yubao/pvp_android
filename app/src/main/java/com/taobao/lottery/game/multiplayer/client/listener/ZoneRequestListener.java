package com.taobao.lottery.game.multiplayer.client.listener;

import com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveUserInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.MatchedRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomEvent;

public abstract interface ZoneRequestListener
{
  public abstract void onDeleteRoomDone(RoomEvent paramRoomEvent, String paramString);
  
  public abstract void onGetAllRoomsDone(AllRoomsEvent paramAllRoomsEvent);
  
  public abstract void onCreateRoomDone(RoomEvent paramRoomEvent, String paramString);
  
  public abstract void onGetOnlineUsersDone(AllUsersEvent paramAllUsersEvent);
  
  public abstract void onGetLiveUserInfoDone(LiveUserInfoEvent paramLiveUserInfoEvent);
  
  public abstract void onSetCustomUserDataDone(LiveUserInfoEvent paramLiveUserInfoEvent);
  
  public abstract void onGetMatchedRoomsDone(MatchedRoomsEvent paramMatchedRoomsEvent);
  
  public abstract void onRPCDone(byte paramByte, String paramString, Object paramObject);
}


