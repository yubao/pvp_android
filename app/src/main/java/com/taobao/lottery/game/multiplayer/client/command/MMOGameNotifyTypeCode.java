package com.taobao.lottery.game.multiplayer.client.command;

public abstract interface MMOGameNotifyTypeCode
{
  public static final byte ROOM_CREATED = 1;
  public static final byte ROOM_DELETED = 2;
  public static final byte USER_JOINED_LOBBY = 3;
  public static final byte USER_LEFT_LOBBY = 4;
  public static final byte USER_JOINED_ROOM = 5;
  public static final byte USER_LEFT_ROOM = 6;
  public static final byte USER_ONLINE = 7;
  public static final byte USER_OFFLINE = 8;
  public static final byte CHAT = 9;
  public static final byte UPDATE_PEERS = 10;
  public static final byte ROOM_PROPERTY_CHANGE = 11;
  public static final byte PRIVATE_CHAT = 12;
  public static final byte MOVE_COMPLETED = 13;
  public static final byte USER_PAUSED = 14;
  public static final byte USER_RESUMED = 15;
  public static final byte GAME_STARTED = 16;
  public static final byte GAME_STOPPED = 17;
  public static final byte CLIENT_CUSTOM_MESSAGE = 10;
}


