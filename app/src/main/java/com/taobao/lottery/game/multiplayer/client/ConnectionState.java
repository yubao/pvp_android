package com.taobao.lottery.game.multiplayer.client;

public abstract interface ConnectionState
{
  public static final int CONNECTED = 0;
  public static final int CONNECTING = 1;
  public static final int DISCONNECTED = 2;
  public static final int DISCONNECTING = 3;
  public static final int RECOVERING = 4;
}


