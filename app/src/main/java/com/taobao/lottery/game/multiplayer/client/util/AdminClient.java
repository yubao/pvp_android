 package com.taobao.lottery.game.multiplayer.client.util;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameRequestMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;
import com.taobao.lottery.game.multiplayer.client.transformer.MMOGameMessageDecoder;
import com.taobao.lottery.game.multiplayer.client.transformer.MMOGameRequestEncoder;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class AdminClient
 {
   public static JSONObject adminCreateZone(String name, String user, String password, String host)
   {
     JSONObject result = null;
     
 
     try
     {
       Socket socket = new Socket(host, Util.MMOGameServerPort);
       InputStream in = socket.getInputStream();
       OutputStream out = socket.getOutputStream();
       
       JSONObject authObj = new JSONObject();
       String timeStamp = Util.getUTCFormattedTimestamp();
       
       HashMap<String, String> params = new HashMap();
       params.put("version", "Admin_1.0");
       params.put("timeStamp", timeStamp);
       params.put("apiKey", "");
       params.put("user", user);
       String signature = Util.sign(password, params);
       
       authObj.put("version", "Admin_1.0");
       authObj.put("timeStamp", timeStamp);
       authObj.put("apiKey", "");
       authObj.put("user", user);
       authObj.put("signature", signature);
       authObj.put("AppName", name);
       
       String payLoad = AesHelper.encrypt(password, authObj.toString());
       
       MMOGameRequestMessage zoneMsg = new MMOGameRequestMessage((byte)15, 0, 0, (byte)0, (byte)1, (byte)2, payLoad.getBytes().length, payLoad.getBytes());
       
 
 
 
 
       ByteBuffer buf = MMOGameRequestEncoder.encode(zoneMsg);
       out.write(buf.array());
       byte[] respBuffer = new byte['Ѐ'];
       int readLen = in.read(respBuffer);
       ByteBuffer decodingBuf = ByteBuffer.wrap(respBuffer, 0, readLen);
       MMOGameResponseMessage msg = (MMOGameResponseMessage)MMOGameMessageDecoder.decode(decodingBuf);
       if (msg.getResultCode() == 0) {
         result = new JSONObject(AesHelper.decrypt(password, new String(msg.getPayLoad())));
       }
       if (socket != null) {
         socket.close();
       }
     } catch (Exception ex) {
       Util.trace(ex.getMessage());
     }
     return result;
   }
   
   public static boolean adminDeleteZone(String apiKey, String user, String password, String host)
   {
     boolean result = false;
     
 
     try
     {
       Socket socket = new Socket(host, Util.MMOGameServerPort);
       InputStream in = socket.getInputStream();
       OutputStream out = socket.getOutputStream();
       
       JSONObject authObj = new JSONObject();
       String timeStamp = Util.getUTCFormattedTimestamp();
       
       HashMap<String, String> params = new HashMap();
       params.put("version", "Admin_1.0");
       params.put("timeStamp", timeStamp);
       params.put("apiKey", apiKey);
       params.put("user", user);
       String signature = Util.sign(password, params);
       
       authObj.put("version", "Admin_1.0");
       authObj.put("timeStamp", timeStamp);
       authObj.put("apiKey", apiKey);
       authObj.put("user", user);
       authObj.put("signature", signature);
       
       String payLoad = AesHelper.encrypt(password, authObj.toString());
       
       MMOGameRequestMessage zoneMsg = new MMOGameRequestMessage((byte)16, 0, 0, (byte)0, (byte)1, (byte)2, payLoad.getBytes().length, payLoad.getBytes());
       
 
 
 
 
       ByteBuffer buf = MMOGameRequestEncoder.encode(zoneMsg);
       out.write(buf.array());
       byte[] respBuffer = new byte['Ѐ'];
       int readLen = in.read(respBuffer);
       ByteBuffer decodingBuf = ByteBuffer.wrap(respBuffer, 0, readLen);
       MMOGameResponseMessage msg = (MMOGameResponseMessage)MMOGameMessageDecoder.decode(decodingBuf);
       if (msg.getResultCode() == 0) {
         result = true;
       }
       if (socket != null) {
         socket.close();
       }
     } catch (Exception ex) {
       Util.trace(ex.getMessage());
     }
     return result;
   }
   
 
   public static JSONObject adminCreateRoom(String apiKey, String name, int maxUsers, HashMap<String, Object> tableProperties, String user, String password, String host)
   {
     JSONObject result = null;
     
 
     try
     {
       JSONObject properties = new JSONObject();
       
       if ((tableProperties != null) && (tableProperties.size() > 0)) {
         properties = Util.getJsonObjectFromHashtable(tableProperties);
       }
       if (properties.toString().getBytes().length > 2048) {
         return result;
       }
       
       Socket socket = new Socket(host, Util.MMOGameServerPort);
       InputStream in = socket.getInputStream();
       OutputStream out = socket.getOutputStream();
       
       JSONObject authObj = new JSONObject();
       String timeStamp = Util.getUTCFormattedTimestamp();
       
       HashMap<String, String> params = new HashMap();
       params.put("version", "Admin_1.0");
       params.put("timeStamp", timeStamp);
       params.put("apiKey", apiKey);
       params.put("user", user);
       String signature = Util.sign(password, params);
       
       authObj.put("version", "Admin_1.0");
       authObj.put("timeStamp", timeStamp);
       authObj.put("apiKey", apiKey);
       authObj.put("user", user);
       authObj.put("signature", signature);
       
       authObj.put("maxUsers", maxUsers);
       authObj.put("name", name);
       
       if (properties != null) {
         authObj.put("properties", properties);
       }
       
       String payLoad = AesHelper.encrypt(password, authObj.toString());
       
       MMOGameRequestMessage roomCreateMsg = new MMOGameRequestMessage((byte)6, 0, 0, (byte)0, (byte)1, (byte)2, payLoad.getBytes().length, payLoad.getBytes());
       
 
 
 
 
       ByteBuffer buf = MMOGameRequestEncoder.encode(roomCreateMsg);
       
       out.write(buf.array());
       byte[] respBuffer = new byte['Ѐ'];
       int readLen = in.read(respBuffer);
       ByteBuffer decodingBuf = ByteBuffer.wrap(respBuffer, 0, readLen);
       MMOGameResponseMessage msg = (MMOGameResponseMessage)MMOGameMessageDecoder.decode(decodingBuf);
       if (msg.getResultCode() == 0) {
         result = new JSONObject(AesHelper.decrypt(password, new String(msg.getPayLoad())));
       }
       if (socket != null) {
         socket.close();
       }
     } catch (Exception ex) {
       Util.trace(ex.getMessage());
     }
     return result;
   }
   
 
 
 
 
 
 
 
   public static boolean adminDeleteRoom(String apiKey, String roomId, String user, String password, String host)
   {
     boolean result = false;
     
 
     try
     {
       Socket socket = new Socket(host, Util.MMOGameServerPort);
       InputStream in = socket.getInputStream();
       OutputStream out = socket.getOutputStream();
       
       JSONObject authObj = new JSONObject();
       String timeStamp = Util.getUTCFormattedTimestamp();
       
       HashMap<String, String> params = new HashMap();
       params.put("version", "Admin_1.0");
       params.put("timeStamp", timeStamp);
       params.put("apiKey", apiKey);
       params.put("user", user);
       String signature = Util.sign(password, params);
       
       authObj.put("version", "Admin_1.0");
       authObj.put("timeStamp", timeStamp);
       authObj.put("apiKey", apiKey);
       authObj.put("user", user);
       authObj.put("signature", signature);
       
       authObj.put("id", roomId);
       
       String payLoad = AesHelper.encrypt(password, authObj.toString());
       
       MMOGameRequestMessage roomDelMsg = new MMOGameRequestMessage((byte)11, 0, 0, (byte)0, (byte)1, (byte)2, payLoad.getBytes().length, payLoad.getBytes());
       
 
 
 
 
 
       ByteBuffer buf = MMOGameRequestEncoder.encode(roomDelMsg);
       
       out.write(buf.array());
       byte[] respBuffer = new byte['Ѐ'];
       int readLen = in.read(respBuffer);
       ByteBuffer decodingBuf = ByteBuffer.wrap(respBuffer, 0, readLen);
       MMOGameResponseMessage msg = (MMOGameResponseMessage)MMOGameMessageDecoder.decode(decodingBuf);
       if (msg.getResultCode() == 0) {
         result = true;
       }
       if (socket != null) {
         socket.close();
       }
     } catch (Exception ex) {
       Util.trace(ex.getMessage());
     }
     return result;
   }
   
 
 
 
 
 
   public static JSONObject adminGetLiveStats(String user, String password, String host)
   {
     JSONObject result = null;
     
 
     try
     {
       Socket socket = new Socket(host, Util.MMOGameServerPort);
       InputStream in = socket.getInputStream();
       OutputStream out = socket.getOutputStream();
       
       JSONObject authObj = new JSONObject();
       String timeStamp = Util.getUTCFormattedTimestamp();
       
       HashMap<String, String> params = new HashMap();
       params.put("version", "Admin_1.0");
       params.put("timeStamp", timeStamp);
       params.put("apiKey", "");
       params.put("user", user);
       String signature = Util.sign(password, params);
       
       authObj.put("version", "Admin_1.0");
       authObj.put("timeStamp", timeStamp);
       authObj.put("apiKey", "");
       authObj.put("user", user);
       authObj.put("signature", signature);
       
       String payLoad = AesHelper.encrypt(password, authObj.toString());
       
       MMOGameRequestMessage zoneMsg = new MMOGameRequestMessage((byte)61, 0, 0, (byte)0, (byte)1, (byte)2, payLoad.getBytes().length, payLoad.getBytes());
       
 
 
 
 
       ByteBuffer buf = MMOGameRequestEncoder.encode(zoneMsg);
       out.write(buf.array());
       byte[] respBuffer = new byte['Ѐ'];
       int readLen = in.read(respBuffer);
       ByteBuffer decodingBuf = ByteBuffer.wrap(respBuffer, 0, readLen);
       MMOGameResponseMessage msg = (MMOGameResponseMessage)MMOGameMessageDecoder.decode(decodingBuf);
       if (msg.getResultCode() == 0) {
         result = new JSONObject(AesHelper.decrypt(password, new String(msg.getPayLoad())));
       }
       if (socket != null) {
         socket.close();
       }
     } catch (Exception ex) {
       Util.trace(ex.getMessage());
       return null;
     }
     return result;
   }
   
 
 
 
 
 
   public static boolean adminValidateCredentials(String user, String password, String host)
   {
     boolean result = false;
     
 
     try
     {
       Socket socket = new Socket(host, Util.MMOGameServerPort);
       InputStream in = socket.getInputStream();
       OutputStream out = socket.getOutputStream();
       
       JSONObject authObj = new JSONObject();
       String timeStamp = Util.getUTCFormattedTimestamp();
       
       HashMap<String, String> params = new HashMap();
       params.put("version", "Admin_1.0");
       params.put("timeStamp", timeStamp);
       params.put("apiKey", "");
       params.put("user", user);
       String signature = Util.sign(password, params);
       
       authObj.put("version", "Admin_1.0");
       authObj.put("timeStamp", timeStamp);
       authObj.put("apiKey", "");
       authObj.put("user", user);
       authObj.put("signature", signature);
       
       String payLoad = AesHelper.encrypt(password, authObj.toString());
       
       MMOGameRequestMessage zoneMsg = new MMOGameRequestMessage((byte)60, 0, 0, (byte)0, (byte)1, (byte)2, payLoad.getBytes().length, payLoad.getBytes());
       
 
 
 
 
       ByteBuffer buf = MMOGameRequestEncoder.encode(zoneMsg);
       out.write(buf.array());
       byte[] respBuffer = new byte['Ѐ'];
       int readLen = in.read(respBuffer);
       ByteBuffer decodingBuf = ByteBuffer.wrap(respBuffer, 0, readLen);
       MMOGameResponseMessage msg = (MMOGameResponseMessage)MMOGameMessageDecoder.decode(decodingBuf);
       if (msg.getResultCode() == 0) {
         result = true;
       }
       if (socket != null) {
         socket.close();
       }
     } catch (Exception ex) {
       Util.trace(ex.getMessage());
     }
     return result;
   }
   
   public static JSONArray adminGetZones(String user, String password, String host)
   {
     JSONArray result = null;
     
 
     try
     {
       Socket socket = new Socket(host, Util.MMOGameServerPort);
       InputStream in = socket.getInputStream();
       OutputStream out = socket.getOutputStream();
       
       JSONObject authObj = new JSONObject();
       String timeStamp = Util.getUTCFormattedTimestamp();
       
       HashMap<String, String> params = new HashMap();
       params.put("version", "Admin_1.0");
       params.put("timeStamp", timeStamp);
       params.put("apiKey", "");
       params.put("user", user);
       String signature = Util.sign(password, params);
       
       authObj.put("version", "Admin_1.0");
       authObj.put("timeStamp", timeStamp);
       authObj.put("apiKey", "");
       authObj.put("user", user);
       authObj.put("signature", signature);
       
       String payLoad = AesHelper.encrypt(password, authObj.toString());
       
       MMOGameRequestMessage zoneMsg = new MMOGameRequestMessage((byte)59, 0, 0, (byte)0, (byte)1, (byte)2, payLoad.getBytes().length, payLoad.getBytes());
       
 
 
 
 
       ByteBuffer buf = MMOGameRequestEncoder.encode(zoneMsg);
       out.write(buf.array());
       byte[] respBuffer = new byte['Ѐ'];
       int readLen = in.read(respBuffer);
       ByteBuffer decodingBuf = ByteBuffer.wrap(respBuffer, 0, readLen);
       MMOGameResponseMessage msg = (MMOGameResponseMessage)MMOGameMessageDecoder.decode(decodingBuf);
       if (msg.getResultCode() == 0) {
         result = new JSONArray(AesHelper.decrypt(password, new String(msg.getPayLoad())));
       }
       if (socket != null) {
         socket.close();
       }
     } catch (Exception ex) {
       Util.trace(ex.getMessage());
     }
     return result;
   }
 }


