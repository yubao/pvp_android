 package com.taobao.lottery.game.multiplayer.client;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameRequestMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;
import com.taobao.lottery.game.multiplayer.client.transformer.MMOGameMessageDecoder;
import com.taobao.lottery.game.multiplayer.client.transformer.MMOGameRequestEncoder;
import com.taobao.lottery.game.multiplayer.client.util.Util;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class UDPListener
   implements Runnable
 {
   private InetAddress mmoGameServerIP;
   private DatagramSocket serverSocket;
   private MMOGameClient theGame;
   private boolean waitingForAck = false;
   private boolean shouldStop = false;
   
   public void Stop() {
     if (this.serverSocket != null) {
       this.serverSocket.close();
       this.serverSocket = null;
     }
   }
   
   public void run()
   {
     try {
       byte[] recvBuff = new byte['Ѐ'];
       MMOGameClient theClient = MMOGameClient.getInstance();
       if (this.serverSocket == null) {
         Util.trace("Can't start listening UDP as socket is null");
         return;
       }
       this.serverSocket.setSoTimeout(10000);
       while (!this.shouldStop) {
         DatagramPacket receivePacket = new DatagramPacket(recvBuff, recvBuff.length);
         try {
           this.serverSocket.receive(receivePacket);
           InetAddress sourceIP = receivePacket.getAddress();
           if (sourceIP.equals(this.mmoGameServerIP)) {
             byte[] receiveData = receivePacket.getData();
             MMOGameMessage msg = MMOGameMessageDecoder.decode(ByteBuffer.wrap(receiveData));
             theClient.addMessageToQueue(msg);
             synchronized (this.theGame.dispatcher) {
               this.theGame.dispatcher.notify();
             }
             if ((msg instanceof MMOGameResponseMessage)) {
               MMOGameResponseMessage responseMsg = (MMOGameResponseMessage)msg;
               if ((this.waitingForAck) && (responseMsg.getRequestType() == 64)) {
                 this.waitingForAck = false;
               }
             }
           }
         }
         catch (SocketTimeoutException e) {
           if (this.waitingForAck) {
             Util.trace("UDP SocketTimeoutException");
             this.shouldStop = true;
             this.theGame.cleanupUdpListener();
             this.theGame.fireUDPEvent((byte)5);
           }
           else {
             Util.trace("UDP sendKeepAlive");
             sendKeepAlive();
           }
         }
       }
     } catch (Exception ex) {
       Util.trace("UDP Listen loop " + ex.toString());
     }
   }
   
   synchronized void SendRequest(MMOGameRequestMessage updateMsg)
   {
     try {
       if (updateMsg.getRequestType() == 64) {
         this.theGame = MMOGameClient.getInstance();
         this.waitingForAck = true;
         this.serverSocket = new DatagramSocket();
         this.mmoGameServerIP = InetAddress.getByName(Util.MMOGameServerHost);
         Util.trace("new UDP Socket created with " + Util.MMOGameServerHost);
       }
       if (this.serverSocket == null) {
         Util.trace("Can't send request as UDP Socket is null");
         return;
       }
       ByteBuffer buf = MMOGameRequestEncoder.encode(updateMsg);
       buf.flip();
       DatagramPacket packet = new DatagramPacket(buf.array(), buf.array().length, this.mmoGameServerIP, Util.MMOGameServerPort);
       this.serverSocket.send(packet);
     }
     catch (Exception ex) {
       Util.trace("UDP SendRequest " + ex);
     }
   }
   
   private void sendKeepAlive() {
     MMOGameRequestMessage msg = new MMOGameRequestMessage((byte)63, this.theGame.sessionId, 0, (byte)0, (byte)0, (byte)0, "".getBytes().length, "".getBytes());
     
 
 
 
 
 
 
 
 
     SendRequest(msg);
   }
 }


