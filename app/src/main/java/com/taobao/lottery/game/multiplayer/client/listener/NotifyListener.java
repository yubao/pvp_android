package com.taobao.lottery.game.multiplayer.client.listener;

import com.taobao.lottery.game.multiplayer.client.events.ChatEvent;
import com.taobao.lottery.game.multiplayer.client.events.LobbyData;
import com.taobao.lottery.game.multiplayer.client.events.MoveEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomData;
import com.taobao.lottery.game.multiplayer.client.events.UpdateEvent;

import java.util.HashMap;

public abstract interface NotifyListener
{
  public abstract void onRoomCreated(RoomData paramRoomData);
  
  public abstract void onRoomDestroyed(RoomData paramRoomData);
  
  public abstract void onUserLeftRoom(RoomData paramRoomData, String paramString);
  
  public abstract void onUserJoinedRoom(RoomData paramRoomData, String paramString);
  
  public abstract void onUserLeftLobby(LobbyData paramLobbyData, String paramString);
  
  public abstract void onUserJoinedLobby(LobbyData paramLobbyData, String paramString);
  
  public abstract void onChatReceived(ChatEvent paramChatEvent);
  
  public abstract void onPrivateChatReceived(String paramString1, String paramString2);
  
  public abstract void onUpdatePeersReceived(UpdateEvent paramUpdateEvent);
  
  public abstract void onUserChangeRoomProperty(RoomData paramRoomData, String paramString, HashMap<String, Object> paramHashMap, HashMap<String, String> paramHashMap1);
  
  public abstract void onMoveCompleted(MoveEvent paramMoveEvent);
  
  public abstract void onGameStarted(String paramString1, String paramString2, String paramString3);
  
  public abstract void onGameStopped(String paramString1, String paramString2);
  
  public abstract void onUserPaused(String paramString1, boolean paramBoolean, String paramString2);
  
  public abstract void onUserResumed(String paramString1, boolean paramBoolean, String paramString2);
}


