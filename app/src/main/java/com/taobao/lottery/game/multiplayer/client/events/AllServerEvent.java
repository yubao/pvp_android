 package com.taobao.lottery.game.multiplayer.client.events;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class AllServerEvent
 {
   private byte result;
   private ArrayList<GameServer> serverList;
   
   public AllServerEvent(byte result, ArrayList<GameServer> servers)
   {
     this.result = result;
     this.serverList = servers;
   }
   
 
 
 
   public byte getResult()
   {
     return this.result;
   }
   
 
 
 
   public ArrayList<GameServer> getServers()
   {
     return this.serverList;
   }
   
 
 
 
 
   public static AllServerEvent buildAllServerEvent(MMOGameResponseMessage msg)
     throws JSONException
   {
     ArrayList<GameServer> servers = null;
     if (msg.getResultCode() == 0)
     {
       JSONArray zoneArray = new JSONArray(new String(msg.getPayLoad()));
       if (zoneArray.length() > 0) {
         servers = new ArrayList();
         for (int i = 0; i < zoneArray.length(); i++) {
           JSONObject zone = zoneArray.getJSONObject(i);
           JSONObject detail = zone.getJSONObject("address");
           Address address = new Address(detail.getString("host"), detail.getInt("port"));
           JSONArray zones = zone.getJSONArray("zones");
           String[] appKeys = new String[zones.length()];
           for (int j = 0; j < zones.length(); j++) {
             appKeys[j] = zones.getString(j);
           }
           servers.add(new GameServer(address, appKeys));
         }
       }
     }
     return new AllServerEvent(msg.getResultCode(), servers);
   }
 }


