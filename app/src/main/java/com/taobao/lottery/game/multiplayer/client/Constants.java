package com.taobao.lottery.game.multiplayer.client;

public class Constants
{
  public static final int MAX_PROPERTY_SIZE_BYTES = 2048;
  public static final int UDP_WAIT_TIME = 3000;
  public static final String VERSION = "Java_1.5";
  public static final int MMOGameKeepAliveTimeOut = 6;
  public static final int MMOGameKeepAliveInterval = 2000;
  public static final int MMOGameUDPKeepAliveInterval = 10000;
}


