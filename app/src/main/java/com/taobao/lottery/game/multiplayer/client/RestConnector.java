 package com.taobao.lottery.game.multiplayer.client;
 
 import java.io.BufferedReader;
 import java.io.FileNotFoundException;
 import java.io.InputStream;
 import java.io.InputStreamReader;
 import java.net.HttpURLConnection;
 import java.net.URL;
 import org.json.JSONObject;
 
 
 
 public class RestConnector
 {
   private final String KEYADDRESS = "address";
   MMOGameClient theGame;
   
   public RestConnector() {
     try {
       this.theGame = MMOGameClient.getInstance();
     } catch (Exception ex) {
       ex.printStackTrace(System.err);
     }
   }
   
   public void fetchHostIp(final String baseUrl, final String apiKey)
   {
     new Thread()
     {
       public void run()
       {
         try {
           byte lookUpStatus = RestConnector.this.getResultStatus(baseUrl, apiKey);
           if (RestConnector.this.theGame != null) {
             RestConnector.this.theGame.onLookUpServer(lookUpStatus);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace(System.err);
           RestConnector.this.theGame.onLookUpServer((byte)5);
         }
       }
     }.start();
   }
   
   private byte getResultStatus(String baseUrl, String apiKey) {
     try {
       String response = excuteGet(buildUrl(baseUrl, apiKey));
       JSONObject jsonResult = new JSONObject(response);
       if ((jsonResult.get("address").toString().trim() != null) && (!jsonResult.get("address").toString().trim().equals("")))
       {
         com.taobao.lottery.game.multiplayer.client.util.Util.MMOGameServerHost = jsonResult.get("address").toString().trim();
         return 0;
       }
       return 5;
     }
     catch (FileNotFoundException e) {
       e.printStackTrace(System.err);
       return 1;
     } catch (Exception e) {}
     return 5;
   }
   
 
 
   private String buildUrl(String basUrl, String apiKey)
   {
     return basUrl + "?api=" + apiKey;
   }
   
 
 
 
 
 
 
 
 
   private String excuteGet(String urlStr)
     throws Exception
   {
     InputStream in = null;
     HttpURLConnection connection = null;
     try
     {
       URL url = new URL(urlStr);
       connection = (HttpURLConnection)url.openConnection();
       connection.setRequestMethod("GET");
       connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
       
       connection.setRequestProperty("Content-Language", "en-US");
       connection.setUseCaches(false);
       connection.setDoInput(true);
       in = connection.getInputStream();
       BufferedReader br = new BufferedReader(new InputStreamReader(in));
       StringBuffer sb = new StringBuffer();
       String line;
       while ((line = br.readLine()) != null) {
         sb.append(line);
       }
       return sb.toString();
     } catch (Exception e) {
       throw e;
     } finally {
       if (connection != null) {
         connection.disconnect();
       }
       if (in != null) {
         in.close();
       }
     }
   }
 }


