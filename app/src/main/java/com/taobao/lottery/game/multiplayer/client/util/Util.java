 package com.taobao.lottery.game.multiplayer.client.util;
 
 import java.io.PrintStream;
 import java.io.UnsupportedEncodingException;
 import java.net.URLEncoder;
 import java.security.InvalidKeyException;
 import java.security.NoSuchAlgorithmException;
 import java.text.SimpleDateFormat;
 import java.util.ArrayList;
 import java.util.Collections;
 import java.util.Date;
 import java.util.HashMap;
 import java.util.Iterator;
import java.util.Map;
 import java.util.Map.Entry;
 import java.util.Random;
 import java.util.TimeZone;

 import javax.crypto.Mac;
 import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
 import org.json.JSONObject;
 
 
 public class Util
 {
   public static String MMOGameServerHost;
   public static int MMOGameServerPort = 12346;
   
   public static final int MMOGameKeepAliveTimeOut = 6;
   
   public static final int MMOGameKeepAliveInterval = 2000;
   
   public static String userName = "";
   
   public static boolean TRACE_ENABLED = false;
   
   public static int RECOVERY_ALLOWANCE_TIME = 0;
   
 
 
 
   static String sortAndConvertTableToString(HashMap<String, String> table)
   {
     ArrayList<String> v = new ArrayList(table.keySet());
     Collections.sort(v);
     StringBuffer requestString = new StringBuffer();
     for (Iterator<String> e = v.iterator(); e.hasNext();) {
       String key = (String)e.next();
       String val = (String)table.get(key);
       requestString.append(key);
       requestString.append(val);
     }
     return requestString.toString();
   }
   
   public static String calculateSignature(String apiKey, String version, String user, String timeStamp, String secretKey)
   {
     HashMap<String, String> params = new HashMap();
     params.put("apiKey", apiKey);
     params.put("version", version);
     params.put("timeStamp", timeStamp);
     params.put("user", user);
     return sign(secretKey, params);
   }
   
   public static String calculateSignature(String apiKey, String version, String timeStamp, String secretKey)
   {
     HashMap<String, String> params = new HashMap();
     params.put("apiKey", apiKey);
     params.put("version", version);
     params.put("timeStamp", timeStamp);
     return sign(secretKey, params);
   }
   
 
 
 
 
   public static String sign(String secretKey, HashMap<String, String> params)
   {
     try
     {
       String sortedParams = sortAndConvertTableToString(params);
       String signature = computeHmac(sortedParams, secretKey);
       return URLEncoder.encode(signature);
     } catch (Exception ex) {
       trace(ex.getMessage());
     }
     return null;
   }
   
 
   public static String computeHmac(String baseString, String key)
     throws NoSuchAlgorithmException, InvalidKeyException, IllegalStateException, UnsupportedEncodingException
   {
     Mac mac = Mac.getInstance("HmacSHA1");
     SecretKeySpec secret = new SecretKeySpec(key.getBytes(), mac.getAlgorithm());
     mac.init(secret);
     byte[] digest = mac.doFinal(baseString.getBytes());
     return Base64.encodeBase64String(digest);
   }
   
   public static String getUTCFormattedTimestamp()
   {
     SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
     
     df.setTimeZone(TimeZone.getTimeZone("UTC"));
     return df.format(new Date());
   }
   
   public static String getUTCFormattedTimestamp(Date date) {
     SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
     
     df.setTimeZone(TimeZone.getTimeZone("UTC"));
     return df.format(date);
   }
   
   public static int generateLocalUdpPort() {
     Random randomGenerator = new Random();
     return randomGenerator.nextInt(100) + 12345;
   }
   
   public static JSONObject getJsonObjectFromHashtable(HashMap<String, Object> properties) { JSONObject jsonObject = new JSONObject();
     try {
       for (Entry<String, Object> entry : properties.entrySet()) {
         jsonObject.put((String)entry.getKey(), entry.getValue());
       }
     } catch (Exception e) {
       trace(e.getMessage());
     }
     return jsonObject;
   }
   
   public static HashMap getHashMapFromProperties(String input) {
     HashMap<String, Object> properties = new HashMap();
     properties.clear();
     try {
       JSONObject jSONObject = new JSONObject(input);
       Iterator it = jSONObject.keys();
       while (it.hasNext()) {
         String key = it.next().toString();
         properties.put(key, jSONObject.get(key));
       }
       return properties;
     } catch (Exception e) {
       trace(e.getMessage()); }
     return null;
   }
   
   public static void trace(String message)
   {
     if (TRACE_ENABLED) {
       System.out.println("AppMMOGameTrace " + message);
     }
   }
   
   public static byte[] toByte(String hexString)
   {
     int len = hexString.length() / 2;
     byte[] result = new byte[len];
     for (int i = 0; i < len; i++) {
       result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue();
     }
     return result;
   }
 }


