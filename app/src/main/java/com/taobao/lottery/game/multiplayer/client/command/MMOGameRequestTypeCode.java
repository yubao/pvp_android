package com.taobao.lottery.game.multiplayer.client.command;

public abstract interface MMOGameRequestTypeCode
{
  public static final byte AUTH = 1;
  public static final byte JOIN_LOBBY = 2;
  public static final byte SUBSCRIBE_LOBBY = 3;
  public static final byte UNSUBSCRIBE_LOBBY = 4;
  public static final byte LEAVE_LOBBY = 5;
  public static final byte CREATE_ROOM = 6;
  public static final byte JOIN_ROOM = 7;
  public static final byte SUBSCRIBE_ROOM = 8;
  public static final byte UNSUBSCRIBE_ROOM = 9;
  public static final byte LEAVE_ROOM = 10;
  public static final byte DELETE_ROOM = 11;
  public static final byte CHAT = 12;
  public static final byte UPDATE_PEERS = 13;
  public static final byte SIGNOUT = 14;
  public static final byte CREATE_ZONE = 15;
  public static final byte DELETE_ZONE = 16;
  public static final byte GET_ROOMS = 17;
  public static final byte GET_USERS = 18;
  public static final byte GET_USER_INFO = 19;
  public static final byte GET_ROOM_INFO = 20;
  public static final byte SET_CUSTOM_ROOM_DATA = 21;
  public static final byte SET_CUSTOM_USER_DATA = 22;
  public static final byte GET_LOBBY_INFO = 23;
  public static final byte JOIN_ROOM_N_USER = 24;
  public static final byte UPDATE_ROOM_PROPERTY = 25;
  public static final byte JOIN_ROOM_WITH_PROPERTIES = 27;
  public static final byte GET_ROOM_WITH_N_USER = 28;
  public static final byte GET_ROOM_WITH_PROPERTIES = 29;
  public static final byte PRIVATE_CHAT = 30;
  public static final byte MOVE = 31;
  public static final byte LOCK_PROPERTIES = 35;
  public static final byte UNLOCK_PROPERTIES = 36;
  public static final byte JOIN_ROOM_IN_RANGE = 37;
  public static final byte GET_ROOM_IN_RANGE = 38;
  public static final byte GET_ZONES = 59;
  public static final byte VALIDATE_ADMIN_CREDENTIALS = 60;
  public static final byte GET_LIVE_STATS = 61;
  public static final byte RPC_REQUEST = 62;
  public static final byte KEEP_ALIVE = 63;
  public static final byte ASSOC_PORT = 64;
  public static final byte ACK_ASSOC_PORT = 65;
  public static final byte START_GAME = 66;
  public static final byte STOP_GAME = 67;
  public static final byte GET_MOVE_HISTORY = 68;
  public static final byte INVOKE_ZONE_RPC = 69;
  public static final byte INVOKE_ROOM_RPC = 70;
  public static final byte MASTER_AUTH = 2;
  public static final byte GET_ALL_SERVERS = 1;
  public static final byte CLIENT_CUSTOM_MESSAGE = 3;
  public static final byte MASTER_SIGNOUT = 4;
}


