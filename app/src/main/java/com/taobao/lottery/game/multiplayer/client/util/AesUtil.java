 package com.taobao.lottery.game.multiplayer.client.util;
 
 import org.apache.commons.codec.DecoderException;

 import java.io.UnsupportedEncodingException;
 import java.security.InvalidAlgorithmParameterException;
 import java.security.InvalidKeyException;
 import java.security.NoSuchAlgorithmException;
 import java.security.SecureRandom;
 import java.security.spec.InvalidKeySpecException;
 import java.security.spec.KeySpec;
 import javax.crypto.BadPaddingException;
 import javax.crypto.Cipher;
 import javax.crypto.IllegalBlockSizeException;
 import javax.crypto.NoSuchPaddingException;
 import javax.crypto.SecretKey;
 import javax.crypto.SecretKeyFactory;
 import javax.crypto.spec.IvParameterSpec;
 
 public class AesUtil
 {
   private final int keySize;
   private final int iterationCount;
   private final Cipher cipher;
   
   public AesUtil(int keySize, int iterationCount)
   {
     this.keySize = keySize;
     this.iterationCount = iterationCount;
     try {
       this.cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
     }
     catch (NoSuchAlgorithmException e) {
       throw fail(e);
     } catch (NoSuchPaddingException e) {
       throw fail(e);
     }
   }
   
   public String encrypt(String salt, String iv, String passphrase, String plaintext) {
     try {
       SecretKey key = generateKey(salt, passphrase);
       byte[] encrypted = doFinal(1, key, iv, plaintext.getBytes("UTF-8"));
       return base64(encrypted);
     }
     catch (UnsupportedEncodingException e) {
       throw fail(e);
     }
   }
   
   public String decrypt(String salt, String iv, String passphrase, String ciphertext) {
     try {
       SecretKey key = generateKey(salt, passphrase);
       byte[] decrypted = doFinal(2, key, iv, base64(ciphertext));
       return new String(decrypted, "UTF-8");
     }
     catch (UnsupportedEncodingException e) {
       throw fail(e);
     }
   }
   
   private byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[] bytes) {
     try {
       this.cipher.init(encryptMode, key, new IvParameterSpec(hex(iv)));
       return this.cipher.doFinal(bytes);
     }
     catch (InvalidKeyException e) {
       throw fail(e);
     } catch (InvalidAlgorithmParameterException e) {
       throw fail(e);
     } catch (IllegalBlockSizeException e) {
       throw fail(e);
     } catch (BadPaddingException e) {
       throw fail(e);
     } catch (DecoderException e) {
       throw fail(e);
     }
   }
   
   private SecretKey generateKey(String salt, String passphrase) {
     try {
       SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
       KeySpec spec = new javax.crypto.spec.PBEKeySpec(passphrase.toCharArray(), hex(salt), this.iterationCount, this.keySize);
       return new javax.crypto.spec.SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
     }
     catch (NoSuchAlgorithmException e)
     {
       throw fail(e);
     } catch (InvalidKeySpecException e) {
       throw fail(e);
     } catch (DecoderException e) {
       throw fail(e);
     }
   }
   
   public static String random(int length) {
     byte[] salt = new byte[length];
     new SecureRandom().nextBytes(salt);
     return hex(salt);
   }
   
   public static String base64(byte[] bytes) {
     //return DatatypeConverter.printBase64Binary(bytes);
     return android.util.Base64.encodeToString(bytes, 16);
   }
   
   public static byte[] base64(String str){
     //return DatatypeConverter.parseBase64Binary(str);
     return android.util.Base64.decode(str,16);

   }
   
   public static String hex(byte[] bytes) {
     //return DatatypeConverter.printHexBinary(bytes);
     return org.apache.commons.codec.binary.Hex.encodeHexString(bytes);
   }
   
   public static byte[] hex(String str) throws DecoderException {
     //return DatatypeConverter.parseHexBinary(str);
     return org.apache.commons.codec.binary.Hex.decodeHex(str.toCharArray());
   }
   
   private IllegalStateException fail(Exception e) {
     return new IllegalStateException(e);
   }
 }


