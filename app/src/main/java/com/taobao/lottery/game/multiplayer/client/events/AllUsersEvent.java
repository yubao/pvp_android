 package com.taobao.lottery.game.multiplayer.client.events;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;

import org.json.JSONException;
import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class AllUsersEvent
 {
   private byte result;
   private String[] userNames;
   
   public AllUsersEvent(byte result, String[] users)
   {
     this.userNames = users;
     this.result = result;
   }
   
 
 
 
   public byte getResult()
   {
     return this.result;
   }
   
 
 
 
   public String[] getUserNames()
   {
     return this.userNames;
   }
   
 
 
 
 
 
 
   public static AllUsersEvent buildAllUsersEvent(MMOGameResponseMessage msg)
     throws JSONException
   {
     String[] usernames = null;
     if (msg.getResultCode() == 0)
     {
 
       JSONObject jsonUsers = new JSONObject(new String(msg.getPayLoad()));
       usernames = jsonUsers.getString("names").split(";");
     }
     return new AllUsersEvent(msg.getResultCode(), usernames);
   }
 }


