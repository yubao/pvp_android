 package com.taobao.lottery.game.multiplayer.client;
 
 import com.taobao.lottery.game.multiplayer.client.events.AllServerEvent;
import com.taobao.lottery.game.multiplayer.client.listener.MasterRequestListener;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameNotifyMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameRequestMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
 
 
 
 
 
 
 
 
 
 
 
 public class MasterClient
 {
   private MasterChannel masterChannel;
   private String masterServerHost;
   private int masterServerPort;
   private int sessionId = 0;
   private int MAX_MESSAGE_SIZE = 500;
   
 
   private Set<MasterRequestListener> masterServerRequestListeners = Collections.synchronizedSet(new HashSet());
   
 
 
 
 
 
   public static MasterClient getInstance()
     throws Exception
   {
     if (_instance == null) {
       throw new Exception("MMOGameClient not initialized!");
     }
     return _instance;
   }
   
   private static MasterClient _instance = null;
   private int connectionState = 2;
   
 
 
 
 
 
 
 
 
 
 
 
 
   public static byte initialize(String masterServerHost, int masterServerPort)
   {
     if ((_instance != null) || (masterServerHost == null) || (masterServerPort == 0)) {
       return 4;
     }
     _instance = new MasterClient();
     _instance.masterServerHost = masterServerHost;
     _instance.masterServerPort = masterServerPort;
     return 0;
   }
   
 
 
 
 
 
   public void addMasterServerRequestListener(MasterRequestListener listener)
   {
     this.masterServerRequestListeners.add(listener);
   }
   
 
 
 
 
   public void removeMasterServerRequestListener(MasterRequestListener listener)
   {
     this.masterServerRequestListeners.remove(listener);
   }
   
 
 
 
 
   public void connect()
   {
     if ((this.connectionState != 2) && (this.connectionState != 3)) {
       fireConnectionEvent((byte)4);
       return;
     }
     try {
       this.connectionState = 1;
       this.masterChannel = new MasterChannel(this.masterServerHost, this.masterServerPort);
       Thread t = new Thread(this.masterChannel);
       t.setDaemon(false);
       t.start();
     } catch (Exception ex) {
       fireConnectionEvent((byte)5);
     }
   }
   
 
 
 
 
   public void disconnect()
   {
     if ((this.connectionState == 2) || (this.connectionState == 3)) {
       fireDisconnectEvent((byte)4);
       return;
     }
     this.sessionId = 0;
     this.masterChannel.disconnect();
     this.connectionState = 2;
     fireDisconnectEvent((byte)0);
   }
   
 
 
 
 
 
   public int getConnectionState()
   {
     return this.connectionState;
   }
   
   protected void onResponse(MMOGameResponseMessage msg) {
     AllServerEvent allServerEvent = null;
     switch (msg.getRequestType())
     {
     case 1: 
       try {
         allServerEvent = AllServerEvent.buildAllServerEvent(msg);
       } catch (Exception e) {
         allServerEvent = new AllServerEvent((byte)6, null);
       }
       for (MasterRequestListener listener : this.masterServerRequestListeners) {
         listener.onGetAllServerDone(allServerEvent);
       }
       break;
     }
     
   }
   
 
   void onNotify(MMOGameNotifyMessage msg)
   {
     if (msg.getUpdateType() == 10) {
       for (MasterRequestListener listener : this.masterServerRequestListeners) {
         listener.onCustomMessageReceived(msg.getPayLoad());
       }
     }
   }
   
   void onConnect(boolean success) {
     if (success) {
       this.connectionState = 0;
       fireConnectionEvent((byte)0);
     }
     else if (this.connectionState == 3) {
       this.connectionState = 2;
       fireDisconnectEvent((byte)0);
     }
     else if (this.connectionState != 2) {
       this.connectionState = 2;
       fireConnectionEvent((byte)5);
     }
   }
   
 
 
 
 
   public void getAllServers()
   {
     if (isNotConnected()) {
       AllServerEvent allServersEvent = new AllServerEvent((byte)5, null);
       for (MasterRequestListener listener : this.masterServerRequestListeners) {
         listener.onGetAllServerDone(allServersEvent);
       }
       return;
     }
     MMOGameRequestMessage msg = new MMOGameRequestMessage((byte)1, this.sessionId, 0, (byte)0, (byte)3, (byte)0, "".getBytes().length, "".getBytes());
     
 
 
 
     this.masterChannel.SendRequest(msg);
   }
   
 
 
 
 
 
 
   public void sendCustomMessage(byte[] message)
   {
     if (isNotConnected())
       return;
     if ((message == null) || (new String(message).length() > this.MAX_MESSAGE_SIZE)) {
       return;
     }
     MMOGameRequestMessage roomMsg = new MMOGameRequestMessage((byte)3, this.sessionId, 0, (byte)0, (byte)3, (byte)2, message.length, message);
     
 
 
 
 
     this.masterChannel.SendRequest(roomMsg);
   }
   
 
   private void fireConnectionEvent(byte resultCode)
   {
     Object[] listenersCopy = this.masterServerRequestListeners.toArray();
     for (int i = 0; i < listenersCopy.length; i++) {
       MasterRequestListener listener = (MasterRequestListener)listenersCopy[i];
       if (this.masterServerRequestListeners.contains(listener)) {
         listener.onConnectDone(resultCode);
       }
     }
   }
   
   private void fireDisconnectEvent(byte resultCode)
   {
     Object[] listenersCopy = this.masterServerRequestListeners.toArray();
     for (int i = 0; i < listenersCopy.length; i++) {
       MasterRequestListener listener = (MasterRequestListener)listenersCopy[i];
       if (this.masterServerRequestListeners.contains(listener)) {
         listener.onDisconnectDone(resultCode);
       }
     }
   }
   
   private boolean isNull(String check) {
     return check == null;
   }
   
   private boolean isNotConnected() {
     return getConnectionState() != 0;
   }
 }


