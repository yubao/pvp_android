package com.taobao.lottery.game.multiplayer.client.command;

public abstract interface MMOGamePayloadTypeCode
{
  public static final byte FLAT_STRING = 0;
  public static final byte BINARY = 1;
  public static final byte JSON = 2;
}


