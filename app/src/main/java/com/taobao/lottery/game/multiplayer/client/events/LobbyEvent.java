 package com.taobao.lottery.game.multiplayer.client.events;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;

import org.json.JSONException;
import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class LobbyEvent
 {
   private LobbyData info;
   private byte result;
   
   public LobbyEvent(LobbyData info, byte result)
   {
     this.info = info;
     this.result = result;
   }
   
 
 
 
   public LobbyData getInfo()
   {
     return this.info;
   }
   
 
 
 
   public byte getResult()
   {
     return this.result;
   }
   
 
 
 
 
 
   public static LobbyEvent buildLobbyEvent(MMOGameResponseMessage msg)
     throws JSONException
   {
     LobbyData lobbyInfo = null;
     if (msg.getResultCode() == 0)
     {
       JSONObject jsonLobby = new JSONObject(new String(msg.getPayLoad()));
       lobbyInfo = new LobbyData(jsonLobby.getString("id"), jsonLobby.getString("owner"), jsonLobby.getString("name"), jsonLobby.getInt("maxUsers"), jsonLobby.getBoolean("isPrimary"));
     }
     
 
 
     LobbyEvent lobbyEvent = new LobbyEvent(lobbyInfo, msg.getResultCode());
     return lobbyEvent;
   }
 }


