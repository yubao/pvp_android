 package com.taobao.lottery.game.multiplayer.client.message;
 
 
 
 
 
 public class MMOGameNotifyMessage
   extends MMOGameMessage
 {
   private byte updateType;
   
 
 
 
 
   public MMOGameNotifyMessage(byte type, byte reserved, byte payLoadType, int payLoadSize, byte[] payLoad)
   {
     super(type, reserved, payLoadType, payLoadSize, payLoad);
   }
   
   public MMOGameNotifyMessage(byte type, byte updateType, byte reserved, byte payLoadType, int payLoadSize, byte[] payLoad) {
     super(type, reserved, payLoadType, payLoadSize, payLoad);
     this.updateType = updateType;
   }
   
   public byte getUpdateType()
   {
     return this.updateType;
   }
   
   public void setUpdateType(byte updateType) {
     this.updateType = updateType;
   }
 }


