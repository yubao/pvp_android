package com.taobao.lottery.game.multiplayer.client.listener;

import com.taobao.lottery.game.multiplayer.client.events.LiveRoomInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomEvent;

public abstract interface RoomRequestListener
{
  public abstract void onSubscribeRoomDone(RoomEvent paramRoomEvent);
  
  public abstract void onUnSubscribeRoomDone(RoomEvent paramRoomEvent);
  
  public abstract void onJoinRoomDone(RoomEvent paramRoomEvent, String paramString);
  
  public abstract void onLeaveRoomDone(RoomEvent paramRoomEvent);
  
  public abstract void onGetLiveRoomInfoDone(LiveRoomInfoEvent paramLiveRoomInfoEvent);
  
  public abstract void onSetCustomRoomDataDone(LiveRoomInfoEvent paramLiveRoomInfoEvent);
  
  public abstract void onUpdatePropertyDone(LiveRoomInfoEvent paramLiveRoomInfoEvent, String paramString);
  
  public abstract void onLockPropertiesDone(byte paramByte);
  
  public abstract void onUnlockPropertiesDone(byte paramByte);
  
  public abstract void onRPCDone(byte paramByte, String paramString, Object paramObject);
}


