package com.taobao.lottery.game.multiplayer.client.command;

public abstract interface MMOGameMessageTypeCode
{
  public static final byte REQUEST = 0;
  public static final byte RESPONSE = 1;
  public static final byte UPDATE = 2;
}


