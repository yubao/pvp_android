 package com.taobao.lottery.game.multiplayer.client.events;
 
 
 
 
 public class GameServer
 {
   private Address address;
   
 
   private String[] appKeys;
   
 
 
   GameServer(Address address, String[] appKeys)
   {
     this.address = address;
     this.appKeys = appKeys;
   }
   
   public Address getAddress() {
     return this.address;
   }
   
   public String[] getAppKeys() {
     return this.appKeys;
   }
 }


