 package com.taobao.lottery.game.multiplayer.client.transformer;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameNotifyMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;
import com.taobao.lottery.game.multiplayer.client.util.Util;

import java.nio.ByteBuffer;
 
 
 
 
 
 
 
 
 
 
 
 
 public class MMOGameMessageDecoder
 {
   public static boolean needsMoreData(byte[] bytesToCheck, int startPos, int limit)
   {
     if (limit - startPos <= 7) {
       Util.trace("Header Missing : Bytes avail are " + (limit - startPos));
       return true;
     }
     
     if ((bytesToCheck[startPos] == 1) && (limit - startPos <= 8)) {
       Util.trace("Header Missing : Bytes avail are " + (limit - startPos));
       return true;
     }
     int payLoadSize = 0;
     if (bytesToCheck[startPos] == 1) {
       payLoadSize = bytesToIntgeger(bytesToCheck, startPos + 5);
       if (limit - startPos < 9 + payLoadSize) {
         Util.trace("Payload Missing: Bytes avail are " + (limit - startPos));
         return true;
       }
     }
     else {
       payLoadSize = bytesToIntgeger(bytesToCheck, startPos + 4);
       if (limit - startPos < 8 + payLoadSize) {
         return true;
       }
     }
     
     return false;
   }
   
   private static int bytesToIntgeger(byte[] bytes, int offset)
   {
     int value = 0;
     for (int i = 0; i < 4; i++)
     {
       value = (value << 8) + (bytes[(offset + i)] & 0xFF);
     }
     
     return value;
   }
   
   public static MMOGameMessage decode(ByteBuffer buf)
   {
     byte type = buf.get();
     if (type == 1)
     {
       byte requestType = buf.get();
       byte resultCode = buf.get();
       byte reserved = buf.get();
       byte payLoadType = buf.get();
       int payLoadSize = buf.getInt();
       byte[] payLoadBytes = new byte[payLoadSize];
       buf.get(payLoadBytes);
       MMOGameResponseMessage response = new MMOGameResponseMessage(type, resultCode, requestType, reserved, payLoadType, payLoadSize, payLoadBytes);
       
       return response;
     }
     
 
     byte updateType = buf.get();
     byte reserved = buf.get();
     byte payLoadType = buf.get();
     int payLoadSize = buf.getInt();
     byte[] payLoadBytes = new byte[payLoadSize];
     buf.get(payLoadBytes);
     MMOGameNotifyMessage notification = new MMOGameNotifyMessage(type, updateType, reserved, payLoadType, payLoadSize, payLoadBytes);
     
     return notification;
   }
 }


