package com.taobao.lottery.game.multiplayer.client.listener;

import com.taobao.lottery.game.multiplayer.client.events.LiveRoomInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LobbyEvent;

public abstract interface LobbyRequestListener
{
  public abstract void onJoinLobbyDone(LobbyEvent paramLobbyEvent);
  
  public abstract void onLeaveLobbyDone(LobbyEvent paramLobbyEvent);
  
  public abstract void onSubscribeLobbyDone(LobbyEvent paramLobbyEvent);
  
  public abstract void onUnSubscribeLobbyDone(LobbyEvent paramLobbyEvent);
  
  public abstract void onGetLiveLobbyInfoDone(LiveRoomInfoEvent paramLiveRoomInfoEvent);
}


