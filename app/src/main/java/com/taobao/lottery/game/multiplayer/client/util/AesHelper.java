 package com.taobao.lottery.game.multiplayer.client.util;
 
 
 public class AesHelper
 {
   private static final String IV = "F27D5C9927726BCEFE7510B1BDD3D137";
   private static final String SALT = "3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55";
   private static final int KEY_SIZE = 128;
   private static final int ITERATION_COUNT = 10;
   
   public static String encrypt(String PASSPHRASE, String PLAIN_TEXT)
   {
     AesUtil util = new AesUtil(128, 10);
     String encrypt = util.encrypt("3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55", "F27D5C9927726BCEFE7510B1BDD3D137", PASSPHRASE, PLAIN_TEXT);
     return encrypt;
   }
   
   public static String decrypt(String PASSPHRASE, String CIPHER_TEXT) {
     AesUtil util = new AesUtil(128, 10);
     String decrypt = util.decrypt("3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55", "F27D5C9927726BCEFE7510B1BDD3D137", PASSPHRASE, CIPHER_TEXT);
     return decrypt;
   }
 }


