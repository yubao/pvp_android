 package com.taobao.lottery.game.multiplayer.client;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameNotifyMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameRequestMessage;
import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;
import com.taobao.lottery.game.multiplayer.client.transformer.MMOGameMessageDecoder;
import com.taobao.lottery.game.multiplayer.client.transformer.MMOGameRequestEncoder;
import com.taobao.lottery.game.multiplayer.client.util.Util;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
 
 
 
 
 
 
 class MasterChannel
   implements Runnable
 {
   private String host;
   private int port;
   private SocketChannel theChannel;
   private List<MMOGameRequestMessage> pendingWriteOperations = new LinkedList();
   
   private Selector selector;
   
   private ByteBuffer socketBuffer = ByteBuffer.allocate(4096);
   
 
   private MasterClient theGame;
   
 
   private static boolean waitForMore = false;
   
   private boolean shouldStop = false;
   
 
 
 
 
 
   MasterChannel(String host, int port)
   {
     this.host = host;
     this.port = port;
     try {
       this.theGame = MasterClient.getInstance();
     } catch (Exception ex) {
       Util.trace("Exception in ClientChannel " + ex.getMessage());
     }
   }
   
   private void startConnect()
     throws Exception
   {
     this.selector = SelectorProvider.provider().openSelector();
     
     this.theChannel = SocketChannel.open();
     this.theChannel.configureBlocking(false);
     
 
     this.theChannel.connect(new InetSocketAddress(InetAddress.getByName(this.host), this.port));
     this.theChannel.register(this.selector, 8);
   }
   
   private void channelRead(SelectionKey key) throws Exception
   {
     SocketChannel socketChannel = (SocketChannel)key.channel();
     if (!waitForMore)
     {
 
       this.socketBuffer.clear();
     }
     
 
     int numDecoded = 0;
     
     int posBeforeRead = this.socketBuffer.position();
     int numToRead = socketChannel.read(this.socketBuffer);
     
 
 
     numToRead += posBeforeRead;
     
 
 
     ByteBuffer decodingBuf = ByteBuffer.wrap(this.socketBuffer.array(), 0, numToRead);
     
     while (numDecoded < numToRead) {
       waitForMore = MMOGameMessageDecoder.needsMoreData(decodingBuf.array(), numDecoded, decodingBuf.limit());
       if (waitForMore)
       {
 
 
 
         this.socketBuffer = decodingBuf.compact();
         return;
       }
       MMOGameMessage msg = MMOGameMessageDecoder.decode(decodingBuf);
       
       if (msg.getType() == 1)
       {
         this.theGame.onResponse((MMOGameResponseMessage)msg);
       }
       else
       {
         this.theGame.onNotify((MMOGameNotifyMessage)msg);
       }
       if (msg.getType() == 1)
       {
         numDecoded += 9 + msg.getPayLoadSize();
       }
       else
       {
         numDecoded += 8 + msg.getPayLoadSize();
       }
     }
   }
   
   private synchronized void channelWrite(SelectionKey key)
     throws Exception
   {
     SocketChannel socketChannel = (SocketChannel)key.channel();
     MMOGameRequestMessage msg = (MMOGameRequestMessage)this.pendingWriteOperations.remove(0);
     if (msg == null)
     {
       return;
     }
     ByteBuffer buf = MMOGameRequestEncoder.encode(msg);
     buf.flip();
     socketChannel.write(buf);
     
 
     if (this.pendingWriteOperations.size() <= 0) {
       key.interestOps(1);
     }
   }
   
   private void channelConnect(SelectionKey key) throws Exception
   {
     SocketChannel socketChannel = (SocketChannel)key.channel();
     
 
 
 
     socketChannel.finishConnect();
     
     key.interestOps(1);
     this.theGame.onConnect(true);
   }
   
   public void run()
   {
     try
     {
       startConnect();
       for (;;)
       {
         this.selector.select();
         if (this.shouldStop) {
           Util.trace("shouldStop... returning");
           return;
         }
         
         Iterator selectedKeys = this.selector.selectedKeys().iterator();
         while (selectedKeys.hasNext()) {
           SelectionKey key = (SelectionKey)selectedKeys.next();
           selectedKeys.remove();
           
           if (key.isValid())
           {
 
 
 
             if (key.isConnectable()) {
               channelConnect(key);
             } else if (key.isReadable()) {
               channelRead(key);
             } else if (key.isWritable()) {
               channelWrite(key);
             }
           }
         }
       }
       
 
 
 
 
 
     }
     catch (Exception e)
     {
       Util.trace("Exception " + e.getClass() + " in thread run " + e.getMessage());
       disconnect();
       this.theGame.onConnect(false);
     }
   }
   
   synchronized void SendRequest(MMOGameRequestMessage authMsg)
   {
     this.pendingWriteOperations.add(authMsg);
     SelectionKey key = this.theChannel.keyFor(this.selector);
     if ((key == null) || (!key.isValid())) {
       Util.trace("key " + key + " is invalid.");
       disconnect();
       this.theGame.onConnect(false);
       return;
     }
     key.interestOps(4);
     this.selector.wakeup();
   }
   
   void disconnect() {
     this.shouldStop = true;
     try {
       this.theChannel.close();
       this.selector.wakeup();
     } catch (Exception ex) {
       Util.trace("Exception in disconnect closing the channel " + ex);
     }
   }
 }


