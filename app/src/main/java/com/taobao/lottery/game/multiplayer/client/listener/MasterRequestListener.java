package com.taobao.lottery.game.multiplayer.client.listener;

import com.taobao.lottery.game.multiplayer.client.events.AllServerEvent;

public abstract interface MasterRequestListener
{
  public abstract void onConnectDone(byte paramByte);
  
  public abstract void onDisconnectDone(byte paramByte);
  
  public abstract void onGetAllServerDone(AllServerEvent paramAllServerEvent);
  
  public abstract void onCustomMessageReceived(byte[] paramArrayOfByte);
}


