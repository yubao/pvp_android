 package com.taobao.lottery.game.multiplayer.client.events;
 
 import com.taobao.lottery.game.multiplayer.client.message.MMOGameResponseMessage;

import org.json.JSONException;
import org.json.JSONObject;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class AllRoomsEvent
 {
   private byte result;
   private String[] roomIds;
   
   public AllRoomsEvent(byte result, String[] rooms)
   {
     this.roomIds = rooms;
     this.result = result;
   }
   
 
 
 
   public byte getResult()
   {
     return this.result;
   }
   
 
 
 
   public String[] getRoomIds()
   {
     return this.roomIds;
   }
   
 
 
 
 
 
   public static AllRoomsEvent buildAllRoomsEvent(MMOGameResponseMessage msg)
     throws JSONException
   {
     String[] roomIds = null;
     if (msg.getResultCode() == 0)
     {
       JSONObject jsonRooms = new JSONObject(new String(msg.getPayLoad()));
       String jsonRoomIds = jsonRooms.getString("ids");
       if (jsonRoomIds.length() > 0) {
         roomIds = jsonRoomIds.split(";");
       }
     }
     return new AllRoomsEvent(msg.getResultCode(), roomIds);
   }
 }


