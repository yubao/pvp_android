package com.gamepind.pvp;

import android.app.Application;
import android.content.Context;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.gamepind.pvp.demo.Constants;
import com.gamepind.pvp.demo.Utils;
import com.taobao.lottery.game.multiplayer.client.MMOGameClient;

/**
 * @author 张玉保 2019/1/21.
 *
 * 描述：
 */
public class PVPApplication extends Application {

    private static Context sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        Fresco.initialize(this);
//        initMMOGameClient();
        MMOGameClient.initialize(Constants.APP_KEY, Constants.HOST_NAME);
        MMOGameClient.setRecoveryAllowance(Constants.RECCOVERY_ALLOWANCE_TIME);
//        startService(new Intent(getContext(), DownloadService.class));

        // 初始化内存分析工具
//        if (!LeakCanary.isInAnalyzerProcess(this)) {
//            LeakCanary.install(this);
//        }
    }


    public static Context getContext(){
        return sInstance;
    }
}
