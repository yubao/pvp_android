package com.gamepind.pvp.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gamepind.pvp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author 张玉保 2019/1/22.
 *
 * 描述：自定义顶部栏
 */
public class CustomToolBar extends RelativeLayout {

    private Context mContext;
    @BindView(R.id.text_tittle)
    TextView mTextViewTittle;

    public CustomToolBar(Context context) {
        super(context);
        initView(context);
    }

    public CustomToolBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CustomToolBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }


    private void initView(Context context) {
        this.mContext = context;
        View view = LayoutInflater.from(context).inflate(R.layout.custom_toolbar_layout, this);
        ButterKnife.bind(this, view);

    }


    @OnClick(R.id.image_back)
    public void back() {
        if (mContext instanceof Activity) {
            ((Activity) mContext).finish();
        }
    }


    public void setText(String text) {
        mTextViewTittle.setText(text);
    }
}
