package com.gamepind.pvp.demo;

/**
 * Created by Administrator on 2019/1/25/025.
 */

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gamepind.pvp.R;

public class ExampleFragment extends Fragment {
    public ExampleFragment() {
        // Required empty public constructor
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment1,container,false);  // 此处的布局文件是普通的线性布局（此博客忽略）
    }
}
