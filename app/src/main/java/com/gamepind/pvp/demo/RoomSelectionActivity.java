package com.gamepind.pvp.demo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.gamepind.pvp.R;
import com.gamepind.pvp.activity.MatchPendingActivity;
import com.taobao.lottery.game.multiplayer.client.MMOGameClient;
import com.taobao.lottery.game.multiplayer.client.command.MMOGameResponseResultCode;
import com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent;
import com.taobao.lottery.game.multiplayer.client.events.ChatEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveRoomInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveUserInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LobbyData;
import com.taobao.lottery.game.multiplayer.client.events.MatchedRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.MoveEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomData;
import com.taobao.lottery.game.multiplayer.client.events.RoomEvent;
import com.taobao.lottery.game.multiplayer.client.events.UpdateEvent;
import com.taobao.lottery.game.multiplayer.client.listener.NotifyListener;
import com.taobao.lottery.game.multiplayer.client.listener.RoomRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.ZoneRequestListener;

import java.util.HashMap;


public class RoomSelectionActivity extends Activity implements ZoneRequestListener, RoomRequestListener ,NotifyListener{
	
	private MMOGameClient theClient;
	private ProgressDialog progressDialog;
	private int roomSize = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.room_selection);
		getMMOGameClientInstance();
	}
	
	private void getMMOGameClientInstance(){
        try {
            theClient = MMOGameClient.getInstance();
        } catch (Exception ex) {
        	Utils.showToastAlert(this, Constants.ALERT_INIT_EXEC);
        }
    }
	
	@Override
	public void onStart(){
		super.onStart();
		theClient.addZoneRequestListener(this);
		theClient.addRoomRequestListener(this);
		theClient.addNotificationListener(this);
		roomSize = -1;
	}
	
	@Override
	public void onStop(){
		super.onStop();
		theClient.removeZoneRequestListener(this);
		theClient.removeRoomRequestListener(this);
		theClient.removeNotificationListener(this);

	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		//theClient.disconnect();
	}
	
	public void onJoinTwoUserClicked(View view){
		roomSize = 2;
		HashMap<String, Object> table = new HashMap<String, Object>();
		table.put("twoUser", true);
		theClient.joinRoomWithProperties(table);
	}
	
	public void onJoinThreeUserClicked(View view){
//		roomSize = 3;
//		HashMap<String, Object> table = new HashMap<String, Object>();
//		table.put("threeUser", true);
//		theClient.joinRoomWithProperties(table);
		roomSize = 2;
		HashMap<String, Object> table = new HashMap<String, Object>();
		table.put("twoUser", true);
		theClient.joinRoomWithProperties(table);
	}
	
	@Override
	public void onCreateRoomDone(final RoomEvent event, String desc) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(progressDialog!=null){
					progressDialog.dismiss();
					progressDialog=null;
				}
				if(event.getResult()== MMOGameResponseResultCode.SUCCESS){// if room created successfully
					theClient.joinRoom(event.getData().getId());
				}else{
					Utils.showToastAlert(RoomSelectionActivity.this, Constants.ALERT_ROOM_CREATE + event.getResult());
				}
			}
		});
	}
	
	@Override
	public void onDeleteRoomDone(RoomEvent event, String desc) {
		
	}
	@Override
	public void onGetAllRoomsDone(AllRoomsEvent event) {
		
	}
	@Override
	public void onGetLiveUserInfoDone(LiveUserInfoEvent event) {
		
	}
	@Override
	public void onGetMatchedRoomsDone(final MatchedRoomsEvent event){
		
	}
	@Override
	public void onGetOnlineUsersDone(AllUsersEvent arg0) {
		
	}
	@Override
	public void onSetCustomUserDataDone(LiveUserInfoEvent arg0) {
		
	}
	
	public void startGame(String roomId, int maxUsers){
//		Intent intent = new Intent(this, GameActivity.class);
		Intent intent = new Intent(this, MatchPendingActivity.class);
		intent.putExtra("roomId", roomId);
		intent.putExtra("gameType", maxUsers);
		startActivity(intent);
		finish();
	}
	
	@Override
	public void onRPCDone(byte arg0, String arg1, Object arg2) {
		
	}

	@Override
	public void onGetLiveRoomInfoDone(LiveRoomInfoEvent arg0) {
		
	}

	@Override
	public void onJoinRoomDone(final RoomEvent event, final String desc) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
					theClient.subscribeRoom(event.getData().getId());
				}else{
					HashMap<String, Object> table = new HashMap<String, Object>();
					if(roomSize==2){
						table.put("twoUser", true);
					}else if(roomSize==3){
						table.put("threeUser", true);
					}
					progressDialog = ProgressDialog.show(RoomSelectionActivity.this , "", "Creating room...");
		       		theClient.createTurnRoom(String.valueOf(System.currentTimeMillis()), "owner", roomSize, table, Constants.TURN_TIME);
				}
			}
		});
	}

	@Override
	public void onLeaveRoomDone(RoomEvent arg0) {
		
	}

	@Override
	public void onLockPropertiesDone(byte arg0) {
		
	}

	@Override
	public void onSetCustomRoomDataDone(LiveRoomInfoEvent arg0) {
		
	}

	@Override
	public void onSubscribeRoomDone(RoomEvent event) {
		if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
			//发送进入 某个房间

			theClient.sendPrivateChat("aaa",event.getData().getId());
			Log.e("onPrivateChatReceived","paramString1-----"+event.getData().getId());

			startGame(event.getData().getId(), event.getData().getMaxUsers());
		}else{
			Utils.showToastAlertOnUIThread(this, "onSubscribeRoomDone Failed"+event.getResult());
		}
		
	}

	@Override
	public void onUnSubscribeRoomDone(RoomEvent arg0) {
		
	}

	@Override
	public void onUnlockPropertiesDone(byte arg0) {
		
	}

	@Override
	public void onUpdatePropertyDone(LiveRoomInfoEvent arg0, String arg1) {
		
	}

	@Override
	public void onRoomCreated(RoomData paramRoomData) {

	}

	@Override
	public void onRoomDestroyed(RoomData paramRoomData) {

	}

	@Override
	public void onUserLeftRoom(RoomData paramRoomData, String paramString) {

	}

	@Override
	public void onUserJoinedRoom(RoomData paramRoomData, String paramString) {

	}

	@Override
	public void onUserLeftLobby(LobbyData paramLobbyData, String paramString) {

	}

	@Override
	public void onUserJoinedLobby(LobbyData paramLobbyData, String paramString) {

	}

	@Override
	public void onChatReceived(ChatEvent paramChatEvent) {

	}

	@Override
	public void onPrivateChatReceived(String paramString1, String paramString2) {
			//获得信息
		Log.e("onPrivateChatReceived","paramString1-----"+paramString1+"rid-----"+paramString2);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(RoomSelectionActivity.this);
				builder.setMessage("big邀请你玩游戏")
						.setCancelable(false)
						.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
//								theClient.joinRoomWithProperties();
								HashMap<String, Object> table = new HashMap<String, Object>();
								table.put("twoUser", true);
								theClient.joinRoomWithProperties(table);
//								startGame(paramString2, 2);
								onBackPressed();
							}
						})
						.setNegativeButton("拒绝", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								theClient.sendPrivateChat("bbb","no");
								onBackPressed();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});


	}

	@Override
	public void onUpdatePeersReceived(UpdateEvent paramUpdateEvent) {

	}

	@Override
	public void onUserChangeRoomProperty(RoomData paramRoomData, String paramString, HashMap<String, Object> paramHashMap, HashMap<String, String> paramHashMap1) {

	}

	@Override
	public void onMoveCompleted(MoveEvent paramMoveEvent) {

	}

	@Override
	public void onGameStarted(String paramString1, String paramString2, String paramString3) {

	}

	@Override
	public void onGameStopped(String paramString1, String paramString2) {

	}

	@Override
	public void onUserPaused(String paramString1, boolean paramBoolean, String paramString2) {

	}

	@Override
	public void onUserResumed(String paramString1, boolean paramBoolean, String paramString2) {

	}
}
