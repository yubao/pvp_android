package com.gamepind.pvp.demo;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.gamepind.pvp.R;
import com.taobao.lottery.game.multiplayer.client.MMOGameClient;
import com.taobao.lottery.game.multiplayer.client.command.MMOGameResponseResultCode;
import com.taobao.lottery.game.multiplayer.client.events.ConnectEvent;
import com.taobao.lottery.game.multiplayer.client.listener.ConnectionRequestListener;

public class GameMainActivity extends Activity implements ConnectionRequestListener {

	private EditText nameEditText;
	private MMOGameClient theClient;
    public ProgressDialog progressDialog;
    private String authData = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_demo);
		nameEditText = (EditText)findViewById(R.id.nameEditText);
		init();
	}
	
	public void onPlayGameClicked(View view){
		if(nameEditText.getText().length()==0){
			Utils.showToastAlert(this, getApplicationContext().getString(R.string.enterName));
			return;
		}
		String userName = nameEditText.getText().toString().trim();
		loginToApp(userName, "");
		
	}
	public void onInviteeClicked(View view){
		if(nameEditText.getText().length()==0){
			Utils.showToastAlert(this, getApplicationContext().getString(R.string.enterName));
			return;
		}
		String userName = nameEditText.getText().toString().trim();
		loginToApp(userName, "");

	}


	
	private void loginToApp(String name, String authData){
		Log.d("loginToApp", ""+name);
		Utils.userName = name;
		theClient.connectWithUserName(name, authData);
		progressDialog =  ProgressDialog.show(this, "", "connecting to app");
		progressDialog.setCancelable(true);
	}
	

	private void init(){
		try {
			MMOGameClient.initialize(Constants.APP_KEY, Constants.HOST_NAME);
			MMOGameClient.setRecoveryAllowance(Constants.RECCOVERY_ALLOWANCE_TIME);
			theClient = MMOGameClient.getInstance();
		} catch (Exception ex) {
        	Utils.showToastAlert(this, Constants.ALERT_INIT_EXEC);
        }
    }
	
	@Override 
	public void onStart(){
		super.onStart();
		theClient.addConnectionRequestListener(this); 
	}
	
	@Override
	public void onStop(){
		super.onStop();
		theClient.removeConnectionRequestListener(this); 
		
	}
	
	public void goToRoomList(){
		Log.d("goToRoomList", "goToRoomList called");
		Intent intent = new Intent(getApplicationContext(), RoomSelectionActivity.class);
		startActivity(intent);
	}
	
	@Override
	public void onConnectDone(final ConnectEvent event, String desc) {
		Log.d("onConnectDone", event.getResult()+" listener "+desc);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(progressDialog!=null){
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (event.getResult() == MMOGameResponseResultCode.SUCCESS) {
					goToRoomList();
				}else if (event.getResult() == MMOGameResponseResultCode.BAD_REQUEST) {
					theClient.disconnect();
				}else {
					Utils.showToastAlert(GameMainActivity.this, Constants.ALERT_CONN_FAIL + event.getResult());
				}
			}
		});
	}

	@Override
	public void onDisconnectDone(ConnectEvent event) {
		if (event.getResult() == MMOGameResponseResultCode.SUCCESS) {
			
		} else {
			Utils.showToastAlertOnUIThread(GameMainActivity.this, Constants.ALERT_ERR_DISCONN);
		}
	}

	@Override
	public void onInitUDPDone(final byte resultCode) {

	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        Log.d("onActivityResult", "onActivityResult");
//        FacebookService.instance().authorizeCallback(requestCode, resultCode, data);
    }
	
	public void onFacebookProfileRetreived(boolean success) {
//		Log.d("onFacebookProfileRetreived", ""+success);
//		if(progressDialog!=null){
//			progressDialog.dismiss();
//			progressDialog = null;
//		}
//		if(success){
//			// do success logic
//			Log.d("UserContext.AccessToken", UserContext.AccessToken);
//			Log.d("UserContext.MyUserName", UserContext.MyUserName);
//			try {
//				JSONObject data = new JSONObject();
//				data.put("token", UserContext.AccessToken);
//				loginToApp(UserContext.MyUserName, data.toString());
//			} catch (Exception e) {
//				Utils.showToastAlert(this, "onFacebookProfileRetreived"+e.toString());
//			}
//		}
	}
	
}
