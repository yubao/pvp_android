package com.gamepind.pvp.demo;
public class Constants {
	
	public static String APP_KEY = "f2aac7ca-00e0-47ca-a";
	
    //public static String HOST_NAME =  "30.27.184.26";
//    public static String HOST_NAME =  "192.168.43.5";
//    public static String HOST_NAME =  "192.168.43.59";
//    public static String HOST_NAME =  "172.20.10.10";
//    public static String HOST_NAME =  "172.20.10.2";
    public static String HOST_NAME =  "10.10.33.23";

//    192.168.43.59




    // 192.168.43.5:12346
    public static final String FB_APP_ID = "";
    
    public static final int RECCOVERY_ALLOWANCE_TIME = 60;
    
    public static final int MAX_RECOVERY_ATTEMPT = 10;
    
    public static final int TURN_TIME = 120;
    
    public static final int TOTAL_CARDS = 52;
    
    public static String SERVER_NAME = "MMOGameServer";
    
    public static final byte USER_HAND = 1;
    
    public static final byte RESULT_GAME_OVER = 3;
    public static final byte RESULT_USER_LEFT = 4;
    
    // error code
    public static final int INVALID_MOVE = 121;
    public static final byte SUBMIT_CARD = 111;
    
    // GAME_STATUS
    
    public static final int STOPPED = 71;
    public static final int RUNNING = 72;
    public static final int PAUSED = 73;
    
    // String constants
    
    public static String RECOVER_TEXT = "Recovering...";
    
    // Alert Messages
    public static String ALERT_INIT_EXEC = "Exception in Initilization";
    public static String ALERT_ERR_DISCONN = "Can't Disconnect";
    public static String ALERT_INV_MOVE = "Invalid Move: Not Your Turn";
    public static String ALERT_ROOM_CREATE = "Room creation failed";
    public static String ALERT_CONN_FAIL = "Connection Failed";
    public static String ALERT_SEND_FAIL = "Send Move Failed";
    public static String ALERT_CONN_SUCC = "Connection Success";
    public static String ALERT_CONN_RECOVERED = "Connection Recovered";
    public static String ALERT_CONN_ERR_RECOVABLE = "Recoverable connection error. Recovering session after 5 seconds";
    public static String ALERT_CONN_ERR_NON_RECOVABLE = "Non-recoverable connection error.";
    
    public static final String HOW_TO_PLAY = "Login: You can login with";
}
