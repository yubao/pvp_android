package com.gamepind.pvp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.utils.ScreenUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Roybal 2019/1/24.
 * <p>
 * 描述：游戏邀请界面底部游戏列表
 */
public class GameAdapter extends RecyclerView.Adapter<GameAdapter.ViewHolder> {

    private Context mContext;
    private List<GameInfo> datas = new ArrayList<>();


    public GameAdapter(Context context) {
        mContext = context;
    }

    public void setData(List<GameInfo> dataList) {
        if (null != dataList) {
            this.datas.clear();
            this.datas.addAll(dataList);
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_game_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GameInfo gameInfo = datas.get(position);
        holder.mGameCover.setImageURI(gameInfo.getGameImgUrl());
        holder.mGameNameView.setText(gameInfo.getGameName());
        holder.mParentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onGameClick(gameInfo);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.parent_container)
        RelativeLayout mParentContainer;

        @BindView(R.id.game_cover)
        SimpleDraweeView mGameCover;

        @BindView(R.id.text_game_name)
        TextView mGameNameView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ViewGroup.LayoutParams params = mParentContainer.getLayoutParams();
            params.height = ScreenUtils.getScreenHeight(mContext) / 4;
            params.width = ScreenUtils.getScreenWidth(mContext) / 4;
            itemView.setLayoutParams(params);
        }
    }

    private OnGameItemClickListener mItemClickListener;

    public void setItemClickListener(
            OnGameItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public interface OnGameItemClickListener {
        void onGameClick(GameInfo gameInfo);
    }
}
