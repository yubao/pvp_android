package com.gamepind.pvp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.activity.GameChatActivity;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.User;
import com.gamepind.pvp.model.remote.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author 张玉保 2019/1/23.
 *
 * 描述：
 */
public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.ViewHolder> {

    private Context mContext;
    private List<Friend> datas = new ArrayList<>();
    private static final int ONLINE = 0x01;
    private static final int INVITED = 0x01;

    public FriendsListAdapter(Context context) {
        this.mContext = context;
    }

    public void setData(List<Friend> dataList) {
        if (null != dataList) {
            this.datas.clear();
            this.datas.addAll(dataList);
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(mContext).inflate(R.layout.item_friend_layout, parent,
                false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Friend user = datas.get(position);
        holder.mUserAvatarView.setImageURI(user.getImageId());
        holder.mDateTimeView.setText(user.getLastTime());
        holder.mUsernameView.setText(user.getUserName());
        if (Integer.valueOf(user.getOnline()) == ONLINE) {
            holder.mOnlineView.setVisibility(View.VISIBLE);
        } else {
            holder.mOnlineView.setVisibility(View.GONE);
        }
        if (Integer.valueOf(user.getInvited())== INVITED) {
            holder.mPointView.setVisibility(View.VISIBLE);
        } else {
            holder.mPointView.setVisibility(View.GONE);
        }

        holder.mParentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GameChatActivity.class);
                intent.putExtra(Constant.MEMBER, user);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.parent_container)
        RelativeLayout mParentContainer;

        @BindView(R.id.image_avatar)
        SimpleDraweeView mUserAvatarView;

        @BindView(R.id.image_point)
        ImageView mPointView;

        @BindView(R.id.text_online)
        TextView mOnlineView;

        @BindView(R.id.text_username)
        TextView mUsernameView;

        @BindView(R.id.text_date)
        TextView mDateTimeView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
