package com.gamepind.pvp.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Roybal 2019/1/24.
 *
 * 描述：游侠聊天界面
 */
public class GameChatAdapter extends RecyclerView.Adapter<GameChatAdapter.ViewHolder> {

    private Context mContext;
    private List<GameInfo> datas = new ArrayList<>();
    private static final int FROM_OTHER = 0x00;
    private static final int FROM_ME = 0x01;

    private static final int FAILURE = 0x00;
    private static final int SUCCEESS = 0x01;
    private static final int TIMEOUT = 0x02;
    private Friend mFriend;


    public GameChatAdapter(Context context) {
        this.mContext = context;
    }


    public void setData(List<GameInfo> dataList) {
        if (null != dataList) {
            this.datas.clear();
            this.datas.addAll(dataList);
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView;
        if (viewType == FROM_OTHER) {
            rootView = LayoutInflater.from(mContext).inflate(R.layout.item_game_left, parent,
                    false);
        } else {
            rootView = LayoutInflater.from(mContext).inflate(R.layout.item_game_right, parent,
                    false);
        }
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GameInfo gameInfo = datas.get(position);
        holder.mDatetimeView.setText(gameInfo.getInvitedate());
        holder.mGameCoverView.setImageURI(Uri.parse(gameInfo.getGameImgUrl()));
        //状态
        if (gameInfo.getInviteResult() == TIMEOUT || gameInfo.getInviteResult() == FAILURE) {
            holder.mImageCoverView.setVisibility(View.VISIBLE);
        } else {
            holder.mImageCoverView.setVisibility(View.GONE);
        }
        // 邀请别人还是别人邀请自己 1.邀请别人
        if (gameInfo.getActive() == FROM_OTHER) {
            holder.mUserAcatarView.setImageURI(Uri.parse(Friend.getInstance().getImageId()));
        } else {
            holder.mUserAcatarView.setImageURI(Uri.parse(mFriend.getImageId()));
        }

        //接受
        holder.mAcceptView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2019/1/25  add by 张玉保
                ToastUtils.show("accept");
            }
        });
        // 拒绝
        holder.mLaterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2019/1/24  add by 张玉保
                ToastUtils.show("refuse");
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    @Override
    public int getItemViewType(int position) {
        GameInfo gameInfo = datas.get(position);
        if (gameInfo.getActive() == FROM_OTHER) {
            return FROM_OTHER;
        } else {
            return FROM_ME;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_msg_datetime)
        TextView mDatetimeView;

        @BindView(R.id.image_avatar)
        SimpleDraweeView mUserAcatarView;

        @BindView(R.id.game_cover)
        SimpleDraweeView mGameCoverView;

        @BindView(R.id.text_accept)
        TextView mAcceptView;

        @BindView(R.id.text_later)
        TextView mLaterView;

        @BindView(R.id.view_splilt)
        View mSplitView;

        @BindView(R.id.image_cover)
        ImageView mImageCoverView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public void setFriend(Friend friend) {
        mFriend = friend;
    }
}
