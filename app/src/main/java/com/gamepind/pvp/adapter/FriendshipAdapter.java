package com.gamepind.pvp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.activity.WaitingActivity;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.model.bean.User;
import com.gamepind.pvp.model.remote.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author 张玉保 2019/1/23.
 *
 * 描述：
 */
public class FriendshipAdapter extends RecyclerView.Adapter<FriendshipAdapter.ViewHolder> {

    private Context mContext;
    private List<Friend> datas = new ArrayList<>();
    private GameInfo mGameInfo;

    public FriendshipAdapter(Context context) {
        this.mContext = context;
    }

    public void setData(List<Friend> dataList) {
        if (null != dataList) {
            this.datas.clear();
            this.datas.addAll(dataList);
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(mContext).inflate(R.layout.item_friend_invite, parent,
                false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Friend user = datas.get(position);
        holder.mUserAvatarView.setImageURI(user.getImageId());
        holder.mUsernameView.setText(user.getUserName());
        if (Integer.valueOf(user.getOnline()) == 1) {
            holder.mOnlineView.setVisibility(View.VISIBLE);
        } else {
            holder.mOnlineView.setVisibility(View.GONE);
        }
        holder.mInviteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, WaitingActivity.class);
                intent.putExtra(Constant.MEMBER, user);
                intent.putExtra(Constant.GAME,mGameInfo);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_avatar)
        SimpleDraweeView mUserAvatarView;

        @BindView(R.id.text_online)
        TextView mOnlineView;

        @BindView(R.id.text_username)
        TextView mUsernameView;

        @BindView(R.id.text_date)
        TextView mInviteView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setGameInfo(GameInfo gameInfo) {
        mGameInfo = gameInfo;
    }
}
