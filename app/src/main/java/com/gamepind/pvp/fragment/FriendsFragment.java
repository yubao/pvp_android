package com.gamepind.pvp.fragment;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.gamepind.pvp.R;
import com.gamepind.pvp.adapter.FriendsListAdapter;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.FriendList;
import com.gamepind.pvp.model.bean.User;
import com.gamepind.pvp.model.remote.RemoteRepository;
import com.gamepind.pvp.ui.base.BaseFragment;
import com.gamepind.pvp.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author 张玉保 2019/1/22.
 *
 * 描述：好友列表页
 */
public class FriendsFragment extends BaseFragment {


    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.ll_friend_no)
    LinearLayout mFriendsIdlelView;

    private FriendsListAdapter mAdapter;

    @Override
    protected int getContentId() {
        return R.layout.fragment_friend;
    }

    @Override
    protected void initData() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new FriendsListAdapter(getActivity());
        mRecyclerView.setAdapter(mAdapter);

        RemoteRepository.getInstance().findFriends(Friend.getInstance().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<FriendList>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addDisposable(d);
                    }

                    @Override
                    public void onSuccess(FriendList value) {
                        List<Friend> friends = value.getResponseMessage();
                        mAdapter.setData(friends);
                        if (friends != null && friends.size() != 0) {
                            mFriendsIdlelView.setVisibility(View.GONE);
                        } else {
                            mFriendsIdlelView.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Log", e.getMessage());
                    }
                });
    }
}
