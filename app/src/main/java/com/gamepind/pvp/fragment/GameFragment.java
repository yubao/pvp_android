package com.gamepind.pvp.fragment;

import android.content.Intent;
import android.net.Uri;
import android.widget.RelativeLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.activity.LoginActivity;
import com.gamepind.pvp.activity.MatchActivity;
import com.gamepind.pvp.activity.WebViewActivity;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.ui.base.BaseFragment;
import com.gamepind.pvp.utils.LoginHelper;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author 张玉保 2019/1/22.
 *
 * 描述：游戏页
 */
public class GameFragment extends BaseFragment {

    @BindView(R.id.game_play)
    RelativeLayout mGameLeftView;

    @BindView(R.id.game_play_right)
    RelativeLayout mGameRightView;

    @BindView(R.id.sdv_image_left)
    SimpleDraweeView mLeftImageView;

    @BindView(R.id.sdv_image_right)
    SimpleDraweeView mRightImageView;


    @Override
    protected int getContentId() {
        return R.layout.fragment_game;
    }

    @Override
    protected void initData() {
        Uri uri = Uri.parse("res://com.gamepind.pvp/" + R.mipmap.game_cover);
        Uri urino = Uri.parse("res://com.gamepind.pvp/" + R.mipmap.home_game1);
        mLeftImageView.setImageURI(uri);
        mRightImageView.setImageURI(urino);
    }

    @OnClick(R.id.game_play)
    public void clickGameLeft() {
        if (!LoginHelper.isLogin()) {
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        }else{
            startPlay();
        }
    }

    /**
     * 开始游戏
     */
    private void startPlay() {
        GameInfo gameInfo = new GameInfo();
        gameInfo.setGameImgUrl("res://com.gamepind.pvp/" + R.mipmap.game_cover);
        gameInfo.setGameName("Angry Bird2");
        Intent intent = new Intent(getActivity(), MatchActivity.class);
        intent.putExtra(Constant.GAME,gameInfo);
        startActivity(intent);
    }



    @OnClick(R.id.game_play_right)
    public void startWebViewActivity(){
        getActivity().startActivity(new Intent(getActivity(),WebViewActivity.class));
    }
}
