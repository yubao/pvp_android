package com.gamepind.pvp.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.activity.GameChatActivity;
import com.gamepind.pvp.activity.MainActivity;
import com.gamepind.pvp.activity.MatchActivity;
import com.gamepind.pvp.activity.MatchPendingActivity;
import com.gamepind.pvp.activity.WaitingActivity;
import com.gamepind.pvp.demo.Constants;
import com.gamepind.pvp.demo.GameActivity;
import com.gamepind.pvp.demo.Utils;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.model.bean.ResultInfo;
import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.model.remote.RemoteRepository;
import com.gamepind.pvp.ui.base.BaseFragment;
import com.gamepind.pvp.utils.ToastUtils;
import com.taobao.lottery.game.multiplayer.client.MMOGameClient;
import com.taobao.lottery.game.multiplayer.client.command.MMOGameResponseResultCode;
import com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent;
import com.taobao.lottery.game.multiplayer.client.events.ChatEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveRoomInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveUserInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LobbyData;
import com.taobao.lottery.game.multiplayer.client.events.MatchedRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.MoveEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomData;
import com.taobao.lottery.game.multiplayer.client.events.RoomEvent;
import com.taobao.lottery.game.multiplayer.client.events.UpdateEvent;
import com.taobao.lottery.game.multiplayer.client.listener.NotifyListener;
import com.taobao.lottery.game.multiplayer.client.listener.RoomRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.ZoneRequestListener;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Roybal 2019/1/24.
 *
 * 描述：胜利结果
 */
public class ResultWinFragment extends BaseFragment implements ZoneRequestListener, RoomRequestListener,NotifyListener {

    public static final String WIN = "win";
    public static final String FRIEND = "friend";
    public static final String GAME = "game";

    private boolean mIsWin = true;
    private Disposable mDisposable;
    private boolean mIsBackMode;
    private  String roomId = "";//退出房間用

    @BindView(R.id.parent_container)
    LinearLayout mLinearContainer;

    @BindView(R.id.text_game_name)
    TextView mGameNameView;

    @BindView(R.id.text_win_score)
    TextView mLeftScoreView;

    @BindView(R.id.text_lose_score)
    TextView mRightScoreView;

    @BindView(R.id.image_avatar_left)
    SimpleDraweeView mLeftAvatarView;

    @BindView(R.id.image_avatar_right)
    SimpleDraweeView mRightAvatarView;

    @BindView(R.id.text_name_left)
    TextView mUsernameLeftView;

    @BindView(R.id.text_name_right)
    TextView mUsernameRightView;

    @BindView(R.id.text_opponent)
    TextView mOpponentView;

    @BindView(R.id.text_playagain)
    TextView mPlayagainView;

    @BindView(R.id.status_battale)
    ImageView mImageView;

    @BindView(R.id.text_add_friend)
    TextView mAddFriendView;

    boolean isAagin = false;
    boolean isNew = false;
    private MMOGameClient theClient;
    private void getMMOGameClientInstance(){
        Utils.userName= Friend.getInstance().getUserName();
//        MMOGameClient.initialize(Constants.APP_KEY, Constants.HOST_NAME);
//        MMOGameClient.setRecoveryAllowance(Constants.RECCOVERY_ALLOWANCE_TIME);
        try {
            theClient = MMOGameClient.getInstance();
//            theClient.disconnect();
            if( theClient.getConnectionState()!=0){
                //用户userName
                theClient.connectWithUserName( Utils.userName, "");
                theClient.addNotificationListener(this);
                theClient.addZoneRequestListener(this);
                theClient.addRoomRequestListener(this);
            }else{
                theClient.addNotificationListener(this);
                theClient.addZoneRequestListener(this);
                theClient.addRoomRequestListener(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private boolean mIsMore;
//    private Friend mFriend;
//    private GameInfo mGameInfo;


    @Override
    protected int getContentId() {
        return R.layout.fragment_win_result;
    }
    String nextTurn ="";

    @Override
    protected void initData() {
        mIsWin = (boolean) getArguments().get(WIN);
        nextTurn = (String) getArguments().get("opponent");
        Log.e("onPrivateChatReceived","opponent-----"+nextTurn);
        Log.e("onPrivateChatReceived","opponent-----"+Utils.userName);


//        mGameInfo = (GameInfo) getArguments().get(GAME);

//        if (mGameInfo != null) {
//            mGameNameView.setText(mGameInfo.getGameName());
//        }

        mUsernameLeftView.setText(Friend.getInstance().getUserName());
        mUsernameRightView.setText(nextTurn);

        mLeftAvatarView.setImageURI(Uri.parse(Friend.getInstance().getImageId()));
//        mLeftAvatarView.setImageURI(Uri.parse("iii"));


//        if (mGameInfo != null) {
//            mGameNameView.setText(mGameInfo.getGameName());
//        }
//
        mUsernameLeftView.setText(Friend.getInstance().getUserName());
//        mUsernameRightView.setText(mFriend.getUserName());
//
//        mLeftAvatarView.setImageURI(Uri.parse(Friend.getInstance().getImageId()));
//        mLeftAvatarView.setImageURI(Uri.parse(mFriend.getImageId()));
//        mLeftAvatarView.setImageURI(Uri.parse("res://com.gamepind.pvp/" + R.mipmap.neo));
        if(nextTurn.equals("emma")){
            mRightAvatarView.setImageURI(Uri.parse("res://com.gamepind.pvp/" + R.mipmap.emma));
        }
        if(nextTurn.equals("kaven")){
            mRightAvatarView.setImageURI(Uri.parse("res://com.gamepind.pvp/" + R.mipmap.kaven));
        }
        if(nextTurn.equals("leo")){
            mRightAvatarView.setImageURI(Uri.parse("res://com.gamepind.pvp/" + R.mipmap.leo));
        }
        if(nextTurn.equals("lili")){
            mRightAvatarView.setImageURI(Uri.parse("res://com.gamepind.pvp/" + R.mipmap.lili));
        }
        if(nextTurn.equals("meimei")){
            mRightAvatarView.setImageURI(Uri.parse("res://com.gamepind.pvp/" + R.mipmap.meimei));
        }
        if(nextTurn.equals("neo")){
            mRightAvatarView.setImageURI(Uri.parse("res://com.gamepind.pvp/" + R.mipmap.neo));
        }
        if(nextTurn.equals("tom")){
            mRightAvatarView.setImageURI(Uri.parse("res://com.gamepind.pvp/" + R.mipmap.tom));
        }

        if (!mIsWin) {
            mLeftScoreView.setText("0");
            mRightScoreView.setText("1");

            mImageView.setImageResource(R.mipmap.icon_lose);
            mLinearContainer.setBackgroundColor(
                    getResources().getColor(R.color.result_container_background));
            mLeftScoreView.setBackgroundDrawable(getResources().getDrawable(R.mipmap.icon_lose_bg));
            mRightScoreView.setBackgroundDrawable(getResources().getDrawable(R.mipmap.icon_win_bg));
            mPlayagainView.setBackground(getResources().getDrawable(R.drawable.shape_btn_match));
            mAddFriendView.setText("More");
        }
        getMMOGameClientInstance();
    }
    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                return false;
//            }

            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_BACK){
//                    Toast.makeText(getActivity(), "按了返回键", Toast.LENGTH_SHORT).show();
                    // 離開room
                    theClient.leaveRoom(roomId);
                    return true;
                }
                return false;
            }
        });
    }
    @OnClick(R.id.text_playagain)
    public void playAgain() {
        isAagin = true;
        if (mIsBackMode) {
            close();
            return;
        }
        mPlayagainView.setBackground(getResources().getDrawable(R.drawable.shape_btn_unenable));
        mDisposable = Observable.interval(1, TimeUnit.SECONDS).subscribeOn(
                Schedulers.io()).take(31).observeOn(
                AndroidSchedulers.mainThread()).subscribe(
                new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        mPlayagainView.setText("Waitting (" + String.valueOf(30 - aLong) + "s)");
                        if (aLong == 30) {
                            mIsBackMode = true;
                            mPlayagainView.setEnabled(true);
                            mPlayagainView.setText("Back");
                        }
                    }
                });
        mPlayagainView.setEnabled(false);
        onJoinTwoUserClicked();
    }

    @Override
    public void onStart(){
        super.onStart();
//        if(theClient!=null){
//            theClient.addZoneRequestListener(this);
//            theClient.addRoomRequestListener(this);
//            theClient.addNotificationListener(this);
//        }

    }

    @Override
    public void onStop(){
        super.onStop();
        if(theClient!=null) {
            theClient.removeZoneRequestListener(this);
            theClient.removeRoomRequestListener(this);
            theClient.removeNotificationListener(this);
        }

    }

    @Override
    protected void initView() {
        super.initView();

    }

    @OnClick(R.id.text_opponent)
    public void anthorOpponent() {
        // TODO: 2019/1/24  add by 张玉保  其他选择
//        HashMap<String, Object> table = new HashMap<String, Object>();
//        table.put("twoUser", true);
//        theClient.joinRoomWithProperties(table);
        Log.e("Result","joinRoomWithProperties-----anthorOpponent");
        // 点击去除
        if(!isNew){//去重处理
            isNew = true;
            onJoinTwoUserClicked();
        }else{
            Toast.makeText(getContext(), "Matching", Toast.LENGTH_SHORT).show();

        }

    }

    @OnClick(R.id.text_add_friend)
    public void addFriends() {
        if (mIsMore) {
            Intent intent = new Intent(getActivity(), GameChatActivity.class);
            intent.putExtra("user", "");
            startActivity(intent);
        } else {
            addFriend(Friend.getInstance().getId(), "123");
        }
    }


    @OnClick(R.id.image_back)
    public void close() {
        getActivity().finish();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }


    public void addFriend(String userId, String friendId) {
        RemoteRepository
                .getInstance().addFriend(userId, friendId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResultInfo>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addDisposable(d);
                    }

                    @Override
                    public void onSuccess(ResultInfo value) {
                        mAddFriendView.setText("More");
                        mIsMore = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Log", e.getMessage());
                        ToastUtils.show("**********");
                    }
                });
    }
    @Override
    public void onCreateRoomDone(final RoomEvent event, String desc) {
        Log.e("Result","onCreateRoomDone-----");

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(event.getResult()== MMOGameResponseResultCode.SUCCESS){// if room created successfully
                    theClient.joinRoom(event.getData().getId());
                }else{
                    Utils.showToastAlert(getActivity(), Constants.ALERT_ROOM_CREATE + event.getResult());
                }
            }
        });
    }

    @Override
    public void onDeleteRoomDone(RoomEvent event, String desc) {

    }
    @Override
    public void onGetAllRoomsDone(AllRoomsEvent event) {

    }
    @Override
    public void onGetLiveUserInfoDone(LiveUserInfoEvent event) {

    }
    @Override
    public void onGetMatchedRoomsDone(final MatchedRoomsEvent event){

    }
    @Override
    public void onGetOnlineUsersDone(AllUsersEvent arg0) {

    }
    @Override
    public void onSetCustomUserDataDone(LiveUserInfoEvent arg0) {

    }

    public void startGame(String roomId, int maxUsers){
//		Intent intent = new Intent(getActivity(), GameActivity.class);
        Intent intent = new Intent(getActivity(), MatchPendingActivity.class);
        intent.putExtra("roomId", roomId);
        intent.putExtra("gameType", maxUsers);
        intent.putExtra("nextTurn", nextTurn);
        //TODO 游戏信息
        GameInfo gameInfo = new GameInfo();
        gameInfo.setGameImgUrl("res://com.gamepind.pvp/" + R.mipmap.game_cover);
        gameInfo.setGameName("Angry Bird2");
        intent.putExtra(Constant.GAME, gameInfo);
        startActivity(intent);
        getActivity().finish();
    }
    public void onJoinTwoUserClicked(){
        HashMap<String, Object> table = new HashMap<String, Object>();
        table.put("twoUser", true);
        theClient.joinRoomWithProperties(table);
        Log.e("Result","joinRoomWithProperties-----");
    }
    @Override
    public void onRPCDone(byte arg0, String arg1, Object arg2) {

    }

    @Override
    public void onGetLiveRoomInfoDone(LiveRoomInfoEvent arg0) {

    }

    @Override
    public void onJoinRoomDone(final RoomEvent event, final String desc) {
        Log.e("Result","onJoinRoomDone-----anthorOpponent"+event.getResult());

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
                    roomId = event.getData().getId();
                    theClient.subscribeRoom(event.getData().getId());
                }else{
                    HashMap<String, Object> table = new HashMap<String, Object>();
//                    if(roomSize==2){
                    table.put("twoUser", true);
//                    }else if(roomSize==3){
//                        table.put("threeUser", true);
//                    }
                    //TODO
                    theClient.createTurnRoom(String.valueOf(System.currentTimeMillis()), "owner", 2, table, Constants.TURN_TIME);
                }
            }
        });
    }

    @Override
    public void onLeaveRoomDone(RoomEvent arg0) {

    }

    @Override
    public void onLockPropertiesDone(byte arg0) {

    }

    @Override
    public void onSetCustomRoomDataDone(LiveRoomInfoEvent arg0) {

    }

    @Override
    public void onSubscribeRoomDone(RoomEvent event) {
        if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
            //发送进入 某个房间
            //TODO 用户名
            if(isAagin) {
                Log.e("Result","sendPrivateChat-----");
                theClient.sendPrivateChat(nextTurn,Utils.userName);
            }
            if(isNew){// 發出邀請
                Log.e("Result","onSubscribeRoomDone-----");
                //TODO
                startGame(event.getData().getId(), event.getData().getMaxUsers());
            }

        }else{
//            Utils.showToastAlertOnUIThread(this, "onSubscribeRoomDone Failed"+event.getResult());
        }

    }

    @Override
    public void onUnSubscribeRoomDone(RoomEvent arg0) {

    }

    @Override
    public void onUnlockPropertiesDone(byte arg0) {

    }

    @Override
    public void onUpdatePropertyDone(LiveRoomInfoEvent arg0, String arg1) {

    }

    @Override
    public void onRoomCreated(RoomData paramRoomData) {

    }

    @Override
    public void onRoomDestroyed(RoomData paramRoomData) {

    }

    @Override
    public void onUserLeftRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserJoinedRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserLeftLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onUserJoinedLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onChatReceived(ChatEvent paramChatEvent) {

    }


    @Override
    public void onUpdatePeersReceived(UpdateEvent paramUpdateEvent) {

    }

    @Override
    public void onUserChangeRoomProperty(RoomData paramRoomData, String paramString, HashMap<String, Object> paramHashMap, HashMap<String, String> paramHashMap1) {

    }

    @Override
    public void onMoveCompleted(MoveEvent paramMoveEvent) {

    }

    boolean isJump = false;
    @Override
    public void onGameStarted(String sender, String roomId, final String nextTurn) {
        Log.d("onGameStarted: ", "sender: "+sender+" roomId: "+roomId+" nextTurn: "+nextTurn);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!isJump){
                    Intent intent = new Intent(getActivity(), GameActivity.class);
                    intent.putExtra("roomId", roomId);
                    intent.putExtra("nextTurn", nextTurn);
                    startActivity(intent);
                    isJump = true;
                    getActivity().finish();
                }
            }
        });
    }
    boolean isShow = false;
    @Override
    public void onPrivateChatReceived(String paramString1, String userName) {
        //获得信息
        Log.e("onPrivateChatReceived","sendPrivateChat-----"+paramString1+"rid-----"+userName);
        if("no".equals(userName)){//拒絕邏輯
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mIsBackMode = true;
                    mPlayagainView.setEnabled(true);
                    mPlayagainView.setText("Back");
                }
            });

        }else if(!userName.equals(Utils.userName)){//邀请人等于自己的时候
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!isShow) {
                        isShow = true;
                        Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
                        View view = View.inflate(getActivity(), R.layout.dialog_friend_invite, null);
                        dialog.setContentView(view);
                        Window window = dialog.getWindow();
                        window.setGravity(Gravity.TOP);
                        //设置弹出动画
//                        window.setWindowAnimations(R.style.main_menu_animStyle);
//                        设置对话框大小
                        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog.findViewById(R.id.accpet).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                HashMap<String, Object> table = new HashMap<String, Object>();
                                table.put("twoUser", true);
                                theClient.joinRoomWithProperties(table);
                                isShow = false;
//                                isAagin = true;
                                dialog.dismiss();
                            }
                        });
                        dialog.findViewById(R.id.later).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                theClient.sendPrivateChat(userName, "no");
                                isShow = false;
                                dialog.dismiss();
                            }
                        });
//                    ((TextView) dialog.findViewById(R.id.tv_user)).setText(userName);
                        ((TextView) dialog.findViewById(R.id.tv_user)).setText(userName+"\n"+"Invite you");
                        dialog.show();
                    }
                }
            });

        }


    }
    @Override
    public void onGameStopped(String paramString1, String paramString2) {

    }

    @Override
    public void onUserPaused(String paramString1, boolean paramBoolean, String paramString2) {

    }

    @Override
    public void onUserResumed(String paramString1, boolean paramBoolean, String paramString2) {

    }

}
