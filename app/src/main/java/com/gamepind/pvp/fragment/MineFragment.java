package com.gamepind.pvp.fragment;


import android.app.Activity;
import android.net.Uri;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.ui.base.BaseFragment;
import com.gamepind.pvp.utils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author 张玉保 2019/1/22.
 *
 * 描述：我的页面
 */
public class MineFragment extends BaseFragment {

    @BindView(R.id.text_name)
    TextView mUsernameTextView;

    @BindView(R.id.image_avatar)
    SimpleDraweeView mUserAvatarView;

    @Override
    protected int getContentId() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initData() {
        Friend friend = Friend.getInstance();
        mUsernameTextView.setText(friend.getUserName());

        mUserAvatarView.setImageURI(Uri.parse(friend.getImageId()));
    }


    @OnClick(R.id.text_logout)
    public void logout() {
        mLogoutListener.onLogout();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof onLogoutListener) {
            mLogoutListener = (onLogoutListener)activity;
        } else{
            throw new IllegalArgumentException("activity must implements FragmentInteraction");
        }
    }

    private onLogoutListener mLogoutListener;

    public interface onLogoutListener{
        void onLogout();
    }
}
