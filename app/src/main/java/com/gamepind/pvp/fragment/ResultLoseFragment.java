package com.gamepind.pvp.fragment;

import com.gamepind.pvp.R;
import com.gamepind.pvp.ui.base.BaseFragment;

/**
 * @author Roybal 2019/1/24.
 *
 * 描述：失败结果
 */
public class ResultLoseFragment extends BaseFragment{

    @Override
    protected int getContentId() {
        return R.layout.fragment_lose_layout;
    }

    @Override
    protected void initData() {

    }
}
