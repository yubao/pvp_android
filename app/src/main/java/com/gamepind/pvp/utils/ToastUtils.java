package com.gamepind.pvp.utils;

import android.widget.Toast;

import com.gamepind.pvp.PVPApplication;


/**
 * Created by newbiechen on 17-5-11.
 */

public class ToastUtils {

    public static void show(String msg){
        Toast.makeText(PVPApplication.getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
