package com.gamepind.pvp.utils;

import android.text.TextUtils;

import com.gamepind.pvp.model.remote.Constant;


/**
 * @author Roybal 2019/1/24.
 *
 * 描述：
 */
public class LoginHelper {

    public static boolean isLogin() {
        String json = SharedPreUtils.getInstance().getString(Constant.MEMBER);
        return !TextUtils.isEmpty(json);
    }

}
