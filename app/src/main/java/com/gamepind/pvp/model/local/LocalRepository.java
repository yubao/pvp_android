//package com.gamepind.pvp.model.local;
//
//import com.gamepind.pvp.model.bean.AuthorBean;
//import com.gamepind.pvp.model.bean.BillboardBean;
//import com.gamepind.pvp.model.bean.BillboardPackage;
//import com.gamepind.pvp.model.gen.DaoSession;
//import com.gamepind.pvp.model.remote.Constant;
//import com.gamepind.pvp.utils.SharedPreUtils;
//import com.google.gson.Gson;
//
//import java.util.List;
//
//
///**
// * Created by sunny on 17-4-26.
// */
//
//public class LocalRepository implements GetDbHelper {
//    private static final String TAG = "LocalRepository";
//    private static final String DISTILLATE_ALL = "normal";
//    private static final String DISTILLATE_BOUTIQUES = "distillate";
//
//    private static volatile LocalRepository sInstance;
//    private DaoSession mSession;
//
//    private LocalRepository() {
//        mSession = DaoDbHelper.getInstance().getSession();
//    }
//
//    public static LocalRepository getInstance() {
//        if (sInstance == null) {
//            synchronized (LocalRepository.class) {
//                if (sInstance == null) {
//                    sInstance = new LocalRepository();
//                }
//            }
//        }
//        return sInstance;
//    }
//
////    @Override
////    public BillboardPackage getBillboardPackage() {
////
////        return null;
////    }
//     public void saveBillboardPackage(BillboardPackage bean) {
//        String json = new Gson().toJson(bean);
//        SharedPreUtils.getInstance()
//                .putString(Constant.SHARED_SAVE_BILLBOARD,json);
//    }
//
//    /*************************************数据存储*******************************************/
//    /**
//     * 存储BookComment
//     *
//     * @param
//     */
//    @Override
//    public BillboardPackage getBillboardPackage() {
//        //TODO
//        String json = SharedPreUtils.getInstance()
//                .getString(Constant.SHARED_SAVE_BILLBOARD);
//        if (json == null){
//            return null;
//        }
//        else {
//            return new Gson().fromJson(json,BillboardPackage.class);
//        }
//    }
//    public void saveBillboardBean(List<BillboardBean> beans){
//        mSession.getBillboardBeanDao()
//                .insertOrReplaceInTx(beans);
//    }
//    public void saveAuthors(List<AuthorBean> beans){
//        mSession.getAuthorBeanDao()
//                .insertOrReplaceInTx(beans);
//    }
//}
