package com.gamepind.pvp.model.bean;

import java.io.Serializable;

/**
 * @author Roybal 2019/1/23.
 *
 * 描述：
 */
public class User implements Serializable {
    private String userName;
    private String avatar;
    private long userId;
    private String dateTime;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
