package com.gamepind.pvp.model.remote;




import com.gamepind.pvp.model.bean.FriendList;
import com.gamepind.pvp.model.bean.InviteList;
import com.gamepind.pvp.model.bean.LoginResultInfo;
import com.gamepind.pvp.model.bean.ResultInfo;
import com.gamepind.pvp.model.bean.UserInfo;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by sunny on 17-4-20.
 */

public interface BookApi {
    /**
     * 推荐书籍
     * @return
     */
//    @GET("/ranking/gender")
//    Single<BillboardPackage> getBillboardPackage();

    @GET("/api/Account/UserInfo")
//    grant_type:password
//    userName:aa@aa.com
//    password:Aaa_111
//    @POST("/api/Account/UserInfo?grant_type=password&userName=aa@aa.com&password=Aaa_111")
//    @POST("/token?grant_type=password&userName=aa@aa.com&password=Aaa_111")
//    getTest(@Field("grant_type") String type,
//            @Field("userName") String userName,
//            @Field("password") String psw
//    );
//String userid,String password
    Single<UserInfo> getUserInfo();
    @FormUrlEncoded
    @POST("/core/login")
    Single<LoginResultInfo> login(@Field("username")String userId,
                                    @Field("password")String password );
    @FormUrlEncoded
    @POST("/core/addFriend")
    Single<ResultInfo> addFriend(@Field("userId")String userId,
                                    @Field("friendId")String friendId );

    @GET("/core/findFriends")
    Single<FriendList> findFriends(@Query("userId")String userId );


    @FormUrlEncoded
    @POST("/core/addInviteFriendHistory")
    //String userId,String friendId,String inviteResult,String active
    Single<ResultInfo> addInviteFriendHistory(@Field("userId")String userId,
                                              @Field("friendId")String friendId,
                                              @Field("inviteResult")String inviteResult,
                                              @Field("active")String active );

    @GET("/core/getInviteFriendHistory")
    Single<InviteList> getInviteFriendHistory(@Query("userId")String userId,
                                              @Query("friendId")String friendId);
//    @GET("/book/recommend")
//    Single<RecommendBookPackage> getRecommendBookPackage(@Query("gender") String gender);

}
