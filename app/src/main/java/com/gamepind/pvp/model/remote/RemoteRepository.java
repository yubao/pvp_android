package com.gamepind.pvp.model.remote;



//import com.gamepind.pvp.model.bean.BillboardPackage;
import com.gamepind.pvp.model.bean.FriendList;
import com.gamepind.pvp.model.bean.InviteList;
import com.gamepind.pvp.model.bean.LoginResultInfo;
import com.gamepind.pvp.model.bean.ResultInfo;
import com.gamepind.pvp.model.bean.UserInfo;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.http.Field;
import retrofit2.http.Query;

/**
 * Created by  sunny on 17-4-20.
 */

public class RemoteRepository {
    private static final String TAG = "RemoteRepository";

    private static RemoteRepository sInstance;
    private Retrofit mRetrofit;
    private BookApi mBookApi;

    private RemoteRepository(){
        mRetrofit = RemoteHelper.getInstance()
                .getRetrofit();

        mBookApi = mRetrofit.create(BookApi.class);
    }

    public static RemoteRepository getInstance(){
        if (sInstance == null){
            synchronized (RemoteHelper.class){
                if (sInstance == null){
                    sInstance = new RemoteRepository();
                }
            }
        }
        return sInstance;
    }
    /**
     * 排行榜的类型
     * @return
     */
//    public Single<BillboardPackage> getBillboardPackage(){
//        return mBookApi.getBillboardPackage();
//    }

    /**
     *
     */
    public Single<UserInfo> getUserInfo(){
         return mBookApi.getUserInfo();
     }
    public Single<LoginResultInfo> login(String userId,String pwd){
         return mBookApi.login(userId,pwd);
    }
    public Single<ResultInfo> addFriend(String userId, String friendId ){
        return  mBookApi.addFriend(userId,friendId);
    }
    public  Single<FriendList> findFriends(String userId ){
        return mBookApi.findFriends(userId);
    }
    public Single<InviteList> getInviteFriendHistory(String userId, String friendId){
        return mBookApi.getInviteFriendHistory(userId,friendId);
    }

    //    public Single<List<CollBookBean>> getRecommendBooks(String gender){
//        return mBookApi.getRecommendBookPackage(gender)
//                .map(bean -> bean.getBooks());
//    }


}
