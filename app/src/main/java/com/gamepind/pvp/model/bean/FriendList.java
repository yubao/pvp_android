package com.gamepind.pvp.model.bean;

import java.util.List;

/**
 * Created by Administrator on 2019/1/23/023.
 */

public class FriendList {

//    "responseCode": "200",
//            "responseMessage": [
//    {
//        "id": "3",
//            "userName": "lili",
//            "imageId": "test"
//    },
//    {
//        "id": "4",
//            "userName": "meimei",
//            "imageId": "test"
//    }
//    ]
    String responseCode ;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public List<Friend> getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(List<Friend> responseMessage) {
        this.responseMessage = responseMessage;
    }

    List<Friend> responseMessage ;

}
