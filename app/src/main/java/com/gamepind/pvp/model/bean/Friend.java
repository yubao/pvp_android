package com.gamepind.pvp.model.bean;


import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.utils.SharedPreUtils;
import com.google.gson.Gson;

import java.io.Serializable;

public class Friend implements Serializable{


    /**
     * id : 1
     * userName : Leo
     * imageId : test
     * online : 0
     * lastTime : 2019-01-24 19:22:22
     */

    private String id;
    private String userName;
    private String imageId;
    private String online;
    private String lastTime;
    private String invited;

    public String getInvited() {
        return invited;
    }

    public void setInvited(String invited) {
        this.invited = invited;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public static Friend getInstance(){
        String json = SharedPreUtils.getInstance().getString(Constant.MEMBER);
        return new Gson().fromJson(json,Friend.class);
    }

}
