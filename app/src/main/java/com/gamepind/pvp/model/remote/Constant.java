package com.gamepind.pvp.model.remote;

/**
 * Description :
 *
 * @auther lelearning.com
 * Created by lxlj
 * on 2018/3/29.
 */

public class Constant {
    /*URL_BASE*/
//    public static final String API_BASE_URL = "http://192.168.0.12:19931";
//    public static final String API_BASE_URL = "http://30.26.231.90:8443";
//    public static final String API_BASE_URL = "http://192.168.43.5:8443";
//    public static final String API_BASE_URL = "http://172.20.10.2:8443";
    public static final String API_BASE_URL = "http://10.10.33.23:8443";

//    public static String HOST_NAME =  "10.10.33.23";

//    public static String HOST_NAME =  "172.20.10.2";


//        public static final String API_BASE_URL = "http://api.zhuishushenqi.com";
    public static final String IMG_BASE_URL = "http://statics.zhuishushenqi.com";

    public static final String AU_USER_INFO ="Account/UserInfo";
    public static final String SHARED_SAVE_BILLBOARD = "billboard";


    public static final String MEMBER = "member";
    public static final String FRIEND = "friend";
    public static final String GAME = "game";
}
