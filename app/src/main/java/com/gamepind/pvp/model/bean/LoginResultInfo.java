package com.gamepind.pvp.model.bean;

/**
 * Description :
 *
 * @auther lelearning.com
 * Created by lxlj
 * on 2018/3/30.
 */

public class LoginResultInfo {

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    String responseCode ;

    public Friend getFriend() {
        return responseMessage;
    }

    public void setFriend(Friend friend) {
        this.responseMessage = friend;
    }

    Friend  responseMessage;
}
