package com.gamepind.pvp.model.bean;


public class Invite {

//"userId": "a",
//        "friendId": "b",
//        "inviteResult": "c",
//        "active": "1"
//    gameImgUrl


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getInviteResult() {
        return inviteResult;
    }

    public void setInviteResult(String inviteResult) {
        this.inviteResult = inviteResult;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getGameImgUrl() {
        return gameImgUrl;
    }

    public void setGameImgUrl(String gameImgUrl) {
        this.gameImgUrl = gameImgUrl;
    }
    String userId;
    String friendId;
    String inviteResult;
    String active;
    String gameImgUrl;

}
