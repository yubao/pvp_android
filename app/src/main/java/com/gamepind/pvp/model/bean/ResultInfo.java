package com.gamepind.pvp.model.bean;

/**
 * Description :
 *
 * @auther lelearning.com
 * Created by lxlj
 * on 2018/3/30.
 */

public class ResultInfo {

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    String responseCode ;
    String responseMessage ;
}
