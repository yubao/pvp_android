package com.gamepind.pvp.model.bean;

import java.util.List;

/**
 * Created by Administrator on 2019/1/23/023.
 */

public class InviteList  {
    String responseCode ;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public List<GameInfo> getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(List<GameInfo> responseMessage) {
        this.responseMessage = responseMessage;
    }

    List<GameInfo> responseMessage ;
}
