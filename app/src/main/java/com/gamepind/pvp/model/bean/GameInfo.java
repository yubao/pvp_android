package com.gamepind.pvp.model.bean;

import java.io.Serializable;

/**
 * @author Roybal 2019/1/24.
 *
 * 描述：游戏
 */
public class GameInfo implements Serializable {
    private String userId;
    private String friendId;
    private int inviteResult;
    private int active;
    private String gameImgUrl;
    private String gameId;
    private String gameName;
    private String invitedate;
    private int isDownload;

    public int getIsDownload() {
        return isDownload;
    }

    public void setIsDownload(int isDownload) {
        this.isDownload = isDownload;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public int getInviteResult() {
        return inviteResult;
    }

    public void setInviteResult(int inviteResult) {
        this.inviteResult = inviteResult;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getGameImgUrl() {
        return gameImgUrl;
    }

    public void setGameImgUrl(String gameImgUrl) {
        this.gameImgUrl = gameImgUrl;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getInvitedate() {
        return invitedate;
    }

    public void setInvitedate(String invitedate) {
        this.invitedate = invitedate;
    }
}
