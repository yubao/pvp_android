package com.gamepind.pvp.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.net.Uri;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.demo.Constants;
import com.gamepind.pvp.demo.GameActivity;
import com.gamepind.pvp.demo.MappingActiity;
import com.gamepind.pvp.demo.Utils;
import com.gamepind.pvp.ui.base.BaseActivity;
import com.gamepind.pvp.utils.DPUtils;
import com.gamepind.pvp.utils.ToastUtils;
import com.taobao.lottery.game.multiplayer.client.MMOGameClient;
import com.taobao.lottery.game.multiplayer.client.command.MMOGameResponseResultCode;
import com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent;
import com.taobao.lottery.game.multiplayer.client.events.ChatEvent;
import com.taobao.lottery.game.multiplayer.client.events.ConnectEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveRoomInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveUserInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LobbyData;
import com.taobao.lottery.game.multiplayer.client.events.MatchedRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.MoveEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomData;
import com.taobao.lottery.game.multiplayer.client.events.RoomEvent;
import com.taobao.lottery.game.multiplayer.client.events.UpdateEvent;
import com.taobao.lottery.game.multiplayer.client.listener.ConnectionRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.NotifyListener;
import com.taobao.lottery.game.multiplayer.client.listener.RoomRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.ZoneRequestListener;
import com.taobao.lottery.game.multiplayer.client.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Roybal 2019/1/23.
 *
 * 描述：正在匹配...
 */
public class MatchPendingActivity extends BaseActivity implements ZoneRequestListener, RoomRequestListener,NotifyListener {

    @BindView(R.id.text_game_name)
    TextView mGameNameView;

//    @BindView(R.id.text_name_right)
//    TextView mRightName;

    @BindView(R.id.text_name_left)
    TextView mLeftName;

    @BindView(R.id.image_user_avatar)
    SimpleDraweeView mUserAvatarView;

    @BindView(R.id.btn_follow)
    LottieAnimationView mBackView;

    @BindView(R.id.text_match_time)
    TextView mMatchTimeView;

    @BindView(R.id.matching_hint)
    TextView mMatchingView;

    @BindView(R.id.image_avatar_left)
    SimpleDraweeView mAvatarLeft;

    @BindView(R.id.middle_container_auto_match)
    RelativeLayout mMatchSucceedView;

    private int[] targetLocation;
    private int[] originLocation;


    private Disposable mDisposable;
    private GameInfo mGameInfo;

    private  String roomId="";
    private  String oppnent = "";
    @Override
    protected int getContentId() {
        return R.layout.activity_auto_match;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mGameInfo = (GameInfo) getIntent().getSerializableExtra(Constant.GAME);
    }

    @Override
    protected void initWidget() {
        mGameNameView.setText(mGameInfo.getGameName());
        mLeftName.setText(Friend.getInstance().getUserName());
        mUserAvatarView.setImageURI(Uri.parse(Friend.getInstance().getImageId()));
        mDisposable = Observable.interval(1000, 1000, TimeUnit.MILLISECONDS).subscribeOn(
                Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        mMatchTimeView.setText(String.valueOf(aLong + 1) + "S");
                    }
                });
        getMMOGameClientInstance();
        //TODO 注意解绑
        theClient.addNotificationListener(this);
        theClient.addZoneRequestListener(this);
        theClient.addRoomRequestListener(this);
//        theClient.addZoneRequestListener(this);
//        roomId = getIntent().getStringExtra("roomId");

        HashMap<String, Object> table = new HashMap<String, Object>();
        table.put("twoUser", true);
        theClient.joinRoomWithProperties(table);

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        originLocation = new int[2];
        targetLocation = new int[2];
        mUserAvatarView.getLocationInWindow(originLocation);
        mAvatarLeft.getLocationInWindow(targetLocation);
    }

    @OnClick(R.id.image_close)
    public void close() {
        theClient.removeNotificationListener(this);
        //TODO 离开房间
        theClient.leaveRoom(roomId);
//        theClient.disconnect();
        finish();
    }

    private MMOGameClient theClient;

    private void getMMOGameClientInstance() {
        try {
            theClient = MMOGameClient.getInstance();
        } catch (Exception ex) {
            Utils.showToastAlert(this, Constants.ALERT_INIT_EXEC);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        theClient.removeNotificationListener(this);
        theClient.removeZoneRequestListener(this);
        theClient.removeRoomRequestListener(this);


        // theClient.leaveRoom(roomId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }

    @Override
    public void onRoomCreated(RoomData paramRoomData) {

    }

    @Override
    public void onRoomDestroyed(RoomData paramRoomData) {

    }

    @Override
    public void onUserLeftRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserJoinedRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserLeftLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onUserJoinedLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onChatReceived(ChatEvent event) {
        Log.d("onGameStarted: ",
                "sender:onChatReceived +LonNotify---------ChatEvent--");
        if (event.getSender().equals(Constants.SERVER_NAME)) {
            if (event.getMessage().indexOf('#') != -1) {
                int hashIndex = event.getMessage().indexOf('#');
                final String code = event.getMessage().substring(0, hashIndex);
                final String message = event.getMessage().substring(hashIndex + 1,
                        event.getMessage().length());
                try {
                    final int CODE = Integer.parseInt(code);
                    if (CODE == Constants.SUBMIT_CARD) {
                        Utils.showToastAlertOnUIThread(this, message);
                    } else if (CODE == Constants.USER_HAND) {
                        Log.e("GameActivity", "------"+message);

                        GameActivity.USER_CARD.clear();
                        JSONObject object = new JSONObject(message);
                        JSONArray cardArray = object.getJSONArray(Utils.userName);
                        for (int i = 0; i < cardArray.length(); i++) {
                            GameActivity.USER_CARD.add((Integer) cardArray.get(i));
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//								Intent intent = new Intent(MappingActiity.this, GameActivity
// .class);
//								intent.putExtra("roomId", roomId);
//								intent.putExtra("gameType", "2");
//								startActivity(intent);
//								finish();
                            }
                        });
                    }
                } catch (NumberFormatException e) {
                    Log.d("GameActivity", "onChatReceived:NumberFormatException");
                } catch (JSONException e) {
                    Log.d("GameActivity", "onChatReceived:JSONException");
                }
            }
        }
    }

    @Override
    public void onPrivateChatReceived(String paramString1, String paramString2) {
//        if (paramString2 == "no") {
//            finish();
//        }
        Log.e("onPrivateChatReceived","paramString1-----"+paramString1+"rid-----"+paramString2);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                AlertDialog.Builder builder = new AlertDialog.Builder(MatchActivity.this);
//                builder.setMessage("big邀请你玩游戏")
//                        .setCancelable(false)
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
////								theClient.joinRoomWithProperties();
//                                HashMap<String, Object> table = new HashMap<String, Object>();
//                                table.put("twoUser", true);
//                                theClient.joinRoomWithProperties(table);
////								startGame(paramString2, 2);
////                                onBackPressed();
//                            }
//                        })
//                        .setNegativeButton("拒绝", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                theClient.sendPrivateChat("bbb","no");
//                                onBackPressed();
//                            }
//                        });
//                AlertDialog alert = builder.create();
//                alert.show();
            }
        });
    }

    @Override
    public void onUpdatePeersReceived(UpdateEvent paramUpdateEvent) {

    }

    @Override
    public void onUserChangeRoomProperty(RoomData paramRoomData, String paramString,
            HashMap<String, Object> paramHashMap, HashMap<String, String> paramHashMap1) {

    }

    @Override
    public void onMoveCompleted(MoveEvent paramMoveEvent) {

    }

    boolean isJump = false;

    String sender = "";
    String nextTurnName = "";
    int index = 1;
    @Override
    public void onGameStarted(String sender, String roomIdStr, final String nextTurn) {
        Log.d("onGameStarted: ",
                "sender:LonNotify --- "+(index++)+"------"+ sender + " roomId: " + roomIdStr + " nextTurn: " + nextTurn);
//        theClient.invokeZoneRPC("getUsersByroomId",Utils.userName,roomId);
        nextTurnName = nextTurn;
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                mRightName.setText(nextTurn);
                matchingAnimation(roomIdStr, nextTurn);
            }
        });
    }

    private void matchingAnimation(String roomId, String nextTurn) {
        onMatched();
        if (mMatchSucceedView != null) {
            mMatchSucceedView.setVisibility(View.VISIBLE);
        }
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator translationX = ObjectAnimator.ofFloat(mUserAvatarView, "translationX",
                targetLocation[0], -originLocation[0] + DPUtils.dip2px(this,32));
        ObjectAnimator translationY = ObjectAnimator.ofFloat(mUserAvatarView, "translationY",
                targetLocation[1] , targetLocation[1] - DPUtils.dip2px(this,38));
        ObjectAnimator alpha = ObjectAnimator.ofFloat(mMatchSucceedView, "Alpha", 0, 1);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(mMatchSucceedView, "scaleX", 0, 1f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(mMatchSucceedView, "scaleY", 0, 1f);
        animatorSet.setDuration(2000);
        animatorSet.play(translationX).with(alpha).with(scaleX).with(scaleY).with(translationY);
        animatorSet.start();
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mUserAvatarView.setVisibility(View.INVISIBLE);
                if(!isJump){
                    Intent intent = new Intent(MatchPendingActivity.this, GameActivity.class);
                    intent.putExtra("roomId", roomId);
                    intent.putExtra("nextTurn", nextTurn);
                    intent.putExtra("oppnent",oppnent);
                    startActivity(intent);
                    isJump = true;
//                    theClient.removeZoneRequestListener(MatchPendingActivity.this);

                    finish();
                }
            }
        });
    }


    private void onMatched() {
        if (mDisposable != null) {
            mDisposable.dispose();
        }
        mMatchingView.setText("Matched");
    }

    @Override
    public void onGameStopped(String paramString1, String paramString2) {

    }

    @Override
    public void onUserPaused(String paramString1, boolean paramBoolean, String paramString2) {

    }

    @Override
    public void onUserResumed(String paramString1, boolean paramBoolean, String paramString2) {

    }

    @Override
    public void onSubscribeRoomDone(RoomEvent event) {
        if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
            //发送进入 某个房间
            //TODO 发邀请
            // theClient.sendPrivateChat("aaa",event.getData().getId());
            Log.e("onPrivateChatReceived","paramString1Match-----"+event.getData().getId());
//            startGame(event.getData().getId(), event.getData().getMaxUsers());
            roomId = event.getData().getId();
        }else{
            Utils.showToastAlertOnUIThread(this, "onSubscribeRoomDone Failed"+event.getResult());
        }
    }


    @Override
    public void onUnSubscribeRoomDone(RoomEvent paramRoomEvent) {

    }

    @Override
    public void onJoinRoomDone(RoomEvent event, String paramString) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
                    theClient.subscribeRoom(event.getData().getId());
                }else{
                    HashMap<String, Object> table = new HashMap<String, Object>();
//                    if(roomSize==2){
                    table.put("twoUser", true);
//                    }else if(roomSize==3){
//                        table.put("threeUser", true);
//                    }
                    //TODO
                    theClient.createTurnRoom(String.valueOf(System.currentTimeMillis()), "owner", 2, table, Constants.TURN_TIME);
                }
            }
        });
    }

    @Override
    public void onLeaveRoomDone(RoomEvent paramRoomEvent) {

    }

    @Override
    public void onGetLiveRoomInfoDone(LiveRoomInfoEvent paramLiveRoomInfoEvent) {

    }

    @Override
    public void onSetCustomRoomDataDone(LiveRoomInfoEvent paramLiveRoomInfoEvent) {

    }

    @Override
    public void onUpdatePropertyDone(LiveRoomInfoEvent paramLiveRoomInfoEvent, String paramString) {

    }

    @Override
    public void onLockPropertiesDone(byte paramByte) {

    }

    @Override
    public void onUnlockPropertiesDone(byte paramByte) {

    }

    @Override
    public void onDeleteRoomDone(RoomEvent paramRoomEvent, String paramString) {

    }

    @Override
    public void onGetAllRoomsDone(AllRoomsEvent paramAllRoomsEvent) {

    }

    @Override
    public void onCreateRoomDone(RoomEvent event, String paramString) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() == MMOGameResponseResultCode.SUCCESS) {// if room created successfully
                    theClient.joinRoom(event.getData().getId());
                } else {
                    Utils.showToastAlert(MatchPendingActivity.this, Constants.ALERT_ROOM_CREATE + event.getResult());
                }
            }
        });
    }

    @Override
    public void onGetOnlineUsersDone(AllUsersEvent paramAllUsersEvent) {

    }

    @Override
    public void onGetLiveUserInfoDone(LiveUserInfoEvent paramLiveUserInfoEvent) {

    }

    @Override
    public void onSetCustomUserDataDone(LiveUserInfoEvent paramLiveUserInfoEvent) {

    }

    @Override
    public void onGetMatchedRoomsDone(MatchedRoomsEvent paramMatchedRoomsEvent) {

    }

    @Override
    public void onRPCDone(byte paramByte, String paramString, Object paramObject) {

    }

//    @Override
//    public void onDeleteRoomDone(RoomEvent paramRoomEvent, String paramString) {
//
//    }
//
//    @Override
//    public void onGetAllRoomsDone(AllRoomsEvent paramAllRoomsEvent) {
//
//    }
//
//    @Override
//    public void onCreateRoomDone(RoomEvent paramRoomEvent, String paramString) {
//
//    }
//
//    @Override
//    public void onGetOnlineUsersDone(AllUsersEvent paramAllUsersEvent) {
//
//    }
//
//    @Override
//    public void onGetLiveUserInfoDone(LiveUserInfoEvent paramLiveUserInfoEvent) {
//
//    }
//
//    @Override
//    public void onSetCustomUserDataDone(LiveUserInfoEvent paramLiveUserInfoEvent) {
//
//    }
//
//    @Override
//    public void onGetMatchedRoomsDone(MatchedRoomsEvent paramMatchedRoomsEvent) {
//
//    }
//
////    @Override
////    public void onRPCDone(byte paramByte, String paramString, Object paramObject) {
////
////    }
//
//    @Override
//    public void onRPCDone(byte result, String function, Object returnValue) {
//        Log.e("onRPCDone---","----------------");
//
//        if(result== MMOGameResponseResultCode.SUCCESS){
//            String res = (String)returnValue;
//            Log.e("onRPCDone---",res+"--------");
//            oppnent = res;
//                    try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                matchingAnimation(roomId, nextTurnName);
//            }
//        });
//        }else{
//            Utils.showToastAlertOnUIThread(this, "RPC Request Failed");
//        }
//    }
}