package com.gamepind.pvp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.demo.Constants;
import com.gamepind.pvp.demo.Utils;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.ui.base.BaseActivity;
import com.gamepind.pvp.utils.LoginHelper;
import com.gamepind.pvp.utils.ToastUtils;
import com.gamepind.pvp.widget.CustomToolBar;
import com.taobao.lottery.game.multiplayer.client.MMOGameClient;
import com.taobao.lottery.game.multiplayer.client.command.MMOGameResponseResultCode;
import com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent;
import com.taobao.lottery.game.multiplayer.client.events.ChatEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveRoomInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveUserInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LobbyData;
import com.taobao.lottery.game.multiplayer.client.events.MatchedRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.MoveEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomData;
import com.taobao.lottery.game.multiplayer.client.events.RoomEvent;
import com.taobao.lottery.game.multiplayer.client.events.UpdateEvent;
import com.taobao.lottery.game.multiplayer.client.listener.NotifyListener;
import com.taobao.lottery.game.multiplayer.client.listener.RoomRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.ZoneRequestListener;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author 张玉保 2019/1/22.
 * <p>
 * 描述：匹配界面
 */
public class MatchActivity extends BaseActivity implements ZoneRequestListener, RoomRequestListener, NotifyListener {


    @BindView(R.id.text_auto_match)
    TextView mAutoMatchView;

    @BindView(R.id.text_auto_friends)
    TextView mAutoFriendsView;

    @BindView(R.id.cust_bar)
    CustomToolBar mToolBar;

    @BindView(R.id.game_cover)
    SimpleDraweeView mGameCover;

    private GameInfo mInfo;

//    private MMOGameClient theClient;


    @Override
    protected int getContentId() {
        return R.layout.activity_match_layout;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mInfo = (GameInfo) getIntent().getSerializableExtra(Constant.GAME);
        getMMOGameClientInstance();
    }

    private void getMMOGameClientInstance() {
//        try {
//            theClient = MMOGameClient.getInstance();
//        } catch (Exception ex) {
//            Utils.showToastAlert(this, Constants.ALERT_INIT_EXEC);
//        }
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        mToolBar.setText(mInfo.getGameName());
        mGameCover.setImageURI(Uri.parse(mInfo.getGameImgUrl()));
    }

    boolean isMatch = false;

    @OnClick(R.id.text_auto_match)
    public void match() {
        if (!LoginHelper.isLogin()) {
            startActivity(LoginActivity.class);
            return;
        }
        Log.e("onPrivateChatReceived", "paramString1-----");
        if (!isMatch) {

            Intent intent = new Intent(this, MatchPendingActivity.class);
            intent.putExtra(Constant.GAME, mInfo);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Matching", Toast.LENGTH_SHORT).show();

        }

//        Intent intent = new Intent(this, MatchPendingActivity.class);
//        intent.putExtra(Constant.GAME, mInfo);
//        startActivity(intent);
    }

    @OnClick(R.id.text_auto_friends)
    public void matchFriends() {
        if (!LoginHelper.isLogin()) {
            startActivity(LoginActivity.class);
            return;
        }
        Intent intent = new Intent(this, FriendshipActivity.class);
        intent.putExtra(Constant.GAME, mInfo);
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
//        if(theClient!=null){
//            theClient.addZoneRequestListener(this);
//            theClient.addRoomRequestListener(this);
//            theClient.addNotificationListener(this);
//        }

    }

    @Override
    public void onStop() {
        super.onStop();
//        if(theClient!=null) {
//            theClient.removeZoneRequestListener(this);
//            theClient.removeRoomRequestListener(this);
//            theClient.removeNotificationListener(this);
//        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        theClient.disconnect();
    }


    @Override
    public void onCreateRoomDone(final RoomEvent event, String desc) {
        Log.e("MatchActivity", "onCreateRoomDone-----");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() == MMOGameResponseResultCode.SUCCESS) {// if room created successfully
//                    theClient.joinRoom(event.getData().getId());
                } else {
                    Utils.showToastAlert(MatchActivity.this, Constants.ALERT_ROOM_CREATE + event.getResult());
                }
            }
        });
    }

    @Override
    public void onDeleteRoomDone(RoomEvent event, String desc) {

    }

    @Override
    public void onGetAllRoomsDone(AllRoomsEvent event) {

    }

    @Override
    public void onGetLiveUserInfoDone(LiveUserInfoEvent event) {

    }

    @Override
    public void onGetMatchedRoomsDone(final MatchedRoomsEvent event) {

    }

    @Override
    public void onGetOnlineUsersDone(AllUsersEvent arg0) {

    }

    @Override
    public void onSetCustomUserDataDone(LiveUserInfoEvent arg0) {

    }

    public void startGame(String roomId, int maxUsers) {
//		Intent intent = new Intent(this, GameActivity.class);
        Intent intent = new Intent(this, MatchPendingActivity.class);
//        intent.putExtra("roomId", roomId);
//        intent.putExtra("gameType", maxUsers);
        intent.putExtra(Constant.GAME, mInfo);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRPCDone(byte arg0, String arg1, Object arg2) {

    }

    @Override
    public void onGetLiveRoomInfoDone(LiveRoomInfoEvent arg0) {

    }

    @Override
    public void onJoinRoomDone(final RoomEvent event, final String desc) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() == MMOGameResponseResultCode.SUCCESS) {
//                    theClient.subscribeRoom(event.getData().getId());
                } else {
                    HashMap<String, Object> table = new HashMap<String, Object>();
//                    if(roomSize==2){
                    table.put("twoUser", true);
//                    }else if(roomSize==3){
//                        table.put("threeUser", true);
//                    }
                    //TODO
//                    theClient.createTurnRoom(String.valueOf(System.currentTimeMillis()), "owner", 2, table, Constants.TURN_TIME);
                }
            }
        });
    }

    @Override
    public void onLeaveRoomDone(RoomEvent arg0) {

    }

    @Override
    public void onLockPropertiesDone(byte arg0) {

    }

    @Override
    public void onSetCustomRoomDataDone(LiveRoomInfoEvent arg0) {

    }

    @Override
    public void onSubscribeRoomDone(RoomEvent event) {
        if (event.getResult() == MMOGameResponseResultCode.SUCCESS) {
            //发送进入 某个房间
            //TODO 发邀请
            // theClient.sendPrivateChat("aaa",event.getData().getId());
            Log.e("onPrivateChatReceived", "paramString1Match-----" + event.getData().getId());
            startGame(event.getData().getId(), event.getData().getMaxUsers());
        } else {
            Utils.showToastAlertOnUIThread(this, "onSubscribeRoomDone Failed" + event.getResult());
        }

    }

    @Override
    public void onUnSubscribeRoomDone(RoomEvent arg0) {

    }

    @Override
    public void onUnlockPropertiesDone(byte arg0) {

    }

    @Override
    public void onUpdatePropertyDone(LiveRoomInfoEvent arg0, String arg1) {

    }

    @Override
    public void onRoomCreated(RoomData paramRoomData) {

    }

    @Override
    public void onRoomDestroyed(RoomData paramRoomData) {

    }

    @Override
    public void onUserLeftRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserJoinedRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserLeftLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onUserJoinedLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onChatReceived(ChatEvent paramChatEvent) {

    }

    @Override
    public void onPrivateChatReceived(String paramString1, String paramString2) {
        //获得信息
        Log.e("onPrivateChatReceived", "paramString1-----" + paramString1 + "rid-----" + paramString2);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                AlertDialog.Builder builder = new AlertDialog.Builder(MatchActivity.this);
//                builder.setMessage("big邀请你玩游戏")
//                        .setCancelable(false)
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
////								theClient.joinRoomWithProperties();
//                                HashMap<String, Object> table = new HashMap<String, Object>();
//                                table.put("twoUser", true);
//                                theClient.joinRoomWithProperties(table);
////								startGame(paramString2, 2);
////                                onBackPressed();
//                            }
//                        })
//                        .setNegativeButton("拒绝", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                theClient.sendPrivateChat("bbb","no");
//                                onBackPressed();
//                            }
//                        });
//                AlertDialog alert = builder.create();
//                alert.show();
            }
        });


    }

    @Override
    public void onUpdatePeersReceived(UpdateEvent paramUpdateEvent) {

    }

    @Override
    public void onUserChangeRoomProperty(RoomData paramRoomData, String paramString, HashMap<String, Object> paramHashMap, HashMap<String, String> paramHashMap1) {

    }

    @Override
    public void onMoveCompleted(MoveEvent paramMoveEvent) {

    }

    @Override
    public void onGameStarted(String paramString1, String paramString2, String paramString3) {

    }

    @Override
    public void onGameStopped(String paramString1, String paramString2) {

    }

    @Override
    public void onUserPaused(String paramString1, boolean paramBoolean, String paramString2) {

    }

    @Override
    public void onUserResumed(String paramString1, boolean paramBoolean, String paramString2) {

    }

}
