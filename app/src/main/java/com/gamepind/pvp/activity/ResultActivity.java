package com.gamepind.pvp.activity;

import static com.gamepind.pvp.fragment.ResultWinFragment.FRIEND;
import static com.gamepind.pvp.fragment.ResultWinFragment.GAME;
import static com.gamepind.pvp.fragment.ResultWinFragment.WIN;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.gamepind.pvp.R;
import com.gamepind.pvp.fragment.ResultWinFragment;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.ui.base.BaseActivity;

/**
 * @author Roybal 2019/1/24.
 *
 * 描述：对战胜负结果
 */
public class ResultActivity extends BaseActivity {

    private FragmentManager mManager;
    private boolean mIsWin = false;
    private Fragment mFragment;
    private Friend mFriend;
    private GameInfo mGameInfo;
    String nextTurn;
    String opponent;

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mManager = getSupportFragmentManager();
        Intent intent = getIntent();
        nextTurn = intent.getStringExtra("nextTurn");
        opponent = intent.getStringExtra("opponent");

//        mGameInfo = (GameInfo) intent.getSerializableExtra(GAME);
        mIsWin = intent.getBooleanExtra(WIN, false);
    }

    @Override
    protected int getContentId() {
        return R.layout.activity_result_layout;
    }

    @Override
    protected void processLogic() {
        FragmentTransaction transaction = mManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putBoolean(WIN, mIsWin);
        bundle.putString("nextTurn",nextTurn);
        bundle.putString("opponent",opponent);

//        bundle.putSerializable(FRIEND, mFriend);
//        bundle.putSerializable(GAME, mGameInfo);
        mFragment = new ResultWinFragment();
        mFragment.setArguments(bundle);
        transaction.add(R.id.frame_container, mFragment);
        transaction.commit();
    }

}



