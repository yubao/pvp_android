package com.gamepind.pvp.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.gamepind.pvp.R;
import com.gamepind.pvp.adapter.GameAdapter;
import com.gamepind.pvp.adapter.GameChatAdapter;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.model.bean.InviteList;
import com.gamepind.pvp.model.bean.User;
import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.model.remote.RemoteRepository;
import com.gamepind.pvp.ui.base.BaseActivity;
import com.gamepind.pvp.utils.ToastUtils;
import com.gamepind.pvp.widget.CustomToolBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Roybal 2019/1/24.
 *
 * 描述：邀请游戏列表
 */
public class GameChatActivity extends BaseActivity {

    @BindView(R.id.cust_bar)
    CustomToolBar mToolBar;

    @BindView(R.id.recycler_game_chat)
    RecyclerView mGameChatRecyclerView;

    @BindView(R.id.recycler_game_list)
    RecyclerView mGameListRecyclerView;

    private Friend mUser;

    private GameChatAdapter mGameChatAdapter;
    private GameAdapter mGameAdapter;
    private List<GameInfo> datas = new ArrayList<>();


    @Override
    protected int getContentId() {
        return R.layout.activity_game_chat;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mUser = (Friend) getIntent().getSerializableExtra(Constant.MEMBER);
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        mToolBar.setText(mUser.getUserName());
    }

    @Override
    protected void processLogic() {
        super.processLogic();
        List<GameInfo> games = new ArrayList<>();
        mToolBar.setText(mUser.getUserName());
        mGameChatAdapter = new GameChatAdapter(this);
        mGameChatRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mGameChatAdapter.setFriend(mUser);
        mGameChatRecyclerView.setAdapter(mGameChatAdapter);
        getHistory(Friend.getInstance().getId(), mUser.getId());


        for (int i = 0; i < 6; i++) {
            GameInfo gameInfo = new GameInfo();
            if (i == 0) {
                gameInfo.setGameImgUrl("res://com.gamepind.pvp/" + R.mipmap.game_cover);
                gameInfo.setIsDownload(1);
            } else {
                gameInfo.setGameImgUrl("res://com.gamepind.pvp/" + R.mipmap.home_game1);
                gameInfo.setIsDownload(0);
            }
            gameInfo.setActive(1);
            gameInfo.setInviteResult(0);
            gameInfo.setInvitedate("09:34 2019-01-2" + i);
            gameInfo.setGameName("Brains");
            games.add(gameInfo);
        }
        //底部游戏列表
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mGameAdapter = new GameAdapter(this);
        mGameListRecyclerView.setLayoutManager(manager);
        mGameListRecyclerView.setAdapter(mGameAdapter);
        mGameAdapter.setData(games);
        mGameAdapter.setItemClickListener(new GameAdapter.OnGameItemClickListener() {
            @Override
            public void onGameClick(GameInfo gameInfo) {
                if (gameInfo.getIsDownload() == 1) {
                    gameInfo.setInviteResult(1);
                    datas.add(gameInfo);
                    mGameChatAdapter.setData(datas);
                    mGameChatRecyclerView.scrollToPosition(mGameChatAdapter.getItemCount() - 1);
                } else {
                    ToastUtils.show("downloading...");
                }
            }
        });

    }

    private void getHistory(String userId, String friendId) {
        RemoteRepository
                .getInstance().getInviteFriendHistory(userId, friendId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<InviteList>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addDisposable(d);
                    }

                    @Override
                    public void onSuccess(InviteList value) {
                        datas.addAll(value.getResponseMessage());
                        mGameChatAdapter.setData(value.getResponseMessage());
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

}
