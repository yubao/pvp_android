package com.gamepind.pvp.activity;

import android.net.Uri;
import android.view.WindowManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

import com.gamepind.pvp.R;
import com.gamepind.pvp.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Roybal.Zhang on 2019/2/22
 * <p>
 * Des:
 */
public class WebViewActivity extends BaseActivity {

    @BindView(R.id.text_input)
    EditText mInputTextView;

    @BindView(R.id.webView)
    WebView mWebview;

    @Override
    protected int getContentId() {
        return R.layout.activity_webview;
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    @OnClick(R.id.text_confirm)
    public void loadUrl() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        String url = mInputTextView.getText().toString().trim();
        mWebview.loadUrl(url);
    }
}
