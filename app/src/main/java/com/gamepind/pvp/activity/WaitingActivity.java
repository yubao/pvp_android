package com.gamepind.pvp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gamepind.pvp.R;
import com.gamepind.pvp.demo.Constants;
import com.gamepind.pvp.demo.GameActivity;
import com.gamepind.pvp.demo.Utils;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.model.bean.User;
import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.ui.base.BaseActivity;
import com.taobao.lottery.game.multiplayer.client.MMOGameClient;
import com.taobao.lottery.game.multiplayer.client.command.MMOGameResponseResultCode;
import com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent;
import com.taobao.lottery.game.multiplayer.client.events.ChatEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveRoomInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveUserInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LobbyData;
import com.taobao.lottery.game.multiplayer.client.events.MatchedRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.MoveEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomData;
import com.taobao.lottery.game.multiplayer.client.events.RoomEvent;
import com.taobao.lottery.game.multiplayer.client.events.UpdateEvent;
import com.taobao.lottery.game.multiplayer.client.listener.NotifyListener;
import com.taobao.lottery.game.multiplayer.client.listener.RoomRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.ZoneRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Roybal 2019/1/23.
 *
 * 描述：邀请等待
 */
public class WaitingActivity extends BaseActivity implements ZoneRequestListener, RoomRequestListener,NotifyListener {

    @BindView(R.id.text_game_name)
    TextView mGameNameView;

    @BindView(R.id.image_avatar_left)
    SimpleDraweeView mUserAvatarLeftView;

    @BindView(R.id.image_avatar_right)
    SimpleDraweeView mUserAvatarRightView;

    @BindView(R.id.text_match_time)
    TextView mMatchTimeView;

    @BindView(R.id.text_name_left)
    TextView mUsernameLeftView;

    @BindView(R.id.text_name_right)
    TextView mUsernameRightView;

    @BindView(R.id.text_ready)
    TextView mReadyView;

    @BindView(R.id.matching)
    TextView mMatchingView;

    private Disposable mDisposable;

    private Friend mFriend;
    private GameInfo mGameInfo;

    private MMOGameClient theClient;


    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mFriend = (Friend) getIntent().getSerializableExtra(Constant.MEMBER);
        mGameInfo = (GameInfo) getIntent().getSerializableExtra(Constant.GAME);
        getMMOGameClientInstance();
    }
    private void getMMOGameClientInstance(){
        try {
            theClient = MMOGameClient.getInstance();
        } catch (Exception ex) {
            Utils.showToastAlert(this, Constants.ALERT_INIT_EXEC);
        }
    }


    @Override
    protected int getContentId() {
        return R.layout.activity_waiting_layout;
    }

    @Override
    protected void initWidget() {
        mGameNameView.setText(mGameInfo.getGameName());
        mUserAvatarLeftView.setImageURI(Friend.getInstance().getImageId());
        mUserAvatarRightView.setImageURI(mFriend.getImageId());
        mUsernameLeftView.setText(Friend.getInstance().getUserName());
        mUsernameRightView.setText(mFriend.getUserName());
        mDisposable = Observable.interval(0, 1000, TimeUnit.MILLISECONDS).subscribeOn(
                Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        mMatchTimeView.setText(String.valueOf(aLong + 1) + "S");
                    }
                });
    }
    @Override
    protected void processLogic(){
        getMMOGameClientInstance();
        onJoinTwoUserClicked();
    }

    // TODO: 2019/1/23  add by 张玉保  匹配成功并且对方准备完毕
    private void onMatched() {
        if (mDisposable != null) {
            mDisposable.dispose();
        }
        mReadyView.setEnabled(true);
        mReadyView.setText("Ready");
        mMatchingView.setText("Matched");
    }

    @OnClick(R.id.image_close)
    public void close() {
        finish();
       // startActivity(ResultActivity.class);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }
    // 开始
    @Override
    public void onStart(){
        super.onStart();
        theClient.addZoneRequestListener(this);
        theClient.addRoomRequestListener(this);
        theClient.addNotificationListener(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        theClient.removeZoneRequestListener(this);
        theClient.removeRoomRequestListener(this);
        theClient.removeNotificationListener(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //theClient.disconnect();
    }

    public void onJoinTwoUserClicked(){
        HashMap<String, Object> table = new HashMap<String, Object>();
        table.put("twoUser", true);
        theClient.joinRoomWithProperties(table);
    }


    @Override
    public void onCreateRoomDone(final RoomEvent event, String desc) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(event.getResult()== MMOGameResponseResultCode.SUCCESS){// if room created successfully
                    theClient.joinRoom(event.getData().getId());
                }else{
                    //Utils.showToastAlert(RoomSelectionActivity.this, Constants.ALERT_ROOM_CREATE + event.getResult());
                }
            }
        });
    }

    @Override
    public void onDeleteRoomDone(RoomEvent event, String desc) {

    }
    @Override
    public void onGetAllRoomsDone(AllRoomsEvent event) {

    }
    @Override
    public void onGetLiveUserInfoDone(LiveUserInfoEvent event) {

    }
    @Override
    public void onGetMatchedRoomsDone(final MatchedRoomsEvent event){

    }
    @Override
    public void onGetOnlineUsersDone(AllUsersEvent arg0) {

    }
    @Override
    public void onSetCustomUserDataDone(LiveUserInfoEvent arg0) {

    }

    public void startGame(String roomId, int maxUsers){
//		Intent intent = new Intent(this, GameActivity.class);
//        Intent intent = new Intent(this, MatchPendingActivity.class);
//        intent.putExtra("roomId", roomId);
//        intent.putExtra("gameType", maxUsers);
//        startActivity(intent);
//        finish();
    }

    @Override
    public void onRPCDone(byte arg0, String arg1, Object arg2) {

    }

    @Override
    public void onGetLiveRoomInfoDone(LiveRoomInfoEvent arg0) {

    }

    @Override
    public void onJoinRoomDone(final RoomEvent event, final String desc) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
                    theClient.subscribeRoom(event.getData().getId());
                }else{
                    HashMap<String, Object> table = new HashMap<String, Object>();
                    table.put("twoUser", true);
                    theClient.createTurnRoom(String.valueOf(System.currentTimeMillis()), "owner", 2, table, Constants.TURN_TIME);
                }
            }
        });
    }

    @Override
    public void onLeaveRoomDone(RoomEvent arg0) {

    }

    @Override
    public void onLockPropertiesDone(byte arg0) {

    }

    @Override
    public void onSetCustomRoomDataDone(LiveRoomInfoEvent arg0) {

    }

    @Override
    public void onSubscribeRoomDone(RoomEvent event) {
        if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
            //发送进入 某个房间
            Log.e("onPrivateChatReceived","paramString1-----Wart"+mFriend.getUserName()+"-----"+Utils.userName);

            theClient.sendPrivateChat(mFriend.getUserName(),Utils.userName);
            Log.e("onPrivateChatReceived","paramString1-----Wart"+event.getData().getId());
            startGame(event.getData().getId(), event.getData().getMaxUsers());
        }else{
            Utils.showToastAlertOnUIThread(this, "onSubscribeRoomDone Failed"+event.getResult());
        }

    }

    @Override
    public void onUnSubscribeRoomDone(RoomEvent arg0) {

    }

    @Override
    public void onUnlockPropertiesDone(byte arg0) {

    }

    @Override
    public void onUpdatePropertyDone(LiveRoomInfoEvent arg0, String arg1) {

    }

    @Override
    public void onRoomCreated(RoomData paramRoomData) {

    }

    @Override
    public void onRoomDestroyed(RoomData paramRoomData) {

    }

    @Override
    public void onUserLeftRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserJoinedRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserLeftLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onUserJoinedLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onChatReceived(ChatEvent event) {
        if(event.getSender().equals(Constants.SERVER_NAME)){
            if(event.getMessage().indexOf('#')!=-1){
                int hashIndex = event.getMessage().indexOf('#');
                final String code = event.getMessage().substring(0, hashIndex);
                final String message = event.getMessage().substring(hashIndex+1, event.getMessage().length());
                try {
                    final int CODE = Integer.parseInt(code);
                    if(CODE==Constants.SUBMIT_CARD){
                        Utils.showToastAlertOnUIThread(this, message);
                    }else if(CODE==Constants.USER_HAND){
                        GameActivity.USER_CARD.clear();
                        JSONObject object = new JSONObject(message);
                        JSONArray cardArray = object.getJSONArray(Utils.userName);
                        for(int i=0;i<cardArray.length();i++){
                            GameActivity.USER_CARD.add((Integer)cardArray.get(i));
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//								Intent intent = new Intent(MappingActiity.this, GameActivity.class);
//								intent.putExtra("roomId", roomId);
//								intent.putExtra("gameType", "2");
//								startActivity(intent);
//								finish();
                            }
                        });
                    }
                } catch (NumberFormatException e) {
                    Log.d("GameActivity", "onChatReceived:NumberFormatException");
                }catch (JSONException e) {
                    Log.d("GameActivity", "onChatReceived:JSONException");
                }
            }
        }
    }

    @Override
    public void onPrivateChatReceived(String paramString1, String paramString2) {
        //获得信息
        Log.e("onPrivateChatReceived","paramString1-----"+paramString1+"rid-----"+paramString2);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(WaitingActivity.this);
                builder.setMessage("big邀请你玩游戏")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
//								theClient.joinRoomWithProperties();
                                HashMap<String, Object> table = new HashMap<String, Object>();
                                table.put("twoUser", true);
                                theClient.joinRoomWithProperties(table);
//								startGame(paramString2, 2);
                                onBackPressed();
                            }
                        })
                        .setNegativeButton("拒绝", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                theClient.sendPrivateChat("bbb","no");
                                onBackPressed();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });


    }

    @Override
    public void onUpdatePeersReceived(UpdateEvent paramUpdateEvent) {

    }

    @Override
    public void onUserChangeRoomProperty(RoomData paramRoomData, String paramString, HashMap<String, Object> paramHashMap, HashMap<String, String> paramHashMap1) {

    }

    @Override
    public void onMoveCompleted(MoveEvent paramMoveEvent) {

    }
    boolean isJump = false;
    @Override
    public void onGameStarted(String sender, String roomId, final String nextTurn) {
        Log.d("onGameStarted: ", "sender: "+sender+" roomId: "+roomId+" nextTurn: "+nextTurn);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!isJump){
                    Intent intent = new Intent(WaitingActivity.this, GameActivity.class);
                    intent.putExtra("roomId", roomId);
                    intent.putExtra("nextTurn", nextTurn);
                    startActivity(intent);
                    isJump = true;
                    finish();
                }

            }
        });
    }


    @Override
    public void onGameStopped(String paramString1, String paramString2) {

    }

    @Override
    public void onUserPaused(String paramString1, boolean paramBoolean, String paramString2) {

    }

    @Override
    public void onUserResumed(String paramString1, boolean paramBoolean, String paramString2) {

    }


}
