package com.gamepind.pvp.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.gamepind.pvp.R;
import com.gamepind.pvp.adapter.FriendshipAdapter;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.FriendList;
import com.gamepind.pvp.model.bean.GameInfo;
import com.gamepind.pvp.model.bean.User;
import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.model.remote.RemoteRepository;
import com.gamepind.pvp.ui.base.BaseActivity;
import com.gamepind.pvp.utils.ToastUtils;
import com.gamepind.pvp.widget.CustomToolBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Roybal 2019/1/23.
 *
 *
 * 描述：邀请好友
 */
public class FriendshipActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.cut_bar)
    CustomToolBar mToolBar;

    private FriendshipAdapter mAdapter;

    private GameInfo mGameInfo;

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mGameInfo = (GameInfo) getIntent().getSerializableExtra(Constant.GAME);
    }

    @Override
    protected int getContentId() {
        return R.layout.activity_invate_friends;
    }


    @Override
    protected void initWidget() {
        mToolBar.setText("Pick up Your Opponent");
        mAdapter = new FriendshipAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter.setGameInfo(mGameInfo);
        RemoteRepository.getInstance().findFriends(Friend.getInstance().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<FriendList>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addDisposable(d);
                    }

                    @Override
                    public void onSuccess(FriendList value) {
                        List<Friend> friends = value.getResponseMessage();
                        mAdapter.setData(friends);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Log", e.getMessage());
                    }
                });
    }
}
