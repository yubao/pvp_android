package com.gamepind.pvp.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gamepind.pvp.R;
import com.gamepind.pvp.demo.Constants;
import com.gamepind.pvp.demo.GameActivity;
import com.gamepind.pvp.demo.GameMainActivity;
import com.gamepind.pvp.demo.Utils;
import com.gamepind.pvp.fragment.FriendsFragment;
import com.gamepind.pvp.fragment.GameFragment;
import com.gamepind.pvp.fragment.MineFragment;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.ui.base.BaseActivity;
import com.gamepind.pvp.utils.LoginHelper;
import com.gamepind.pvp.utils.SharedPreUtils;
import com.taobao.lottery.game.multiplayer.client.MMOGameClient;
import com.taobao.lottery.game.multiplayer.client.command.MMOGameResponseResultCode;
import com.taobao.lottery.game.multiplayer.client.events.AllRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.AllUsersEvent;
import com.taobao.lottery.game.multiplayer.client.events.ChatEvent;
import com.taobao.lottery.game.multiplayer.client.events.ConnectEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveRoomInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LiveUserInfoEvent;
import com.taobao.lottery.game.multiplayer.client.events.LobbyData;
import com.taobao.lottery.game.multiplayer.client.events.MatchedRoomsEvent;
import com.taobao.lottery.game.multiplayer.client.events.MoveEvent;
import com.taobao.lottery.game.multiplayer.client.events.RoomData;
import com.taobao.lottery.game.multiplayer.client.events.RoomEvent;
import com.taobao.lottery.game.multiplayer.client.events.UpdateEvent;
import com.taobao.lottery.game.multiplayer.client.listener.ConnectionRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.NotifyListener;
import com.taobao.lottery.game.multiplayer.client.listener.RoomRequestListener;
import com.taobao.lottery.game.multiplayer.client.listener.ZoneRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;


public class MainActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener,
        MineFragment.onLogoutListener ,NotifyListener , ConnectionRequestListener ,RoomRequestListener,ZoneRequestListener {

    private String mTag;
    private FragmentManager mManager;
    //保存当前创建的Fragment,避免重复创建
    private HashMap<String, Fragment> mFragments = new HashMap<>();
    private static final String FRAMENT_TAG_PALY = "play";
    private static final String FRAMENT_TAG_FRIEND = "friend";
    private static final String FRAMENT_TAG_MINE = "mine";

    @BindView(R.id.radio_parent)
    RadioGroup mRadioGroup;

    @BindView(R.id.radio_btn_game)
    RadioButton mGameButton;

    @BindView(R.id.radio_btn_friend)
    RadioButton mFriendButton;

    @BindView(R.id.radio_btn_mine)
    RadioButton mMineButton;

    private MMOGameClient theClient;


    @Override
    protected int getContentId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mManager = getSupportFragmentManager();
        initView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (LoginHelper.isLogin()) {
            initMMOGameClient();
        }
//        Dialog dialog = new Dialog(this, R.style.DialogTheme);
//        View view = View.inflate(this, R.layout.dialog_friend_invite, null);
//        dialog.setContentView(view);
//        Window window = dialog.getWindow();
//        window.setGravity(Gravity.TOP);
//        //设置弹出动画
////                        window.setWindowAnimations(R.style.main_menu_animStyle);
////                        设置对话框大小
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        dialog.findViewById(R.id.accpet).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HashMap<String, Object> table = new HashMap<String, Object>();
//                table.put("twoUser", true);
//                theClient.joinRoomWithProperties(table);
//                isShow = false;
//                dialog.dismiss();
//            }
//        });
//        dialog.show();

    }
//    private void initMMOGameClient(){
//        try {
//            MMOGameClient.initialize(Constants.APP_KEY, Constants.HOST_NAME);
//            MMOGameClient.setRecoveryAllowance(Constants.RECCOVERY_ALLOWANCE_TIME);
//        } catch (Exception ex) {
//            Utils.showToastAlert(this, Constants.ALERT_INIT_EXEC);
//        }
//    }



    private void initView() {
        generateFragment(FRAMENT_TAG_PALY, GameFragment.class);
        mGameButton.setChecked(true);
        mRadioGroup.setOnCheckedChangeListener(this);

        // 初始化 MMO
        }
    private void initMMOGameClient(){
        try {
            Utils.userName= Friend.getInstance().getUserName();
            theClient = MMOGameClient.getInstance();
            if( theClient.getConnectionState()!=0){
                //用户userName
                theClient.connectWithUserName( Utils.userName, "");
                theClient.addNotificationListener(this);
                theClient.addConnectionRequestListener(this);
                theClient.addZoneRequestListener(this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 创建并添加Fragment
     */
    private void generateFragment(String key, Class clazz) {
        //通过show()与hide()方法来做界面切换
        FragmentTransaction transaction = mManager.beginTransaction();
        try {
            Fragment fragment = mFragments.get(key);
            if (fragment == null) {
                fragment = (Fragment) clazz.newInstance();
                mFragments.put(key, fragment);
                transaction.add(R.id.frame_container, fragment, key);
            }
            if (!TextUtils.isEmpty(mTag)) {
                Fragment currentVisiable = mManager.findFragmentByTag(mTag);
                transaction.hide(currentVisiable);
            }
            transaction.show(fragment);
            transaction.commit();
            mTag = key;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_btn_game:
                mGameButton.setChecked(true);
                generateFragment(FRAMENT_TAG_PALY, GameFragment.class);
                break;
            case R.id.radio_btn_friend:
                if (!LoginHelper.isLogin()) {
                    mGameButton.setChecked(true);
                    startActivity(LoginActivity.class);
                    break;
                }
                mFriendButton.setChecked(true);
                generateFragment(FRAMENT_TAG_FRIEND, FriendsFragment.class);
                break;
            case R.id.radio_btn_mine:
                if (!LoginHelper.isLogin()) {
                    mGameButton.setChecked(true);
                    startActivity(LoginActivity.class);
                    break;
                }
//                Intent intent = new Intent(this, GameMainActivity.class);
//                startActivity(intent);
                mMineButton.setChecked(true);
                generateFragment(FRAMENT_TAG_MINE, MineFragment.class);
                break;
            default:
                break;
        }
    }

    @Override
    public void onLogout() {
        mGameButton.setChecked(true);
        generateFragment(FRAMENT_TAG_PALY, GameFragment.class);
        SharedPreUtils.getInstance().putString(Constant.MEMBER, "");
    }

    @Override
    public void onRoomCreated(RoomData paramRoomData) {

    }

    @Override
    public void onRoomDestroyed(RoomData paramRoomData) {

    }

    @Override
    public void onUserLeftRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserJoinedRoom(RoomData paramRoomData, String paramString) {

    }

    @Override
    public void onUserLeftLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onUserJoinedLobby(LobbyData paramLobbyData, String paramString) {

    }

    @Override
    public void onChatReceived(ChatEvent event) {
//        if(event.getSender().equals(Constants.SERVER_NAME)){
//            if(event.getMessage().indexOf('#')!=-1){
//                int hashIndex = event.getMessage().indexOf('#');
//                final String code = event.getMessage().substring(0, hashIndex);
//                final String message = event.getMessage().substring(hashIndex+1, event.getMessage().length());
//                try {
//                    final int CODE = Integer.parseInt(code);
//                    if(CODE==Constants.SUBMIT_CARD){
//                        Utils.showToastAlertOnUIThread(this, message);
//                    }else if(CODE==Constants.USER_HAND){
//                        GameActivity.USER_CARD.clear();
//                        JSONObject object = new JSONObject(message);
//                        JSONArray cardArray = object.getJSONArray(Utils.userName);
//                        for(int i=0;i<cardArray.length();i++){
//                           // GameActivity.USER_CARD.add((Integer)cardArray.get(i));
//                        }
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
////								Intent intent = new Intent(MappingActiity.this, GameActivity.class);
////								intent.putExtra("roomId", roomId);
////								intent.putExtra("gameType", "2");
////								startActivity(intent);
////								finish();
//                            }
//                        });
//                    }
//                } catch (NumberFormatException e) {
//                    Log.d("GameActivity", "onChatReceived:NumberFormatException");
//                }catch (JSONException e) {
//                    Log.d("GameActivity", "onChatReceived:JSONException");
//                }
//            }
//        }
    }
    boolean isShow = false;
    @Override
    public void onPrivateChatReceived(String paramString1, String userName) {
        //获得信息
        Log.e("onPrivateChatReceived","paramString1Main-----"+paramString1+"rid-----"+userName);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!isShow) {
                    isShow = true;
//                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//
//                    builder.setMessage("big邀请你玩游戏")
//                            .setCancelable(false)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
////								theClient.joinRoomWithProperties();
//
////								startGame(paramString2, 2);
////                                onBackPressed();
//                                }
//                            })
//                            .setNegativeButton("拒绝", new DialogInterface.OnClickListener() {
//
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    theClient.sendPrivateChat("bbb","no");
//                                    isShow = false;
//                                }
//                            });
//                    AlertDialog alert = builder.create();
//                    alert.show();
                    Dialog dialog = new Dialog(MainActivity.this, R.style.DialogTheme);
                    View view = View.inflate(MainActivity.this, R.layout.dialog_friend_invite, null);
                    dialog.setContentView(view);
                    Window window = dialog.getWindow();
                    window.setGravity(Gravity.TOP);
                    //设置弹出动画
                    window.setWindowAnimations(R.style.main_menu_animStyle);
                    //设置对话框大小
                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.findViewById(R.id.accpet).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            HashMap<String, Object> table = new HashMap<String, Object>();
                            table.put("twoUser", true);
                            theClient.joinRoomWithProperties(table);
                            isShow = false;
                            dialog.dismiss();
                        }
                    });
                    dialog.findViewById(R.id.later).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            theClient.sendPrivateChat(userName, "no");
                            isShow = false;
                            dialog.dismiss();
                        }
                    });
//                    ((TextView) dialog.findViewById(R.id.tv_user)).setText(userName);
                    ((TextView) dialog.findViewById(R.id.tv_user)).setText(userName+"\n"+"Invite you");

                    dialog.show();
                }
            }
        });


    }
    @Override
    public void onStart(){
        super.onStart();
//        Dialog dialog = new Dialog(MainActivity.this, R.style.DialogTheme);
//        View view = View.inflate(MainActivity.this, R.layout.dialog_friend_invite, null);
//        dialog.setContentView(view);
//        Window window = dialog.getWindow();
//        window.setGravity(Gravity.TOP);
//        //设置弹出动画
//        window.setWindowAnimations(R.style.main_menu_animStyle);
//        //设置对话框大小
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        ((TextView) dialog.findViewById(R.id.tv_user)).setText("userName"+"\n"+"Invite you");
//
//        dialog.show();
    }

    @Override
    public void onStop(){
        super.onStop();
        if(theClient!=null){
            theClient.removeZoneRequestListener(this);
            theClient.removeRoomRequestListener(this);
            theClient.removeNotificationListener(this);
        }
    }
    @Override
    public void onUpdatePeersReceived(UpdateEvent paramUpdateEvent) {

    }

    @Override
    public void onUserChangeRoomProperty(RoomData paramRoomData, String paramString, HashMap<String, Object> paramHashMap, HashMap<String, String> paramHashMap1) {

    }

    @Override
    public void onMoveCompleted(MoveEvent paramMoveEvent) {

    }
    boolean isJump = false;
    @Override
    public void onGameStarted(String sender, String roomId, final String nextTurn) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //TODO
                if(!isJump){
                    Intent intent = new Intent(MainActivity.this, GameActivity.class);
                    intent.putExtra("roomId", roomId);
                    intent.putExtra("nextTurn", nextTurn);
                    startActivity(intent);
                    isJump = true;
                }

            }
        });
    }

    @Override
    public void onGameStopped(String paramString1, String paramString2) {

    }

    @Override
    public void onUserPaused(String paramString1, boolean paramBoolean, String paramString2) {

    }

    @Override
    public void onUserResumed(String paramString1, boolean paramBoolean, String paramString2) {

    }

    @Override
    public void onConnectDone(ConnectEvent event, String paramString) {
        Log.d("onConnectDone", event.getResult()+" listener "+paramString);

    }

    @Override
    public void onDisconnectDone(ConnectEvent paramConnectEvent) {

    }

    @Override
    public void onInitUDPDone(byte paramByte) {

    }

    @Override
    public void onSubscribeRoomDone(RoomEvent event) {
        if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
            //发送进入 某个房间
            startGame(event.getData().getId(), event.getData().getMaxUsers());
        }else{
            Utils.showToastAlertOnUIThread(this, "onSubscribeRoomDone Failed"+event.getResult());
        }
    }
    public void startGame(String roomId, int maxUsers){
		Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("roomId", roomId);
        intent.putExtra("gameType", maxUsers);
        startActivity(intent);
//        finish();
    }

    @Override
    public void onUnSubscribeRoomDone(RoomEvent paramRoomEvent) {

    }

    @Override
    public void onJoinRoomDone(RoomEvent event, String paramString) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(event.getResult()== MMOGameResponseResultCode.SUCCESS){
                    theClient.subscribeRoom(event.getData().getId());
                }else{
                    HashMap<String, Object> table = new HashMap<String, Object>();
                    table.put("twoUser", true);
                    theClient.createTurnRoom(String.valueOf(System.currentTimeMillis()), "owner", 2, table, Constants.TURN_TIME);
                }
            }
        });
    }

    @Override
    public void onLeaveRoomDone(RoomEvent paramRoomEvent) {

    }

    @Override
    public void onGetLiveRoomInfoDone(LiveRoomInfoEvent paramLiveRoomInfoEvent) {

    }

    @Override
    public void onSetCustomRoomDataDone(LiveRoomInfoEvent paramLiveRoomInfoEvent) {

    }

    @Override
    public void onUpdatePropertyDone(LiveRoomInfoEvent paramLiveRoomInfoEvent, String paramString) {

    }

    @Override
    public void onLockPropertiesDone(byte paramByte) {

    }

    @Override
    public void onUnlockPropertiesDone(byte paramByte) {

    }

    @Override
    public void onDeleteRoomDone(RoomEvent paramRoomEvent, String paramString) {

    }

    @Override
    public void onGetAllRoomsDone(AllRoomsEvent paramAllRoomsEvent) {

    }

    @Override
    public void onCreateRoomDone(RoomEvent event, String paramString) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(event.getResult()== MMOGameResponseResultCode.SUCCESS){// if room created successfully
                    theClient.joinRoom(event.getData().getId());
                }else{
                    //Utils.showToastAlert(RoomSelectionActivity.this, Constants.ALERT_ROOM_CREATE + event.getResult());
                }
            }
        });
    }

    @Override
    public void onGetOnlineUsersDone(AllUsersEvent paramAllUsersEvent) {

    }

    @Override
    public void onGetLiveUserInfoDone(LiveUserInfoEvent paramLiveUserInfoEvent) {

    }

    @Override
    public void onSetCustomUserDataDone(LiveUserInfoEvent paramLiveUserInfoEvent) {

    }

    @Override
    public void onGetMatchedRoomsDone(MatchedRoomsEvent paramMatchedRoomsEvent) {

    }

    @Override
    public void onRPCDone(byte paramByte, String paramString, Object paramObject) {

    }
}
