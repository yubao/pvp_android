package com.gamepind.pvp.activity;


import static butterknife.OnTextChanged.Callback.AFTER_TEXT_CHANGED;

import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gamepind.pvp.R;
import com.gamepind.pvp.demo.Utils;
import com.gamepind.pvp.model.bean.Friend;
import com.gamepind.pvp.model.bean.LoginResultInfo;
import com.gamepind.pvp.model.bean.ResultInfo;
import com.gamepind.pvp.model.remote.Constant;
import com.gamepind.pvp.model.remote.RemoteRepository;
import com.gamepind.pvp.ui.base.BaseActivity;
import com.gamepind.pvp.utils.SharedPreUtils;
import com.gamepind.pvp.utils.ToastUtils;
import com.google.gson.Gson;
import com.taobao.lottery.game.multiplayer.client.MMOGameClient;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * @author 张玉保 2019/1/22.
 *
 * 描述：用户登录
 */
public class LoginActivity extends BaseActivity {

    @BindView(R.id.edit_number)
    EditText mUserNameEditText;

    @BindView(R.id.edit_password)
    EditText mPasswordEditText;

    @BindView(R.id.btn_login)
    TextView mLoginBtn;

    @Override
    protected int getContentId() {
        return R.layout.activity_login;
    }


    @OnTextChanged(value = R.id.edit_number, callback = AFTER_TEXT_CHANGED)
    public void onAfterTextChange(Editable editable) {
        if (TextUtils.isEmpty(mUserNameEditText.getText().toString().trim())) {
            return;
        } else if (!TextUtils.isEmpty(mUserNameEditText.getText().toString().trim())) {
            mLoginBtn.setEnabled(true);
        } else {
            mLoginBtn.setEnabled(false);
        }
    }

    /**
     * 用户登录接口
     */
    @OnClick(R.id.btn_login)
    public void onLogin() {
        String username = mUserNameEditText.getText().toString().trim();
        String password = mPasswordEditText.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            ToastUtils.show("please input username");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            ToastUtils.show("please input password");
            return;
        }

        RemoteRepository
                .getInstance().login(username,password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<LoginResultInfo>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addDisposable(d);
                    }

                    @Override
                    public void onSuccess(LoginResultInfo value){
                        SharedPreUtils.getInstance().putString(Constant.MEMBER, new Gson().toJson(value.getFriend()));
                        //建立连接
                        Utils.userName = value.getFriend().getUserName();
                        try {
                            MMOGameClient.getInstance().connectWithUserName(Utils.userName, "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Log",e.getMessage());
                        ToastUtils.show("**********") ;
                    }
                });

    }




}