package com.gamepind.pvp.ui.main;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;


import com.gamepind.pvp.R;
import com.gamepind.pvp.activity.MainActivity;
import com.gamepind.pvp.demo.GameMainActivity;
import com.gamepind.pvp.model.bean.FriendList;
import com.gamepind.pvp.model.bean.InviteList;
import com.gamepind.pvp.model.bean.ResultInfo;
import com.gamepind.pvp.model.bean.UserInfo;
import com.gamepind.pvp.model.remote.RemoteRepository;
//import com.gamepind.pvp.ui.activity.BillboardActivity;
import com.gamepind.pvp.ui.base.BaseActivity;
import com.gamepind.pvp.utils.ToastUtils;

import butterknife.BindView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by newbiechen on 17-4-14.
 */

public class SplashActivity extends BaseActivity {
    private static final int WAIT_TIME = 3000;
    private static final int PERMISSIONS_REQUEST_STORAGE = 0;

    static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @BindView(R.id.btn_login)
    TextView login;
//    @BindView(R.id.test_btn)
//    Button testBtn;

//    private Unbinder unbinder;
//    private Runnable skipRunnable;
//    private PermissionsChecker mPermissionsChecker;

    private boolean isSkip = false;


    @Override
    protected int getContentId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        login.setOnClickListener(
                (v)->{
                    Intent intent = new Intent(this,GameMainActivity.class);
                    startActivity(intent);
                    ToastUtils.show("fdsff");
                }
        );
    }


//        testBtn.setOnClickListener(
//                (v)->{

//                  //TODO 登陸
//               RemoteRepository
//                            .getInstance().login("leo","123")
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(new SingleObserver<ResultInfo>() {
//                                @Override
//                                public void onSubscribe(Disposable d) {
//                                    addDisposable(d);
//                                }
//
//                                @Override
//                                public void onSuccess(ResultInfo value){
//                                    ToastUtils.show(value.getResponseMessage()) ;
//                                }
//
//                                @Override
//                                public void onError(Throwable e) {
//                                    Log.e("Log",e.getMessage());
//                                    ToastUtils.show("**********") ;
//                                }
//                            })
//TODO 添加好友
//                    RemoteRepository
//                            .getInstance().addFriend("1","123")
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(new SingleObserver<ResultInfo>() {
//                                @Override
//                                public void onSubscribe(Disposable d) {
//                                    addDisposable(d);
//                                }
//
//                                @Override
//                                public void onSuccess(ResultInfo value){
//                                    ToastUtils.show(value.getResponseMessage()) ;
//                                }
//
//                                @Override
//                                public void onError(Throwable e) {
//                                    Log.e("Log",e.getMessage());
//                                    ToastUtils.show("**********") ;
//                                }
//                            });

                    //TODO 获得好友
//                    RemoteRepository
//                            .getInstance().findFriends("1")
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(new SingleObserver<FriendList>() {
//                                @Override
//                                public void onSubscribe(Disposable d) {
//                                    addDisposable(d);
//                                }
//
//                                @Override
//                                public void onSuccess(FriendList value){
//                                    ToastUtils.show(value.getResponseMessage().toString()) ;
//                                }
//
//                                @Override
//                                public void onError(Throwable e) {
//                                    Log.e("Log",e.getMessage());
//                                    ToastUtils.show("**********") ;
//                                }
//                            });
//
//                    RemoteRepository
//                            .getInstance().getInviteFriendHistory("1","3")
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(new SingleObserver<InviteList>() {
//                                @Override
//                                public void onSubscribe(Disposable d) {
//                                    addDisposable(d);
//                                }
//
//                                @Override
//                                public void onSuccess(InviteList value){
//                                    ToastUtils.show(value.getResponseMessage().toString()) ;
//                                }
//
//                                @Override
//                                public void onError(Throwable e) {
//                                    Log.e("Log",e.getMessage());
//                                    ToastUtils.show("**********") ;
//                                }
//                            });
//                }



//        );
//        login.setOnClickListener(
//                (v)->{
////                    startActivity(BillboardActivity.class);
//                    ToastUtils.show("fdsff");
//                }
//        );
//        testBtn.setOnClickListener(
//                (v)->{
////                    UserInfo user = (UserInfo)RemoteRepository.getInstance().getUserInfo();
//                    RemoteRepository
//                            .getInstance().getUserInfo()
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(new SingleObserver<UserInfo>() {
//                                @Override
//                                public void onSubscribe(Disposable d) {
//                                    addDisposable(d);
//                                }
//
//                                @Override
//                                public void onSuccess(UserInfo value){
//                                    ToastUtils.show(value.getMessage()) ;
//                                }
//
//                                @Override
//                                public void onError(Throwable e) {
//                                    ToastUtils.show("**********") ;
//                                }
//                            });
//                }
//
//
//
//        );
//    }
//    protected void startActivity(Class<? extends AppCompatActivity> activity){
//        Intent intent = new Intent(this,activity);
//        startActivity(intent);
//    }

    private void requestPermission(){
        //获取读取和写入SD卡的权限
//        if (mPermissionsChecker.lacksPermissions(PERMISSIONS)){
//            //是否应该展示详细信息
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
//
//                // Show an expanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//            } else {
//                //请求权限
//                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSIONS_REQUEST_STORAGE);
//            }
//        }
    }

    private synchronized void skipToMain(){
//        if (!isSkip){
//            isSkip = true;
//            Intent intent = new Intent(this,GameMainActivity.class);
//            startActivity(intent);
//            finish();
//        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case PERMISSIONS_REQUEST_STORAGE: {
///*                // 如果取消权限，则返回的值为0
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    //申请权限成功
//
//                } else {
//                    //申请权限失败
//                }*/
//
//                //自动跳过
//                mTvSkip.postDelayed(skipRunnable,WAIT_TIME);
//                //点击跳过
//                mTvSkip.setOnClickListener((view) -> skipToMain());
//                return;
//            }
//        }
    }

}
