//package com.gamepind.pvp.presenter;
//
//import com.gamepind.pvp.model.bean.BillboardPackage;
//import com.gamepind.pvp.model.local.LocalRepository;
//import com.gamepind.pvp.model.remote.Constant;
//import com.gamepind.pvp.model.remote.RemoteRepository;
//import com.gamepind.pvp.presenter.conract.BillboardContract;
//import com.gamepind.pvp.ui.base.RxPresenter;
//import com.gamepind.pvp.utils.SharedPreUtils;
//import com.google.gson.Gson;
//
//import io.reactivex.SingleObserver;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.schedulers.Schedulers;
//
///**
// * Created by sunny on 17-4-23.
// */
//
//public class BillboardPresenter extends RxPresenter<BillboardContract.View>
//        implements BillboardContract.Presenter {
//
//    public BillboardPackage getB() {
//        //TODO
//        String json = SharedPreUtils.getInstance()
//                .getString(Constant.SHARED_SAVE_BILLBOARD);
//        if (json == null){
//            return null;
//        }
//        else {
//            return new Gson().fromJson(json,BillboardPackage.class);
//        }
//    }
//    @Override
//    public void loadBillboardList() {
//        //这个最好是设定一个默认时间采用Remote加载，如果Remote加载失败则采用数据中的数据。我这里先写死吧
//        BillboardPackage bean = getB();
//        bean = null;
//        if (bean == null){
//            RemoteRepository.getInstance()
//                    .getBillboardPackage()
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .doOnSuccess(
//                            (value) ->{
//                                Schedulers.io().createWorker()
//                                        .schedule(
//                                                () ->{
//                                                    LocalRepository.getInstance()
//                                                            .saveBillboardBean(value.getMale());
////                                                    String json = new Gson().toJson(value);
////                                                    SharedPreUtils.getInstance()
////                                                            .putString(Constant.SHARED_SAVE_BILLBOARD,json);
//                                                }
//                                        );
//                            }
//                    )
//                    .subscribe(new SingleObserver<BillboardPackage>() {
//                        @Override
//                        public void onSubscribe(Disposable d) {
//                            addDisposable(d);
//                        }
//
//                        @Override
//                        public void onSuccess(BillboardPackage value) {
//                            mView.finishRefresh(value);
//                            mView.complete();
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//                            mView.showError();
//                        }
//                    });
//        }
//        else {
//            mView.finishRefresh(bean);
//            mView.complete();
//        }
//    }
//}
