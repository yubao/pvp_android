//package com.gamepind.pvp.presenter.conract;
//
//
//import com.gamepind.pvp.model.bean.BillboardPackage;
//import com.gamepind.pvp.ui.base.BaseContract;
//
///**
// * Created by newbiechen on 17-4-23.
// */
//
//public interface BillboardContract {
//
//    interface View extends BaseContract.BaseView{
//        void finishRefresh(BillboardPackage beans);
//    }
//
//    interface Presenter extends BaseContract.BasePresenter<View>{
//        void loadBillboardList();
//    }
//}
